var Product = require('../models/product.js');
var image64 = require('../lib/image64.js');
var Store = require('../models/store.js');

var controller = {};

/* GET /api/products */
controller.index = function (req, res, next) {
  var page = req.query.page || 0;

  Product.find({ visible: true }).sort({ "created_at": -1}).skip(page * 8).limit(8).exec(function(err, data) {
    if (err) { console.log(err); return res.status(500).send(); }
    res.status(200).json(data);
  });
}

/* GET /api/products/:id */
controller.show = function (req, res, next) {
  var product_id = req.params.id;
  if(!product_id) return res.status(400).send();

  Product.findOne({ _id: product_id }, function (err, user) {
    if (err) { console.log(err); return res.status(500).send(); }

    if (user) {
      res.status(200).json(user);
    } else {
      res.status(404).send();
    }
  });
}

/* POST /api/products */
controller.create = function (req, res, next) {
  var data = req.body;
  data.created_at = new Date();

  var product = new Product(data);

  product.validate(function(err){
    if(err) return res.status(400).send(err);
  });

  Store.findOne({ _id: data.store_id }, function (err, store) {
    if (err) { console.log(err); return res.status(500).send(); }

    if(store){
      product.visible = store.visible;
      product.vitrine = store.vitrine;
      product.hide_price = store.hide_price;

      product.save(function(err, data) {
        if (err) { console.log(err); return res.status(500).send(); }
        res.status(200).send();
      });
    } else {
      res.status(400).send();
    }
  });



};

/* PUT /api/products/:id */
controller.update = function (req, res, next){
    var product_id = req.params.id;
    var data = req.body.product;

    if(!product_id || !data) return res.status(400).send();

    Product.update({ _id: product_id }, data , {} ,function (err, numAffected) {
      if (err) { console.log(err); return res.status(500).send(); }

      if (numAffected.n != 0) {
        res.status(200).send(numAffected);
      } else {
        res.status(404).send(numAffected);
      }
    });
};

/* DELETE /api/products/:id */
controller.delete = function (req, res, next){
    var product_id = req.params.id;
    if(!product_id) return res.status(400).send();

    Product.remove({ _id: product_id }, function(err) {
      if (err) { console.log(err); return res.status(500).send();
      } else {
        return res.status(200).send();
      }
   });
};

/* POST /api/products/view/:id
  Aumenta a visualização de um produto  */
controller.view = function (req, res, next){
    var product_id = req.params.id;

    if(!product_id) return res.status(400).send();

    Product.findOneAndUpdate({ _id: product_id },
        {$inc: { views: 1 }},
        {safe: true, upsert: true},
        function(err) {
          if (err) {
            console.log(err);
            return res.status(500).send();
          } else{
            return res.status(200).send();
          }
        }
    );
};

/* GET /api/category/products */
controller.categoryProducts = function (req, res, next) {
  var category = req.query.category;
  if(!category) return res.status(400).send();
  var page = req.query.page || 0;

  Product.find({ categories: { $in: [category] } , visible: true }).sort({ "created_at": -1}).skip(page * 8).limit(8)
  .exec(function(err, data) {
    if (err) { console.log(err); return res.status(500).send(); }
    res.status(200).json(data);
  });
};


/* GET /api/category/:id/products
   Retorna produtos baseado no id da loja e da categoria.*/
controller.categoryProductsByStore = function (req, res, next) {
  var category = req.query.category;
  var store_id = req.params.id;

  if(!category || !store_id) return res.status(400).send();
  var page = req.query.page || 0;

  Product.find({ store_id: store_id , categories: { $in: [category] } }).sort({ "created_at": -1})
  .exec(function(err, data) {
    if (err) { console.log(err); return res.status(500).send(); }

    Product.find({ store_id: store_id , categories: { $in: [category] } }).count().exec(function(err, length) {
      res.header('pages', parseInt( (length + 19) /20 ));
      res.status(200).json(data);
    });
  });
};

/* GET /api/search/products */
controller.search = function (req, res, next) {
  var query = req.query.query;
  if(!query) return res.status(400).send();

  Product.find({ 'name' : new RegExp(query, 'i') , visible: true })
  .limit(15).exec(function(err, data) {
    if (err) { console.log(err); return res.status(500).send(); }
    res.status(200).json(data);
  });
};



/* GET /api/search/:id/products */
controller.searchInStore = function (req, res, next) {
  var query = req.query.query;
  var store_id = req.params.id;
  if(!query || !store_id) return res.status(400).send();

  Product.find({ 'name' : new RegExp(query, 'i'), store_id: store_id })
  .limit(15).exec(function(err, data) {
    if (err) { console.log(err); return res.status(500).send(); }
    res.status(200).json(data);
  });
};

/* GET /api/stores/:id/products
retornando produtos por loja. */
controller.storeProducts = function (req, res, next) {
  var page = req.query.page || 0;
  var store_id = req.params.id;

  if(!store_id) return res.status(400).send();

  Product.find({ store_id: store_id }).sort({ "created_at": -1}).skip(page * 20).limit(20).exec(function(err, data) {
    if (err) { console.log(err); return res.status(500).send(); }

    Product.find({ store_id: store_id }).count().exec(function(err, length) {
      res.header('pages', parseInt( (length + 19) /20 ));
      res.status(200).json(data);
    });

  });
}

/* POST /api/upload
retornando produtos por loja. */
controller.upload = function (req, res, next) {
  image64.decodeBase64Image(req.body.image, function(err, data){
    if (err) { console.log(err); return res.status(500).send(); }

    return res.status(200).send({ url:  data.url, thumb: data.thumb });
  })
};



/* POST /api/group/products
retorna os produtos baseados em um array de id's. */
controller.groupProducts = function (req, res, next) {
  var ids = req.body.ids;
  if(!ids) return res.status(400).send();
  if(!Array.isArray(ids)) return res.status(400).send();

  // certificando que todos os ids são válidos.
  for (var i = 0; i < ids.length; i++) {
    if(ids[i] == undefined || ids[i] == 'undefined' || ids[i] == null)
      ids.splice(i, 1);
  }

  Product.find({ _id: { $in: ids } }).exec(function(err, data) {
    if (err) { console.log(err); return res.status(500).send(); }
    res.status(200).json(data);
  });
}

module.exports = controller;
