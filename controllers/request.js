var Request = require('../models/request.js');
var User = require('../models/user.js');
var Store = require('../models/store.js');
var request = require('request');
var ObjectId = require('mongoose').Types.ObjectId; 

var PROXY = "http://proxy.maxshoplus.com.br"; //"http://localhost:8080";

controller = {};

/* GET /api/requests */
controller.index = function (req, res, next) {
  var page = req.query.page || 0;

  Request.find({}).skip(page * 20).limit(20).sort({ "created_at": -1 })
  .exec(function(err, data) {
    if (err) { console.log(err); return res.status(500).send(); }
    res.status(200).json(data);
  });
};


/* GET /api/requests/:id */
controller.show = function (req, res, next) {
  var request_id = req.params.id;
  if(!request_id) return res.status(400).send();

  Request.findOne({ _id: request_id }, function (err, data) {
    if (err) {console.log(err); return res.status(500).send(); }

    if (data) {
      Store.findOne({_id: data.store_id}, function(err2, store){
        if (err2) {console.log(err2); return res.status(500).send(); }
        if(store){
          request.get(PROXY+"/transactions/"+data.transaction_id+"?apiKey="+store.pagarmeApiKey, function(err, data3, transaction){
            var pedido = data.toObject();
            pedido.transaction = typeof(transaction) === 'string' ? JSON.parse(transaction) : transaction;
            res.status(200).json(pedido);
          });
        }else{
          res.status(404).send();
        }
      });
    } else {
      res.status(404).send();
    }
  });
};

/* POST /api/requests */
controller.create = function (req, res, next) {
  var data = req.body;
  data.created_at = new Date();
  var requestModel = new Request(data);
  
  var transaction = data.transaction;

  requestModel.validate(function(err){
    if(err) return res.status(400).send(err);
  });

  requestModel.save(function(err, respReq) {
    if (err) { console.log(err); return res.status(500).send(); }
    var postback_url = 'http://107.170.34.163:3000/api/requests/{id}/update'; //107.170.34.163
    transaction.postback_url = postback_url.replace('{id}', respReq._id);

    var userParams = {}; 
    if(data.card_id) {
      userParams = {
        cards: {
          $elemMatch: {
            _id: data.card_id
          }
        }
      };
    }else{
      userParams = {_id: data.user_id};
    }

    User.findOne(userParams, function (err, user) {
      if (err) { console.log(err); return res.status(500).send(); }

      if (user) {
        if(transaction.payment_method == 'credit_card') {
          for(var i in user.cards){
            var card = user.cards[i];
            if(card._id == data.card_id){
              transaction.card = card;
            }
          }
        }
        request.post( { url: PROXY+"/transactions?apiKey="+data.apiKey, json: transaction }, function (error, response, body) {
          if (err) {
            respReq.remove(); 
            console.log(err); 
            return res.status(500).send(); 
          }
          if (response.statusCode == 400) {
            respReq.remove();
            return res.status(400).send(body);
          }
          
          var transaction = body;
          transaction.request_id = respReq._id;
          transaction.code = respReq.code;
          //debugger;
          var pedido = respReq.toObject();
          pedido.transaction = transaction;
          pedido.status = body.status;
          pedido.transaction_id = body.id;

          Request.update({ _id: respReq._id }, {
            transaction_id: body.id,
            status: body.status  
          } , {} ,function (err, numAffected) {
            //debugger;
            if (err) { console.log(err); return res.status(500).send(); }

            if (numAffected.n !== 0) {
              res.status(200).send(pedido);
            } else {
              res.status(404).send(numAffected);
            }
          });
      });
      } else {
        res.status(404).send();
      }
    });
    
    
    //res.status(200).json(data);
  });
};

/* PUT /api/requests/:id */
controller.update = function (req, res, next){
    var request_id = req.params.id;
    var data = req.body;
    if(!request_id || !data) return res.status(400).send();
    // console.log(data);
    Request.update({ _id: request_id }, data , {} ,function (err, numAffected) {
      if (err) { console.log(err); return res.status(500).send(); }

      if (numAffected.n !== 0) {
        res.status(200).send(numAffected);
      } else {
        res.status(404).send(numAffected);
      }
    });
};

/* DELETE /api/requests/:id */
controller.delete = function (req, res, next){
    var request_id = req.params.id;
    if(!request_id) return res.status(400).send();

    Request.remove({ _id: request_id }, function(err) {
      if (err) {
        console.log(err);
        return res.status(500).send();
      } else {
        return res.status(200).send();
      }
   });
};

/* GET /api/requests/user/:id */
controller.requestsByUser = function (req, res, next) {
  var user_id = req.params.id;
  if(!user_id) return res.status(400).send();
  var page = req.query.page || 0;

  Request.find({ user_id: user_id }).skip(page * 20).limit(20)
  .sort({ "created_at": -1 }).exec(function(err, data) {
    if (err) { console.log(err); return res.status(500).send(); }
    res.status(200).json(data);
  });
};

/* GET /api/requests/user/:id */
controller.requestsByStore = function (req, res, next) {
  var store_id = req.params.id;
  if(!store_id) return res.status(400).send();
  var page = req.query.page || 0;

  Request.find({ store_id: store_id }).skip(page * 20).limit(20)
  .sort({ "created_at": -1 }).exec(function(err, data) {
    if (err) { console.log(err); return res.status(500).send(); }
    res.status(200).json(data);
  });
};
/* POST /api/requests/:id/update */
controller.updateStatus = function(req, res, next) {
  var request_id = req.params.id;
  var data = {
    // id: req.body.id,
    status: req.body.current_status
  };

  if(!request_id) return res.status(400).send();
  /*
  Request.findOne({ _id: request_id, 'transactions.id': data.id }).exec(function (err, request) {
    if (err) { console.log(err); return res.status(500).send(err); }

    if (request) {
      var l = request.transactions.length;

      for(var i =0; i < l ; i++){
          var trans = request.transactions[i];
          if(trans.id == data.id){
            request.transactions[i] = data.status;
          }
      }

      request.save(function(err){
        console.log(err);
      });

      res.status(200).send();
    } else {
      res.status(404).send();
    }
  });
  */
  Request.update({ _id: request_id }, data , {} ,function (err, numAffected) {
    if (err) { console.log(err); return res.status(500).send(); }

    if (numAffected.n !== 0) {
      res.status(200).send(numAffected);
    } else {
      res.status(404).send(numAffected);
    }
  });
};

/* POST /api/requests/:id/refund */
controller.refund = function(req, res, next) {
  var request_id = req.params.id;
  var data = {
    transaction_id: req.body.transaction_id,
    store_id: req.body.store_id
  };
  
  Store.findOne({_id: data.store_id }).exec(function(err, store) {
    if (err) { return res.status(500).send(); }
    if(store){
      request.get( { url: PROXY+"/transactions/"+data.transaction_id+"/refund?apiKey="+store.pagarmeApiKey }, function (error, response, body) {
          if (error || response.statusCode == 500) { return res.status(500).send(); }
          if (response.statusCode == 400) { return res.status(400).send(body); }
          
          if(body) {
            Request.update({ _id: request_id }, { status: 'refunded' } , {} ,function (err, numAffected) {
              if (err) { console.log(err); return res.status(500).send(); }
              if (numAffected.n !== 0) {
                res.status(200).send(numAffected);
              } else {
                res.status(404).send(numAffected);
              }
            });
          }
      });
    }
  });
};

module.exports = controller;
