var controller = {};
var request = require("request");
var parseString = require('xml2js').parseString;

var Request = require('../models/request.js');
var Product = require('../models/product.js');

controller.notification = function (req, res, next) {
  console.log("Notification chamada!");
  var requestModel = require('../models/request.js');

  var notificationCode = req.body.notificationCode;
  if (! notificationCode) {
    res.status(500).json({msg:'problemas, sem notificationCode' });
    return false;
  }

  request({
    uri: "https://ws.pagseguro.uol.com.br/v3/transactions/notifications/"+
    notificationCode+"?email=jhonyjl37@gmail.com&token=02F762290889416DB2CCD7E0AC8A8D1A",
    method: "GET"
    }, function(error, response, body) {
    console.log(body);

    parseString(body, function (err, result) {
        console.dir(result);

        if(result.transaction){
          requestModel.update(result.transaction.reference,
            { status: result.transaction.status });

          var status = result.transaction.status;
          if( status == 6 || status == 7 || status == 8){
            requestModel.updateQuantity(result.transaction.reference);
          }

        }
    });

    res.status(200).json({msg: 'tudo correu bem.' });
  });

}


/* PARA OS RELATÓRIOS
==============================================================================*/
//var runsql = require('../config/runsql.js')
// Todos os produtos cadastrados.

controller.allProducts = function (req, res, next) {
  Product.find().count({}, function(err, obj){
    if (err) { console.log(err); return res.status(500).send(err); }
    if(obj){
      res.status(200).json({"total":obj});
    }else{
      res.status(404).send();
    }
  });
};


// Todas as visualizações.
controller.allViews = function (req, res, next) {
  var sql = 'SELECT SUM(views) AS total FROM products; ';
  runsql.query(sql, function(err, results){
    if(err){
      res.status(500).json(err); return;
    }

    res.status(200).json(results[0]);
  });
};

// Todas as visualizações.
controller.allLovers = function (req, res, next) {
  var sql = 'SELECT COUNT(*) AS total FROM product_lover; ';
  runsql.query(sql, function(err, results){
    if(err){
      res.status(500).json(err); return;
    }

    res.status(200).json(results[0]);
  });
};

// Todas de Lojas.
controller.allStores = function (req, res, next) {
  var sql = 'SELECT COUNT(*) AS total FROM stores; ';
  runsql.query(sql, function(err, results){
    if(err){
      res.status(500).json(err); return;
    }

    res.status(200).json(results[0]);
  });
};


// Todas de clientes usuários.
controller.allClients = function (req, res, next) {
  var sql = " SELECT COUNT(*) AS total FROM users WHERE role = 'client'; ";
  runsql.query(sql, function(err, results){
    if(err){
      res.status(500).json(err); return;
    }

    res.status(200).json(results[0]);
  });
};


// Valor das vendas mensal
controller.monthRequests = function (req, res, next) {
  var sql = "SELECT sum(price * quantity) AS total, to_char(created_at,'Mon') As month FROM " +
  "requests INNER JOIN products_request ON requests.id = " +
  "products_request.request_id group by month LIMIT 7;" ;

  runsql.query(sql, function(err, results){
    if(err){
      res.status(500).json(err); return;
    }

    res.status(200).json(results);
  });
};

// retorna os valores baseado nos dias
controller.dailyRequests = function (req, res, next) {
  var sql = "SELECT sum(price * quantity) AS total, to_char(created_at,'Day') As day FROM " +
  "requests INNER JOIN products_request ON requests.id = " +
  "products_request.request_id group by day LIMIT 7;" ;

  runsql.query(sql, function(err, results){
    if(err){
      res.status(500).json(err); return;
    }

    res.status(200).json(results);
  });
};


/* Para todas as lojas - Relatorios
------------------------------------------------------------------------------*/
controller.allProductsStore = function (req, res, next) {
  var sql = "SELECT COUNT(*) AS total FROM products WHERE store_id = '" +
  req.params.id + "';";

  runsql.query(sql, function(err, results){
    if(err){
      res.status(500).json(err); return;
    }

    res.status(200).json(results[0]);
  });
};

// Todas as visualizações.
controller.allViewsStore = function (req, res, next) {

  var sql = "SELECT SUM(views) AS total FROM products WHERE store_id = '" +
  req.params.id + "';";
  runsql.query(sql, function(err, results){
    if(err){
      res.status(500).json(err); return;
    }

    res.status(200).json(results[0]);
  });
};

// Todas as favoritos.
controller.allLoversStore = function (req, res, next) {
  var sql = 'SELECT COUNT(*) AS total FROM product_lover INNER JOIN ' +
  " products ON product_lover.product_id = products.id WHERE store_id = '"+req.params.id + "';";
  runsql.query(sql, function(err, results){
    if(err){
      res.status(500).json(err); return;
    }

    res.status(200).json(results[0]);
  });
};



// Valor das vendas mensal
controller.monthRequestsStore = function (req, res, next) {
  var sql = "SELECT sum(price * quantity) AS total, to_char(created_at,'Mon') As month FROM " +
  " requests INNER JOIN products_request ON requests.id =  " +
  " products_request.request_id WHERE store_id = '"+req.params.id+"' group by month LIMIT 7;";

  runsql.query(sql, function(err, results){
    if(err){
      res.status(500).json(err); return;
    }

    res.status(200).json(results);
  });
};

// retorna os valores baseado nos dias
controller.dailyRequestsStore = function (req, res, next) {
  var sql = "SELECT sum(price * quantity) AS total, to_char(created_at,'Day') As day FROM " +
  " requests INNER JOIN products_request ON requests.id =  " +
  " products_request.request_id WHERE store_id = '"+req.params.id+"' group by day LIMIT 7;";

  runsql.query(sql, function(err, results){
    if(err){
      res.status(500).json(err); return;
    }

    res.status(200).json(results);
  });
};


// Export controller
module.exports = controller;
