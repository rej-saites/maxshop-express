var jwt  = require('jsonwebtoken');
var postman = require('../lib/postman/postman.js');
var User = require('../models/user.js');
controller = {};

var privateFields = {
  password: 0
};

function generateUUID(){
    return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'
    .replace(/[xy]/g, function(c) {var r = Math.random()*16|0,v=c=='x'?r:r&0x3|0x8;return v.toString(16);});
}

/* GET /api/users
INDEX, retornando todos os usuários.*/
controller.index = function (req, res, next) {
  User.find({}, privateFields).sort({ "created_at": -1}).exec(function(err, data) {
    if (err) {
      console.log(err);
      return res.status(500).send();
    }

    res.status(200).json(data);
  });
}

/* GET /api/users/:id
 retorna um único usuário. */
controller.show = function (req, res, next) {
  var user_id = req.params.id;
  if(!user_id) return res.status(400).send();

  User.findOne({ _id: user_id }, privateFields, function (err, user) {
    if (err) {
      console.log(err);
      return res.status(500).send();
    }

    if (user) {
      res.status(200).json(user);
    } else {
      res.status(404).send();
    }
  });
}

/* POST /register
   Registrando um novo usuário do tipo client.*/
controller.create = function (req, res, next) {
  console.log("entrei");
  var data = req.body;
  data.role = "client";
  data.created_at = new Date();

  var user = new User(data);

  user.validate(function(err){
    console.log("validate");
    if(err) return res.status(400).send(err);
  });

  user.save(function(err, data) {
    console.log("user salvo");
    if (err) {
      console.log(err);
      return res.status(500).send();
    }

    res.status(200).send(data);
  });
};


/* POST /register/owner
   Registrando um novo usuário do tipo lojista.*/
controller.createOwner = function (req, res, next) {
  var data = {
    name: req.body.name,
    email: req.body.email,
    cdl: req.body.cdl
  };

  if(!data.name || !data.email || !data.cdl)
    return res.status(400).send();

  data.role = "owner";
  data.password = (Math.random().toString(36).slice(2));
  data.created_at = new Date();

  var user = new User(data);

  user.validate(function(err){
    if(err) return res.status(400).send(err);
  });

  user.save(function(err, data) {
    if (err) {
      console.log(err);
      return res.status(500).send();
    }

    var body = {
      email: data.email,
      subject: "Bem vindo ao CDL Shopping!"
    };

    // mandando email para usuário
    postman(body, 'new_account', data, function(err){
        if (err) { console.log(err); return res.status(500).send(); }
        return res.status(200).send();
    })

    // mandando email para Elthon
    postman({ email: 'elthon.maverick@gmail.com' , subject: "Novo cadastro de lojista."},
    'elthon', data, function(err){
        if (err) { console.log(err); return res.status(500).send(); }
        return res.status(200).send();
    })
  });
};

/* POST /authenticate
Method to authenticate the user not client */
controller.authenticate = function (req, res, next){
    // credentials to authenticate
    var email = req.body.email;
    var password = req.body.password;

    if(!email || !password) return res.status(400).send();

    var credentials = {
      email: email,
      password: password,
      role: {'$ne': 'client' }
    };

    User.findOne(credentials, privateFields, function (err, user) {
      if (err) {
        console.log(err);
        return res.status(500).send();
      }

      if (user) {
        var token = jwt.sign(user, 's558tuQ245a-33H-9t', { expiresInMinutes: 120 });
        res.status(200).json({ user: user, token: token});
      } else {
        res.status(401).send();
      }
    });
};


/* POST /authenticate-client
Method to authenticate the user */
controller.authenticateClient = function (req, res, next){
    // credentials to authenticate
    var email = req.body.email;
    var password = req.body.password;

    if(!email || !password) return res.status(400).send();

    var credentials = {
      email: email,
      password: password,
      role: {"$nin": [ "admin", "owner"] }
    };
    console.log(credentials);
    User.findOne(credentials, privateFields, function (err, user) {
      if (err) {
        console.log(err);
        return res.status(500).send();
      }

      if (user) {
        res.status(200).json(user);
      } else {
        res.status(401).send();
      }
    });
};

/* GET expire_auth
verifica se o token ainda é válido. */
controller.expireAuth = function (req, res, next) {
  res.status(200).send();
};

/* POST /showBySocial/user/:id
  Verificar se existe um usuário utilizando id social. */
controller.showBySocial = function (req, res, next){
    // credentials to authenticate
    var social_id = req.params.id;
    if(!social_id) return res.status(400).send();

    User.findOne({ social_id: social_id }, privateFields, function (err, user) {
      if (err) {
        console.log(err);
        return res.status(500).send();
      }

      if (user) {
        res.status(200).json(user);
      } else {
        res.status(404).send();
      }
    });
};



/* POST /api/duplicate_email/
  Verificar se existe um usuário utilizando id social. */
controller.duplicateEmail = function (req, res, next){
    // credentials to authenticate
    var email = req.body.email;
    if(!email) return res.status(400).send();

    User.findOne({ email: email }, function (err, user) {
      if (err) {
        console.log(err);
        return res.status(500).send();
      }

      if (user) {
        res.status(200).json({ unique: false });
      } else {
        res.status(200).json({ unique: true });
      }
    });
};

/* PUT /api/users/:id
  Atualizando usuário cliente. */
controller.update = function (req, res, next){
    var user_id = req.params.id;
    var data = req.body.user;

    if(!user_id || !data) return res.status(400).send();

    User.update({ _id: user_id }, data ,
      {} ,function (err, numAffected) {
      if (err) {
        console.log(err);
        return res.status(500).send();
      }

      if (numAffected.n != 0) {
        res.status(200).send(numAffected);
      } else {
        res.status(404).send(numAffected);
      }
    });
};

/* POST /api/update_user_client */
controller.updateUserClient = function (req, res, next){
    var user_id = req.body.id;
    var password = req.body.password;
    var name = req.body.name;

    if(!user_id || !password || !name) return res.status(400).send();

    User.update({ _id: user_id , role: 'client'}, { password: password , name: name } ,
      {} ,function (err, numAffected) {
      if (err) {
        console.log(err);
        return res.status(500).send();
      }

      if (numAffected.n != 0) {
        res.status(200).send(numAffected);
      } else {
        res.status(404).send(numAffected);
      }
    });
};

/* DELETE /api/users/:id
  remove usuário cliente. */
controller.delete = function (req, res, next){
    var user_id = req.params.id;
    if(!user_id) return res.status(400).send();

    User.remove({ _id: user_id }, function(err) {
      if (err) {
        console.log(err);
        return res.status(500).send();
      } else {
        return res.status(200).send();
      }
   });
};

/* Métodos referentes ao interesse (Lover)
=============================================================== */
/* GET /api/lover/:user/:product_id
  verifica interesse em produto */
controller.checkLover = function (req, res, next){
    var user_id = req.params.user;
    var product_id = req.params.product_id;

    if(!user_id || !product_id) return res.status(400).send();

    User.findOne({ _id: user_id, beloved_products: {  $in: [ product_id ] }},
    function (err, user) {
      if (err) {
        console.log(err);
        return res.status(500).send();
      }

      if (user) {
        res.status(200).send();
      } else {
        res.status(404).send();
      }
    });
};

/* POST /api/lover/:user/:product_id
  Atribui interesse em produto */
controller.addLover = function (req, res, next){
    var user_id = req.params.user;
    var product_id = req.params.product_id;

    if(!user_id || !product_id) return res.status(400).send();

    User.findOneAndUpdate({ _id: user_id },
        {$addToSet: { beloved_products: product_id }},
        {safe: true, upsert: true},
        function(err) {
          if (err) {
            console.log(err);
            return res.status(500).send();
          } else{
            return res.status(200).send();
          }
        }
    );
};

/* DELETE /api/lover/:user/:product_id
  remove interesse em produto */
controller.deleteLover = function (req, res, next){
    var user_id = req.params.user;
    var product_id = req.params.product_id;

    if(!user_id || !product_id) return res.status(400).send();

    User.findOneAndUpdate({ _id: user_id },
        {$pull: { beloved_products: product_id }},
        {safe: true, upsert: true},
        function(err) {
          if (err) {
            console.log(err);
            return res.status(500).send();
          } else{
            return res.status(200).send();
          }
        }
    );
};

/* Métodos referentes a Stores ( Lojas )
=============================================================== */

/* GET /api/stores/:id/user
 retorna um único usuário. */
controller.storeUser = function (req, res, next) {
  var store_id = req.params.id;
  if(!store_id) return res.status(400).send();

  User.findOne({ store_id: store_id, role: "owner" }, privateFields, function (err, user) {
    if (err) { console.log(err); return res.status(500).send(); }

    if (user) {
      res.status(200).json(user);
    } else {
      res.status(404).send();
    }
  });
}

/* Métodos que não são funções de requisições
================================================== */
controller.box = {};

// Função pra criar um usuário owner
controller.box.createOwner = function(data, done) {
  data.role = "owner";
  data.created_at = new Date();

  var user = new User(data);

  user.validate(function(err){
     if(err) done(err);
  });

  user.save(function(err, res) {
     if(err) done(err);
     done(null); // sucesso
  });
};

/* Métodos referentes aos cartões
=============================================================== */

/* GET /api/cards/:user
 retorna todos os cartões */
controller.cards = function (req, res, next) {
  var user_id = req.params.user;
  if(!user_id) return res.status(400).send();
  
  User.findOne({ _id: user_id }, function (err, user) {
    if (err) { console.log(err); return res.status(500).send(); }
    
    if (user) {
      
      var cards = user.cards.map(function(c){
        return {
          _id: c._id,
          card_holder_name: c.card_holder_name,
          card_number: c.card_number.replace(/^(\d{6})(\d{6})(\d{4})$/g, '$1******$3')
                                    .split(/([\d*]{4})/)
                                    .join(' ').trim().replace(/\s{2}/g, ' '),
          card_brand: c.card_brand,
          card_expiration: c.card_expiration_month + '/' + c.card_expiration_year
        };
      });
      
      res.status(200).json(cards);
    } else {
      res.status(404).send();
    }
  });
};

/* GET /api/card/:user
 retorna o cartão favorito */
controller.card = function (req, res, next) {
  var user_id = req.params.user;
  if(!user_id) return res.status(400).send();

  User.findOne({ _id: user_id }, function (err, user) {
    if (err) { console.log(err); return res.status(500).send(); }

    if (user) {
      var cards = user.cards;
      for (var i = 0; i < cards.length; i++) {
        if(cards[i].favorite == true)
          return res.status(200).json(cards[i]);
      }
      res.status(404).send();
    } else {
      res.status(404).send();
    }
  });
};

/* PUT /api/card/:user
 atualiza o cartão favorito */
controller.updateCard = function (req, res, next) {
  var user_id = req.params.user;
  var card_id = req.body.card_id;
  if(!user_id || !card_id) return res.status(400).send();

  User.findOne({ _id: user_id }, function (err, user) {
    if (err) { console.log(err); return res.status(500).send(); }

    if (user) {
      var cards = user.cards;
      var find = false;
      // procurando id sentando true and false
      for (var i = 0; i < cards.length; i++) {
        if(cards[i]._id == card_id) {
          find = true;
          cards[i].favorite = true;
        } else {
          cards[i].favorite = false;
        }
      }

      if(find){
        user.cards = cards;
        User.update({ _id: user._id }, user , {} ,function (err, numAffected) {
          if (err) { console.log(err); return res.status(500).send(); }
          res.status(200).send();
        });
      } else {
        res.status(404).send();
      }

    } else {
      res.status(404).send();
    }
  });
};

/* POST /api/cards/:user
 cria um novo cartão */
controller.createCard = function (req, res, next) {
  var user_id = req.params.user;
  console.log(req.params);
  var new_card = req.body.card;
  var favorite  = req.body.favorite || false;

  if(!user_id || !new_card) return res.status(400).send();

  User.findOne({ _id: user_id }, function (err, user) {
    if (err) { console.log(err); return res.status(500).send(); }

    if (user) {
      var cards = user.cards;
      var card = new_card;
      card._id = generateUUID();
      card.created_at = new Date();

      cards.push(card);

      user.cards = cards;
      User.update({ _id: user._id }, user , {} ,function (err, numAffected) {
        if (err) { console.log(err); return res.status(500).send(); }
        res.status(200).send();
      });

    } else {
      res.status(404).send();
    }
  });
};


/* DELETE /api/cards/:user
 remove um cartão */
controller.deleteCard = function (req, res, next) {
  var user_id = req.params.user;
  var card_id = req.query.card_id;

  if(!user_id || !card_id) return res.status(400).send();

  User.findOne({ _id: user_id }, function (err, user) {
    if (err) { console.log(err); return res.status(500).send(); }

    if (user) {
      var cards = user.cards;
      var find  = false;

      for (var i = 0; i < cards.length; i++) {
        if(cards[i]._id == card_id){
          cards.splice(i, 1);
          find = true;
        }
      }

      if(find == false ) return res.status(404).send();

      user.cards = cards;
      User.update({ _id: user._id }, user , {} ,function (err, numAffected) {
        if (err) { console.log(err); return res.status(500).send(); }
        res.status(200).send();
      });

    } else {
      res.status(404).send();
    }
  });
};

/* POST /api/forget_password */
controller.forget = function(req, res, next){
  if(!req.body.email) return res.status(400).send();

  User.findOne({ email : req.body.email }, function(err, user) {
    if (err) {console.log(err); return res.status(500).send(); }

    if(user){
      var body = {
        email: user.email,
        subject: "CDL Shopping: recuperar senha."
      };

      postman(body, 'forget_password', user, function(err){
          if (err) { console.log(err); return res.status(500).send(); }
          res.status(200).send();
      })
    } else {
      res.status(404).send();
    }
  });
};

/* PUT /api/change-password */
controller.changePassword = function(req, res, next){
  var user_id = req.body.id;
  var password = req.body.password;
  var new_password = req.body.new_password;

  if(!user_id || !password || !new_password )
    return res.status(400).send();

  User.findOne({ _id: user_id, password: password }, function(err, user) {
    if (err) { console.log(err); return res.status(500).send(); }

    if (user) {
      User.update({ _id: user._id }, { password: new_password } , {} ,
      function (err, numAffected) {
        if (err) { console.log(err); return res.status(500).send(); }
        res.status(200).send();
      });

    } else {
      res.status(401).send();
    }
  });
};

/* GET /api/users/:id/addresses */
controller.addresses = function (req, res, next) {
  var user_id = req.params.id;
  if(!user_id) return res.status(400).send();

  User.findOne({ _id: user_id }, function (err, user) {
    if (err) { console.log(err); return res.status(500).send(); }

    if (user) {
      console.log(user);
      return res.status(200).json(user.addresses || []);
    } else {
      res.status(404).send();
    }
  });
};

/* POST /api/users/:id/addresses */
controller.addAddress = function (req, res, next) {
  var user_id = req.params.id;
  console.log(req.params);
  console.log(req.body);
  if(!user_id) return res.status(400).send();

  User.findByIdAndUpdate(
        user_id,
        {$push: {"addresses": req.body}},
        {safe: true, new : true},
        function(err, model) {
          console.log('não deu');
            if (err) { console.log(err); return res.status(500).send(); }
            if(model){
              return res.status(200).send(model);
            }else{
              return res.status(404).send();
            }
        }
    );
};

module.exports = controller;
