var Store = require('../models/store.js');
var Product = require('../models/product.js');
var User = require('../models/user.js');
var Request = require('../models/request.js');
var UserCtrl = require('../controllers/user.js');
var image64 = require('../lib/image64.js');
var postman = require('../lib/postman/postman.js');

var controller = {};

/* GET /api/stores */
controller.index = function (req, res, next) {
  var page = req.query.page || 0;

  Store.find({ homologado: true })
  .skip(page * 20).limit(20).exec(function(err, data) {
    if (err) { console.log(err); return res.status(500).send(); }

    Store.find({ homologado: true }).count().exec(function(err, length) {
      res.header('pages', parseInt( (length + 19) /20 ) )
      res.header('stores', length )
      res.status(200).json(data);
    });

  });
}

/* GET /api/stores/:id */
controller.show = function (req, res, next) {
  var store_id = req.params.id;
  if(!store_id) return res.status(400).send();

  Store.findOne({ _id: store_id }, function (err, user) {
    if (err) { console.log(err); return res.status(500).send(); }

    if (user) {
      res.status(200).json(user);
    } else {
      res.status(404).send();
    }
  });
}

/* POST /api/stores
Criando uma nova loja.*/
controller.create = function (req, res, next) {
  // Definindo os dados do usuário.
  var userParams = req.body.user;
  userParams.role = "owner";
  userParams.password = (Math.random().toString(36).slice(2));
  userParams.created_at = new Date();

  // Definindo os valores da loja.
  var storeParams = req.body.store;
  storeParams.created_at = new Date();
  storeParams.avatar = {
    url: '/images/store.png',
    thumb: '/images/thumb_store.png'
  };

  // Criando uma instância da loja.
  var store = new Store(storeParams);

  // criando loja
  store.save(function(err, data) {
    if (err) { console.log(err); return res.status(500).send(); }

    User.findOne({ email: userParams.email }, {}, function (err, user) {
      if (err) { console.log(err); return res.status(500).send(); }

      if (user) {
        if(user.role == "owner" && !user.store_id){
          updateUser(user._id,data);
        } else {
          return res.status(400).send();
        }
      } else {
        saveUser(data);
      }
    });

    // Caso o usuário não esteja no banco de dados.
    function saveUser(store){
      // Definindo no usuário a loja associada a ele.
      userParams.store_id = store._id;
      // nova instância de usuário.
      var user = new User(userParams);
      // Salvando usuário.
      user.save(function(err, data) {
        if (err) { console.log(err); return res.status(500).send(); }

        var body = {
          email: data.email,
          subject: "Bem vindo ao CDL Shopping!"
        };

        // mandando email para usuário
        postman(body, 'new_account', data, function(err){
            if (err) { console.log(err); }
        })

        // mandando email para Elthon
        postman({ email: 'elthon.maverick@gmail.com' , subject: "Novo cadastro de lojista."},
        'elthon', data, function(err){
            if (err) { console.log(err); }
        })

        return res.status(200).send();
      });
    }

    // caso o usuário esteja no banco de dados.
    function updateUser(user_id, store){
      User.update({ _id: user_id }, { store_id: store._id } , {} , function (err, numAffected) {
        if (err) { console.log(err); return res.status(500).send(); }

        return res.status(200).send();
      });
    }

  });

};

/* PUT /api/stores/:id */
controller.update = function (req, res, next){
    var store_id = req.params.id;
    var data = req.body.store;

    if(!store_id || !data) return res.status(400).send();

    function done(){
      Store.update({ _id: store_id }, data , {} ,function (err, numAffected) {
        if (err) { console.log(err); return res.status(500).send(); }

        if (numAffected.n != 0) {
          // envia um email informando q os dados da loja foram alterados
          //storeOnChangeSendEmail(data);
          res.status(200).send(numAffected);
        } else {
          res.status(404).send(numAffected);
        }
      });
    }

    if(req.body.image){
      // transformando base64 em imagem.
      image64.decodeBase64Image(req.body.image, function(err, res){
        if (err) { console.log(err); return res.status(500).send(); }

        if(!data.avatar) data.avatar = {};
        data.avatar.url = res.url;
        data.avatar.thumb = res.thumb;
        done();
      });
    } else {
      done(); // chamada normal
    }
};


/* PUT /api/homologar/:id */
controller.homologar = function (req, res, next){
    var store_id = req.params.id;
    var pagarmeApiKey = req.body.pagarmeApiKey;

    if(!store_id) return res.status(400).send();

    var data = {
      homologado: true,
      visible: true
    };

    if(pagarmeApiKey) data.pagarmeApiKey = pagarmeApiKey;

    Store.update({ _id: store_id }, data , {} ,function (err, numAffected) {
      if (err) { console.log(err); return res.status(500).send(); }

      if (numAffected.n != 0) {
        //storeOnChangeSendEmail(data);
        Product.update({ store_id: store_id }, { visible: true }, {}, function () {
          res.status(200).send();
        });

        // encontrar o usuário da loja e mandar email.
        User.findOne({ store_id: store_id }, function (err, user) {
          if (err) { console.log(err); }
          // caso encontre o usuário.
          if (user) {
            var body = {
              email: user.email,
              subject: "Seja bem-vindo!"
            };

            // mandando email para usuário
            postman(body, 'homologar', {}, function(err){
              if (err) { return console.log(err); }
              console.log("E-mail enviado com sucesso.")
              console.dir(body);
            })
          }
        });

      } else {
        res.status(404).send();
      }
    });

};

/* DELETE /api/stores/:id */
controller.delete = function (req, res, next){
    var store_id = req.params.id;
    if(!store_id) return res.status(400).send();

    Product.update({ store_id: store_id }, { visible: false }, {}, function(){})

    Store.update({ _id: store_id }, { visible: false } , {} ,function (err, numAffected) {
      if (err) { console.log(err); return res.status(500).send(); }
      res.status(200).send();
    });
};

/* POST /api/duplicate_email/stores */
controller.duplicateEmail = function (req, res, next){
    // credentials to authenticate
    var email = req.body.email;
    if(!email) return res.status(400).send();

    Store.findOne({ email: email }, function (err, store) {
      if (err) {
        console.log(err);
        return res.status(500).send();
      }

      if (store) {
        res.status(200).json({ unique: false });
      } else {
        res.status(200).json({ unique: true });
      }
    });
};

/* GET /api/search/stores */
controller.search = function (req, res, next) {
  var query = req.query.query;
  if(!query) return res.status(400).send();

  Store.find({ 'name' : new RegExp(query, 'i'), visible: true, homologado: true })
  .limit(15).exec(function(err, data) {
    if (err) { console.log(err); return res.status(500).send(); }
    res.status(200).json(data);
  });
};

/* GET /api/homologar/stores
  Retorna uma lista de todas as lojas que precisam ser homologadas. */
controller.homologarStores = function (req, res, next) {
  var page = req.query.page || 0;

  Store.find({ homologado: false }).skip(page * 20).limit(20).exec(function(err, data) {
    if (err) { console.log(err); return res.status(500).send(); }

    Store.find({ homologado: false }).count().exec(function(err, length) {
      res.header('pages', parseInt( (length + 19) /20 ) )
      res.status(200).json(data);
    });
  });
};



controller.requests = function(req, res, next) {
  var store_id = req.params.id;

  if(!store_id) return res.status(400).send();

  Request.find({"store_id": store_id}).exec(function(err, data){
    if(err) { console.log(err); return res.status(500).send(); }
    res.status(200).json(data);
  });
};
/* FUNÇÕES PRIVATAS
========================================*/

 function storeOnChangeSendEmail(store){

  User.findOne({ store_id : store._id }, function(err, user) {
    if (err) {console.log(err); }

    if(user){
      var body = {
        email: user.email,
        subject: "CDL Shopping: alterações nos dados da loja."
      };

      postman(body, 'store_change', { store: store , name: user.name }, function(err){
          if (err) { console.log(err); }
      })
    }
  });
};

module.exports = controller;
