var Category = require('../models/category.js');
controller = {};

/* GET /api/categories */
controller.index = function (req, res, next) {
  Category.find({}).sort({ "name": "asc"}).exec(function(err, data) {
    if (err) { console.log(err); return res.status(500).send(); }
    res.status(200).json(data);
  });
}

/* POST /api/categories */
controller.create = function (req, res, next) {
  var data = req.body;
  data.created_at = new Date();
  var category = new Category(data);

  category.validate(function(err){
    if(err) return res.status(400).send(err);
  });

  category.save(function(err, data) {
    if (err) { return res.status(500).send(); }

    res.status(200).send();
  });
};

/* PUT /api/categories/:id */
controller.update = function (req, res, next){
    var category_id = req.params.id;
    var data = req.body.category;

    if(!category_id || !data) return res.status(400).send();

    Category.update({ _id: category_id }, data , {} ,function (err, numAffected) {
      if (err) { console.log(err); return res.status(500).send(); }

      if (numAffected.n != 0) {
        res.status(200).send(numAffected);
      } else {
        res.status(404).send(numAffected);
      }
    });
};


/* DELETE /api/categories/:id */
controller.delete = function (req, res, next){
    var category_id = req.params.id;
    if(!category_id) return res.status(400).send();

    Category.remove({ _id: category_id }, function(err) {
      if (err) { console.log(err); return res.status(500).send();
      } else {
        return res.status(200).send();
      }
   });
};

module.exports = controller;
