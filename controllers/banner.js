var Banner = require('../models/banner.js');
var image64 = require('../lib/image64.js');

controller = {};

/* GET /api/banners */
controller.index = function (req, res, next) {
  Banner.find({}).sort({ "created_at": -1}).exec(function(err, data){
    if (err) { console.log(err); return res.status(500).send(); }
    res.status(200).json(data);
  });
};


/* GET /api/banner/:id */
controller.show = function (req, res, next) {
  var banner_id = req.params.id;
  if(!banner_id) return res.status(400).send();

  Banner.findOne({ _id: banner_id }, function (err, user) {
    if (err) { console.log(err); return res.status(500).send(); }

    if (user) {
      res.status(200).json(user);
    } else {
      res.status(404).send();
    }
  });
}

/* GET /api/banners/:id
 Retorna todos os banners de ums loja. */
controller.bannersOfStore = function (req, res, next) {
  var store_id = req.params.id;
  if(!store_id) return res.status(400).send();

  Banner.find({ store_id: store_id }, function(err, data) {
    if (err) { console.log(err); return res.status(500).send(); }
    res.status(200).json(data);
  });
};

/* POST /api/banners*/
controller.create = function (req, res, next) {
  var data = req.body;
  data.created_at = new Date();

  function done(){
    var banner = new Banner(data);

    banner.validate(function(err){
      if(err) return res.status(400).send(err);
    });

    banner.save(function(err, data) {
      if (err) { console.log(err); return res.status(500).send(); }
      res.status(200).send();
    });
  }

  if(data.image){
    // transformando base64 em imagem.
    image64.decodeBase64Image(data.image, function(err, res){
      if (err) { console.log(err); return res.status(500).send(); }
      delete data.image;
      data.avatar = {};
      data.avatar.url = res.url;
      done();
    });
  } else {
    done(); // chamada normal
  }
};


/* PUT /api/stores/:id */
controller.update = function (req, res, next){
    var banner_id = req.params.id;
    var data = req.body.banner;

    if(!banner_id || !data) return res.status(400).send();

    function done(){
      Banner.update({ _id: banner_id }, data , {} ,function (err, numAffected) {
        if (err) { console.log(err); return res.status(500).send(); }

        if (numAffected.n != 0) {
          res.status(200).send(numAffected);
        } else {
          res.status(404).send(numAffected);
        }
      });
    }

    if(req.body.image){
      // transformando base64 em imagem.
      image64.decodeBase64Image(data.image, function(err, data){
        if (err) { console.log(err); return res.status(500).send(); }

        if(!data.avatar) data.avatar = {};
        data.avatar.url = data.url;
        done();
      });
    } else {
      done(); // chamada normal
    }
};

/* DELETE /api/stores/:id */
controller.delete = function (req, res, next){
    var banner_id = req.params.id;
    if(!banner_id) return res.status(400).send();

    Banner.remove({ _id: banner_id }, function(err) {
      if (err) {
        console.log(err); return res.status(500).send();
      } else {
        return res.status(200).send();
      }
   });
};

module.exports = controller;
