var Controller = require('./_controller.js');;
var model = require('../models/cart.js');

// Export controller
var controller = new Controller(model);

/* O index do carrinho de compras retorna todos os produtos
 que estão no carrinho de um usuário apenas.
*/
this.index = function (req, res, next) {
  model.index(req.params.id, function(result){
    res.status(result.status).json(result);
  });
}

module.exports = controller;
