var app = angular.module('my-app',['ngRoute','naif.base64', 'angular-loading-bar', 'ngMask']);

app.config(['$routeProvider',
  function($routeProvider) {
    var rp = $routeProvider;

    rp.when('/', {
      templateUrl: 'views/auth/login.html',
      controller: 'login'
    });

    rp.when('/esqueci-senha', {
      templateUrl: 'views/auth/esqueci.html',
      controller: 'EsqueciController'
    });

    rp.when('/products', {
      templateUrl: 'views/products/index.html',
      controller: 'ProdutoController'
    });

    rp.when('/products/new', {
      templateUrl: 'views/products/form.html',
      controller: 'NovoProdutoController'
    });

    rp.when('/products/edit/:id', {
      templateUrl: 'views/products/form.html',
      controller: 'EditarProdutoController'
    });

    // vendas para lojas
    rp.when('/vendas', {
      templateUrl: 'views/desativadas/vendas.html',
      controller: 'VendasController'
    });

    //
    rp.when('/vendas/visao_geral', {
      templateUrl: 'views/vendas-admin.html',
      controller: 'VendasAdminController'
    });

    rp.when('/stores/new', {
      templateUrl: 'views/stores/new.html',
      controller: 'CadastraLojaController'
    });

    rp.when('/stores/edit/:id', {
      templateUrl: 'views/stores/edit.html',
      controller: 'EditarLojaController'
    });

    rp.when('/stores', {
      templateUrl: 'views/stores/index.html',
      controller: 'ListaLojaController'
    });



    rp.when('/stores/show/:id', {
      templateUrl: 'views/stores/show.html',
      controller: 'LojaController'
    });

    rp.when('/store', {
      templateUrl: 'views/stores/show.html',
      controller: 'StoreRedirect'
    });

    rp.when('/relatorio/admin', {
      templateUrl: 'views/relatorio-admin.html',
      controller: 'RelatorioAdminController'
    });

    rp.when('/relatorio/store', {
      templateUrl: 'views/relatorio-store.html',
      controller: 'RelatorioStoreController'
    });

    rp.when('/promotions', {
      templateUrl: 'views/banners/admin/index.html',
      controller: 'PromotionsController'
    });

    rp.when('/promotions/new', {
      templateUrl: 'views/banners/admin/new.html',
      controller: 'NewPromotionController'
    });


    rp.when('/categories', {
      templateUrl: 'views/categories/index.html',
      controller: 'CategoriesController'
    });

    rp.when('/register', {
      templateUrl: 'views/stores/register.html',
      controller: 'RegisterController'
    });

    rp.when('/homologar', {
      templateUrl: 'views/stores/homologar.html',
      controller: 'HomologarController'
    });

    rp.when('/homologar/store/:id', {
      templateUrl: 'views/stores/homologar_store.html',
      controller: 'HomologarStoreController'
    });

    rp.when('/suporte', {
      templateUrl: 'views/faq/suporte.html',
      controller: 'CategoriesController'
    });

    rp.when('/faq', {
      templateUrl: 'views/faq/index.html',
      controller: 'CategoriesController'
    });

    rp.otherwise({
      redirectTo: '/'
    });

  }]);


app.constant("PAYMENT_GATEWAY","http://proxy.maxshoplus.com.br");

app.run(function($rootScope, $window, $http, User){
  $rootScope.username = localStorage.getItem("username");
  $rootScope.email = localStorage.getItem("email");
  var token = localStorage.getItem("token");

  if(token) {
    $http.defaults.headers.common['Authorization'] = 'Bearer ' + token;
  }

  $rootScope.signOut = function(){
    $rootScope.changePassword = false;
    localStorage.clear();
    $window.location = "#/";
  };

  $rootScope.editPassword = function(){
    $rootScope.changePassword = true;
  };

  $rootScope.closeEditPassword = function(){
    $rootScope.changePassword = false;
  };

  $rootScope.updatePassword = function(){
    var elm1 = document.getElementById('password');
    var elm2 =  document.getElementById('new_password');

    var data = {
      id: localStorage.getItem('user_id'),
      password: elm1.value,
      new_password: elm2.value,
    };

    if(!data.password){
      elm1.focus();
      elm1.style.outline = '1px solid red';
      return;
    }

    elm1.style.outline = 'none';

    if(!data.new_password){
      elm2.focus();
      elm2.style.outline = '1px solid red';
      return;
    }

    elm2.style.outline = 'none';

    User.changePassword(data).then(function(){
      alert('Senha alterada com sucesso!');
      $rootScope.changePassword = false;
      elm1.value = '';
      elm2.value = '';
    }, function(){
      alert('Problema com a senha atual, verifique e tente novamente!');
    });
  };

});


function TestaCPF(strCPF) {
    strCPF = strCPF.replace(/[^\d]+/g, '');
    var Soma;
    var Resto;
    Soma = 0;
	if (strCPF == "00000000000") return false;

	for (i=1; i<=9; i++) Soma = Soma + parseInt(strCPF.substring(i-1, i)) * (11 - i);
	Resto = (Soma * 10) % 11;

    if ((Resto == 10) || (Resto == 11))  Resto = 0;
    if (Resto != parseInt(strCPF.substring(9, 10)) ) return false;

	Soma = 0;
    for (i = 1; i <= 10; i++) Soma = Soma + parseInt(strCPF.substring(i-1, i)) * (12 - i);
    Resto = (Soma * 10) % 11;

    if ((Resto == 10) || (Resto == 11))  Resto = 0;
    if (Resto != parseInt(strCPF.substring(10, 11) ) ) return false;
    return true;
}


function validarCNPJ(cnpj) {

	cnpj = cnpj.replace(/[^\d]+/g,'');

	if(cnpj == '') return false;

	if (cnpj.length != 14)
		return false;

	// Elimina CNPJs invalidos conhecidos
	if (cnpj == "00000000000000" ||
		cnpj == "11111111111111" ||
		cnpj == "22222222222222" ||
		cnpj == "33333333333333" ||
		cnpj == "44444444444444" ||
		cnpj == "55555555555555" ||
		cnpj == "66666666666666" ||
		cnpj == "77777777777777" ||
		cnpj == "88888888888888" ||
		cnpj == "99999999999999")
		return false;

	// Valida DVs
	tamanho = cnpj.length - 2
	numeros = cnpj.substring(0,tamanho);
	digitos = cnpj.substring(tamanho);
	soma = 0;
	pos = tamanho - 7;
	for (i = tamanho; i >= 1; i--) {
	  soma += numeros.charAt(tamanho - i) * pos--;
	  if (pos < 2)
			pos = 9;
	}
	resultado = soma % 11 < 2 ? 0 : 11 - soma % 11;
	if (resultado != digitos.charAt(0))
		return false;

	tamanho = tamanho + 1;
	numeros = cnpj.substring(0,tamanho);
	soma = 0;
	pos = tamanho - 7;
	for (i = tamanho; i >= 1; i--) {
	  soma += numeros.charAt(tamanho - i) * pos--;
	  if (pos < 2)
			pos = 9;
	}
	resultado = soma % 11 < 2 ? 0 : 11 - soma % 11;
	if (resultado != digitos.charAt(1))
		  return false;

	return true;

}
app.filter('date', function() {
  return function(input) {
    return moment(input).format("DD/MM/YYYY, h:mm:ss a");
  };
});

app.filter('status', function() {
  return function(input) {
    switch (input) {
      case 'waiting_payment': 
      case 'processing' : return 'Aguardando pagamento';
      case 'paid'       : return 'Pago';
      case 'refunded'   : return 'Estornado';
      case 'completed'  : return 'Finalizado';
      case 'canceled'   : return 'Cancelado';
      case '1':
        return 'Aguardando pagamento';
      case '2':
        return 'Em análise';
      case '3':
        return 'Paga';
      case '4':
        return 'Disponível';
      case '5':
        return 'Em disputa';
      case '6':
        return 'Devolvida';
      case '7':
        return 'Cancelada';
      case '8':
        return 'Chargeback debitado';
      case '9':
        return 'Em contestação';
      default:
        return 'Indefinido'
    }
  };
});


moment.locale('pt-br', {
        months : 'janeiro_fevereiro_março_abril_maio_junho_julho_agosto_setembro_outubro_novembro_dezembro'.split('_'),
        monthsShort : 'jan_fev_mar_abr_mai_jun_jul_ago_set_out_nov_dez'.split('_'),
        weekdays : 'domingo_segunda-feira_terça-feira_quarta-feira_quinta-feira_sexta-feira_sábado'.split('_'),
        weekdaysShort : 'dom_seg_ter_qua_qui_sex_sáb'.split('_'),
        weekdaysMin : 'dom_2ª_3ª_4ª_5ª_6ª_sáb'.split('_'),
        longDateFormat : {
            LT : 'HH:mm',
            L : 'DD/MM/YYYY',
            LL : 'D [de] MMMM [de] YYYY',
            LLL : 'D [de] MMMM [de] YYYY [às] LT',
            LLLL : 'dddd, D [de] MMMM [de] YYYY [às] LT'
        },
        calendar : {
            sameDay: '[Hoje às] LT',
            nextDay: '[Amanhã às] LT',
            nextWeek: 'dddd [às] LT',
            lastDay: '[Ontem às] LT',
            lastWeek: function () {
                return (this.day() === 0 || this.day() === 6) ?
                    '[Último] dddd [às] LT' : // Saturday + Sunday
                    '[Última] dddd [às] LT'; // Monday - Friday
            },
            sameElse: 'L'
        },
        relativeTime : {
            future : 'em %s',
            past : '%s atrás',
            s : 'segundos',
            m : 'um minuto',
            mm : '%d minutos',
            h : 'uma hora',
            hh : '%d horas',
            d : 'um dia',
            dd : '%d dias',
            M : 'um mês',
            MM : '%d meses',
            y : 'um ano',
            yy : '%d anos'
        },
        ordinal : '%dº'
    });

moment.locale('pt-br');

app.filter('date', function() {
  return function(input) {
    return moment(input).format("DD/MM/YYYY, h:mm:ss a");
  };
});

app.filter('truncate',function(){
  return function(string){
    return (string.length > 75) ? string.substr(0,75-1)+'...' : string;
  };
});

app.filter('money',function(){
  return function(string){
    string = String(string || 0);
    console.log('< ' + string);

    if(/\,/.test(string)){
      string = string.replace('.','');
      string = string.replace(',','.');
    }

    function decimalToHuman(value){
      var parts = String(value).split('.');
      // adiconand ponto a cada 3 casas.
      parts[0] = parts[0].replace(/\d{1,3}(?=(\d{3})+(?!\d))/g, "$&.");
      return parts[0] + ',' + parts[1];
    }


    var output =  decimalToHuman(Number(string).toFixed(2));
    console.log('>  ' + output);
    return output;
  };
});

app.filter('moneyFromPure',function(){
  return function(string){

    function formaterPrice(value){
      value = String(value || 0);
      value = value.replace(/\.|\,/g,'');

      if(value.length >= 3 ){
        var start = value.substring(0, value.length - 2);
        var end = value.substring(value.length - 2, value.length);
        return Number(start + '.' + end).toFixed(2);
      } else{
        return Number(value).toFixed(2);
      }
    };

    string = formaterPrice(string);

    console.log('< ' + string);

    if(/\,/.test(string)){
      string = string.replace('.','');
      string = string.replace(',','.');
    }

    function decimalToHuman(value){
      var parts = String(value).split('.');
      // adiconand ponto a cada 3 casas.
      parts[0] = parts[0].replace(/\d{1,3}(?=(\d{3})+(?!\d))/g, "$&.");
      return parts[0] + ',' + parts[1];
    }


    var output =  decimalToHuman(Number(string).toFixed(2));
    console.log('>  ' + output);
    return output;
  };
});

app.filter('joinNames',function(){
  return function(array){
    if(array) {
      return array.map(function(data){
        return data.name
      }).join(', ');
    } else {
      return array;
    }
  };
});

/* Botão pra enviar ajax, animação */
app.directive('maxBanner', function(Store, Product){
  return {
    restrict: 'E',
    transclude: true,
    scope: {},
    templateUrl: './views/templates/directives/banner.html',
    link: function($scope, element, attrs) {

      function safeApply(scope, fn) {
          (scope.$$phase || scope.$root.$$phase) ? fn() : scope.$apply(fn);
      }

     var objs = [
       {
         type: 'image',
         x: 120,
         y: 160,
         width: '100px',
         height: '100px',
         src: 'http://maxshop-redhateste.rhcloud.com/uploads/n0sredeb3ej0pb9ufr89edqpzy6i529.png'
       },
       {
         type: 'image',
         x: 50,
         y: 50,
         width: '100px',
         height: '100px',
         src: 'http://maxshop-redhateste.rhcloud.com/uploads/86n2tpa0e6sqncdi8o57y0t0flvm42t9.png'
       },
       {
         type: 'textarea',
         x: 100,
         y: 200,
         text: 'Hello World',
         size: '18px',
         font: 'Arial',
         align: 'left',
         width: '100px',
         height: '100px',
         color: '#333'
       }
     ];

     $scope.layers = [];

     $scope.textElement = {};

     $scope.trigerElement = function(i){
       $scope.layers[i].elm.click();
     };

     $scope.setAlign = function(name){
       $scope.textElement.style.textAlign = name;
     }

     var editor = document.getElementById('banner-editor');

     var arrow = document.getElementById('scale-elements');
     var arrowdrag = false;
     var targetarrow = null;
     var pointstart = 0;

     var arrowRight = document.getElementById('scaleX');
     var arrowX = false;

     var arrowTop = document.getElementById('scaleY');
     var arrowY = false;

     var textEditor = document.getElementById('editor-text');

     function cleanOptions(){
       if(targetarrow) targetarrow.style['border'] = 'none';
       if(dragObj) dragObj.style['border'] = 'none';

       arrow.style['display'] = 'none';
       arrowRight.style['display'] = 'none';
       arrowTop.style['display'] = 'none';
       move.style['display'] = 'none';
       textEditor.style['display'] = 'none';
     }

     var move = document.getElementById('move');
     move.onmousedown = function(e){
       diff_x = (e.pageX - editor.offsetLeft) - targetarrow.offsetLeft;
       diff_y = (e.pageY - editor.offsetTop) - targetarrow.offsetTop;
       cleanOptions();
       draggable(targetarrow);
       targetarrow.style['border'] = '1px dashed #23bef7';
     };

     arrow.onmousedown = function(e){
       targetarrow.style['border'] = '1px dashed #23bef7';

       targetWidth = targetarrow.clientWidth;
       targetHeight = targetarrow.clientHeight;

       arrowdrag = true;
       diff_y = (e.pageY - editor.offsetTop) - arrow.offsetTop;
       diff_x = (e.pageX - editor.offsetLeft) - arrow.offsetLeft;
       pointstart = e.pageY;
     };

     arrowRight.onmousedown = function(e){
       targetarrow.style['border'] = '1px dashed #23bef7';

       targetWidth = targetarrow.clientWidth;
       targetHeight = targetarrow.clientHeight;

       arrowX = true;
       diff_x = (e.pageX - editor.offsetLeft) - arrow.offsetLeft;
       pointstart = e.pageX;
     };

     arrowTop.onmousedown = function(e){
       targetarrow.style['border'] = '1px dashed #23bef7';

       targetWidth = targetarrow.clientWidth;
       targetHeight = targetarrow.clientHeight;

       arrowY = true;
       diff_y = (e.pageY - editor.offsetTop) - arrow.offsetTop;
       pointstart = e.pageY;
     };

     function createElement(obj){
       switch (obj.type) {
         case 'image':
          (function(){
             var elm = new Image(obj.width, obj.height);
             elm.src = obj.src;
             elm.style['position'] = 'absolute';
             elm.style['left'] = obj.x + 'px';
             elm.style['top']  = obj.y + 'px';
             elm.style['width']  = obj.width;
             elm.style['height']  = obj.height;

             elm.ondragstart = function() { return false; };
             elm.onmousedown = function(e){
               diff_x = (e.pageX - editor.offsetLeft) - elm.offsetLeft;
               diff_y = (e.pageY - editor.offsetTop) - elm.offsetTop;
               cleanOptions();
               draggable(elm);
               elm.style['border'] = '1px dashed #23bef7';
             };

             elm.onclick = function(){
               cleanOptions();
               elm.style['border'] = '1px dashed #23bef7';

               targetarrow = elm;
               targetWidth = elm.clientWidth;
               targetHeight = elm.clientHeight;

               arrow.style['display'] = 'block';
               arrow.style['left'] = (elm.offsetLeft + elm.clientWidth - 5) + 'px';
               arrow.style['top'] = (elm.offsetTop + elm.clientHeight - 5) + 'px';
             }

             $scope.layers.push({
               name: 'Imagem ',
               elm: elm,
               type: 'image'
             });

             editor.appendChild(elm);
          })();
           break;

          case 'textarea':
          (function(){
            var elm = document.createElement("textarea");
            elm.style['position'] = 'absolute';
            elm.style['left'] = obj.x + 'px';
            elm.style['top']  = obj.y + 'px';
            elm.style['width']  = obj.width;
            elm.style['height']  = obj.height;
            elm.style['font-size']  = obj.size;
            elm.style['color']  = obj.color;
            elm.style['font-family']  = obj.font;
            elm.style['text-align']  = obj.align;
            elm.value =  obj.text;


            elm.onclick = function(){
              cleanOptions();
              elm.style['border'] = '1px dashed #23bef7';

              targetarrow = elm;
              targetWidth = elm.clientWidth;
              targetHeight = elm.clientHeight;

              textEditor.style['display'] = 'block';
              $scope.textElement = elm;
              safeApply($scope,function(){});

              arrowRight.style['display'] = 'block';
              arrowRight.style['left'] = (elm.offsetLeft + elm.clientWidth - 5) + 'px';
              arrowRight.style['top'] = (elm.offsetTop + (elm.clientHeight / 2) - 7) + 'px';

              arrowTop.style['display'] = 'block';
              arrowTop.style['left'] = (elm.offsetLeft + (elm.clientWidth / 2) - 5) + 'px';
              arrowTop.style['top'] = (elm.offsetTop + elm.clientHeight - 5) + 'px';

              move.style['display'] = 'block';
              move.style['left'] = (elm.offsetLeft + elm.clientWidth - 5) + 'px';
              move.style['top'] = (elm.offsetTop + elm.clientHeight - 5) + 'px';
            }

            $scope.layers.push({
              name: 'Texto ',
              elm: elm,
              type: 'textarea'
            });

            editor.appendChild(elm);
          })();
          break;
       }
     }

     for (var i = 0; i < objs.length; i++) {
       createElement(objs[i]);
     }

     var dragObj = null;
     var diff_x = 0, diff_y = 0;

     function draggable(elm){
          dragObj = elm;
      }

      editor.onmouseup = function(e){
          if(dragObj){
            dragObj.style['border'] = 'none';
          }

          dragObj = null;
          arrowdrag = false;
          arrowX = false;
          arrowY = false;
          safeApply($scope,function(){});
      };

      editor.onmousemove = function(e){
          if(dragObj != null){
            var x = e.pageX - editor.offsetLeft - diff_x;
            var y = e.pageY - editor.offsetTop - diff_y;
            dragObj.style.left = x +"px";
            dragObj.style.top= y +"px";
          }

          if(arrowdrag){
            var diff = (e.pageY - editor.offsetTop) - (pointstart - editor.offsetTop);

            targetarrow.style['width'] = (targetWidth + diff) + 'px';
            targetarrow.style['height'] = (targetHeight + diff) + 'px';
            arrow.style.left = (targetarrow.offsetLeft + targetarrow.clientWidth - 5) + "px";
            arrow.style['top'] = (targetarrow.offsetTop + targetarrow.clientHeight - 5) + 'px';
          }

          if(arrowX){
            var diff = (e.pageX - editor.offsetLeft) - (pointstart - editor.offsetLeft);
            targetarrow.style['width'] = (targetWidth + diff) + 'px';

            arrowRight.style['left'] = (targetarrow.offsetLeft + targetarrow.clientWidth - 5) + 'px';
            arrowRight.style['top'] = (targetarrow.offsetTop + (targetarrow.clientHeight / 2) - 7) + 'px';

            arrowTop.style['display'] = 'block';
            arrowTop.style['left'] = (targetarrow.offsetLeft + (targetarrow.clientWidth / 2) - 5) + 'px';
            arrowTop.style['top'] = (targetarrow.offsetTop + targetarrow.clientHeight - 5) + 'px';

            move.style['display'] = 'block';
            move.style['left'] = (targetarrow.offsetLeft + targetarrow.clientWidth - 5) + 'px';
            move.style['top'] = (targetarrow.offsetTop + targetarrow.clientHeight - 5) + 'px';
          }

          if(arrowY){
            var diff = (e.pageY - editor.offsetTop) - (pointstart - editor.offsetTop);
            targetarrow.style['height'] = (targetHeight + diff) + 'px';

            arrowRight.style['left'] = (targetarrow.offsetLeft + targetarrow.clientWidth - 5) + 'px';
            arrowRight.style['top'] = (targetarrow.offsetTop + (targetarrow.clientHeight / 2) - 7) + 'px';

            arrowTop.style['left'] = (targetarrow.offsetLeft + (targetarrow.clientWidth / 2) - 5) + 'px';
            arrowTop.style['top'] = (targetarrow.offsetTop + targetarrow.clientHeight - 5) + 'px';

            move.style['display'] = 'block';
            move.style['left'] = (targetarrow.offsetLeft + targetarrow.clientWidth - 5) + 'px';
            move.style['top'] = (targetarrow.offsetTop + targetarrow.clientHeight - 5) + 'px';
          }
      };


      // escolher uma imagem.
      var fileInput =   document.getElementById('file');

      fileInput.onchange = function(evt){
          var file = evt.currentTarget.files[0];
          var reader = new FileReader();

          reader.onload = function (evt) {
            createElement({
              type: 'image',
              x: 150,
              y: 150,
              width: '100px',
              height: 'auto',
              src: evt.target.result
            });

            safeApply($scope,function(){});
          };

          reader.readAsDataURL(file);
      };

      $scope.pickImage = function(){
        fileInput.click();
      };

      $scope.addText = function(){
        createElement({
          type: 'textarea',
          x: 150,
          y: 150,
          text: 'Novo texto!',
          size: '18px',
          font: 'Arial',
          align: 'left',
          width: '120px',
          height: '50px',
          color: '#333'
        });
      };

      $scope.remove = function(i){
        editor.removeChild($scope.layers[i].elm);
        $scope.layers.splice(i, 1);
      };

      $scope.ganerate = function(){
        arrow.style['display'] = 'none';
        arrowRight.style['display'] = 'none';
        arrowTop.style['display'] = 'none';
        move.style['display'] = 'none';
        textEditor.style['display'] = 'none';

        for (var i = 0; i < $scope.layers.length; i++) {
          $scope.layers[i].elm.style.border = 'none';
        }

        html2canvas(editor, {
           onrendered: function(canvas) {
               Canvas2Image.saveAsPNG(canvas, 640, 300);
           }
       });
      };

    // end link function
    }
  }
});
/* Botão pra enviar ajax, animação */
app.directive('maxButtonSend', function(Store, Product){
  return {
    restrict: 'E',
    transclude: true,
    scope: {
      action: "&"
    },
    templateUrl: './views/templates/directives/button_send.html',
    link: function($scope, element, attrs) {

      window.addEventListener('buttonsend.fail', function() {
        $scope.loading = false;
      });

      $scope.send = function(){
        $scope.loading = true;
        $scope.action();
      };
    }
  }
})
app.directive('ngEnter', function() {
  return function(scope, element, attrs) {
      element.bind("keydown keypress", function(event) {
          if(event.which === 13) {
                  scope.$apply(function(){
                          scope.$eval(attrs.ngEnter);
                  });

                  event.preventDefault();
          }
      });
  };
});
app.factory('Auth', function($window, $http){
  var factory = {};

  function expireAuth(done){
    $http.get('/expire_auth').then(function(){
      done();
    },
    function(res){
      console.log(res.status);
      localStorage.clear();
      $window.location = "#/";
    })
  }

  factory.role = function(role){
    var user_role =  localStorage.getItem("role");

    expireAuth(function(){
      /* verificando se store_id está presente.
       caso de falha suponha-se que o usuário não inicio o
       cadastro dos dados da loja. */
       if(user_role != 'admin'){
         var store_id = localStorage.getItem('store_id');
        if(store_id == 'undefined' || !store_id) return $window.location = "#/register";
      }

      // Verificando permissões de usuários
      if(role.constructor === Array){
        var macth = false;
        for (var i = 0; i < role.length; i++) {
          if(role[i] == user_role){
            macth = true;
          }
        }

        if(macth == false)
          $window.location = "/401.html";
      } else {
        if(user_role != role)
          $window.location = "/401.html";
      }
    });

  };

  factory.isRole = function(role){
    var user_role =  localStorage.getItem("role");

    if(role.constructor === Array){
      var macth = false;
      for (var i = 0; i < role.length; i++) {
        if(role[i] == user_role){
          macth = true;
        }
      }

      return macth;
    } else {
      if(user_role != role){
        return false;
      } else{
        return true;
      }
    }
  };

  return factory;
});
app.factory('Banner', function($http, $q){
  var factory = {};

  factory.create = function(data){
    var q = $q.defer();

    $http.post('/api/banners', data).then(function(result){
      q.resolve(result); // sucesso
    }, function(){
      q.reject(); // caso de erro
    });

    return q.promise;
  };

  factory.index = function(){
    var q = $q.defer();

    $http.get('/api/banners/').then(function(res){
      q.resolve(res.data); // sucesso
    }, function(){
      q.reject(); // caso de erro
    });

    return q.promise;
  };

  factory.get = function(id){
    var q = $q.defer();

    $http.get('/api/banner/' + id).then(function(res){
      q.resolve(res.data); // sucesso
    }, function(){
      q.reject(); // caso de erro
    });

    return q.promise;
  };

  factory.delete = function(id) {
    var q = $q.defer();

    $http.delete('/api/banners/' + id).then(function(result){
      q.resolve(result); // sucesso
    }, function(){
      q.reject(); // caso de erro
    });

    return q.promise;
  };

  return factory;
});
app.factory('Category', function($http, $q){
  var factory = {};

  factory.index = function(){
    var q = $q.defer();

    $http.get('/api/categories').then(function(result){
      q.resolve(result.data); // sucesso
    }, function(){
      q.reject(); // caso de erro
    });

    return q.promise;
  };

  factory.delete = function(id){
    var q = $q.defer();

    $http.delete('/api/categories/'+id).then(function(result){
      q.resolve(result.data.results); // sucesso
    }, function(){
      q.reject(); // caso de erro
    });

    return q.promise;
  };


  factory.create = function(data){
    var q = $q.defer();

    $http.post('/api/categories', data).then(function(res){
      q.resolve(res); // sucesso
    }, function(res){
      q.reject(res); // caso de erro
    });

    return q.promise;
  };

  factory.update = function(id, data){
    var q = $q.defer();

    $http.put('/api/categories/'+ id, data).then(function(result){
      q.resolve(result); // sucesso
    }, function(data){
      q.reject(data); // caso de erro
    });

    return q.promise;
  };

  return factory;
});
app.directive('maxSlider', function() {
  return {
    restrict: 'E',
    require: 'ngModel',
    templateUrl: './views/templates/directives/slider.html',
    link: function($scope, element, attrs, ngModel) {

      var selected = null, // Object of the element to be moved
      x_pos = 0, y_pos = 0; // Stores x & y coordinates of the mouse pointer

      // Will be called when user starts dragging an element
      function _drag_init(elem) {
        // Store the object of the element which needs to be moved
        selected = elem;
        x_elem = x_pos - selected.offsetLeft;
      }

      var slider = angular.element('.slider')[0];
      var fill = angular.element('.fill')[0];
      var value = angular.element('.value')[0];

      setTimeout(function () {
        var days = ngModel.$modelValue;
        // aplicando mudanças baseada no valor atual de days
        var px = (((slider.clientWidth-9) * days) / 100) + 'px';
        angular.element('.handler')[0].style.left = px;
        fill.style.width = px;
        value.style.left = ((((slider.clientWidth-9) * days) / 100) + 20) + 'px';
        if(days == 1) {
            value.innerHTML = '1 dia';
        } else {
          value.innerHTML = days + ' dias';
        }
      }, 10);


      // Will be called when user dragging an element
      function _move_elem(e) {
        x_pos = document.all ? window.event.clientX : e.pageX;
        if (selected !== null) {
            var x = (x_pos - x_elem)
            if((x_pos - x_elem) < 0) x = 0;
            if((x_pos - x_elem) > slider.clientWidth-9) x = slider.clientWidth - 9;
            selected.style.left = x + 'px';
            fill.style.width = x + 'px';
            // value
            value.style.left = (x + 20) + 'px';
            value.style.color = '#333';
            if(x + 60 > (slider.clientWidth-9)){
              value.style.left = (x - 60) + 'px';
              value.style.color = '#fff';
            }

            days = Math.floor((( x * 100 ) / (slider.clientWidth-9)));
            ngModel.$setViewValue(days);

            if(days == 1) {
                value.innerHTML = '1 dia';
            } else {
              value.innerHTML = days + ' dias';
            }

        }
      }

      // Destroy the object when we are done
      function _destroy() {
        selected = null;
      }

      // Bind the functions...
      angular.element('.handler')[0].onmousedown = function () {
        _drag_init(this);
        return false;
      };

      document.onmousemove = _move_elem;
      document.onmouseup = _destroy;
    }
  };
});
/* Componente que faz pesquisa entre produtos e lojas.. sendo que pode selecionar
apenas um elemento dessas duas entidades. Feito primordiamente para ser usado
na telas banners, mas pode vir a ser útil em alguma outra situação. */
app.directive('maxTwoSearch', function(Store, Product){
  return {
    restrict: 'E',
    require: 'ngModel',
    templateUrl: './views/templates/directives/two_search.html',
    link: function($scope, element, attrs, ngModel) {
      // DOM element: Div of product search
      var elm_product = angular.element('.two-tab-product')[0];
      var elm_result_search_store = angular.element('.result-search')[0];
      var elm_result_search_product = angular.element('.result-search')[1];

      $scope.model = {};
      $scope.checkbox_store = false;

      setTimeout(function () {
        $scope.model = ngModel.$modelValue;

        if($scope.model.store_id)
        Store.get($scope.model.store_id).then(function(data){
          $scope.store = data;
        });

        if($scope.model.product_id)
        Product.get($scope.model.product_id).then(function(data){
          $scope.product = data;
        });

        $scope.$apply(function(){
          $scope.checkbox_store = ngModel.$modelValue.store_action;
        });
      }, 10);



      $scope.$watch('checkbox_store', function(){
        $scope.model.store_action = $scope.checkbox_store;

        if($scope.checkbox_store == true){
          elm_product.style['pointer-events'] = 'none';
          elm_product.style['opacity'] = '0.5';
        } else {
          elm_product.style['pointer-events'] = 'auto';
          elm_product.style['opacity'] = '1';
        }
      });

      $scope.store_query = "";
      $scope.store_results = [];

      $scope.storeSearch = function(){
        $scope.store = null;
        Store.search($scope.store_query).then(function(res){
          $scope.isStoreSearch = true;
          $scope.store_results = res;

          if(res.length > 0) elm_result_search_store.style.height = '150px';
        });
      };

      $scope.setStore = function(index){
        $scope.store =  $scope.store_results[index];
        $scope.isStoreSearch = false;
        elm_result_search_store.style.height = '60px';

        $scope.model.store_id = $scope.store._id
      };

      $scope.product_query = "";
      $scope.product_results = [];
      $scope.productSearch = function(){
        $scope.product = null;
        Product.search($scope.product_query).then(function(res){
          $scope.isProductSearch = true;
          $scope.product_results = res;
          console.log(res);

          if(res.length > 0) elm_result_search_product.style.height = '150px';
        });
      };

      $scope.setProduct = function(index){
        $scope.product =  $scope.product_results[index];
        $scope.isProductSearch = false;
        elm_result_search_product.style.height = '60px';

        $scope.model.product_id = $scope.product._id
      };

    }
  };
});
app.factory('Product', function($http, $q){
  var factory = {};

  factory.index = function(page, store_id){
    var q = $q.defer();
    if(!page) page = 1;

    $http.get('/api/stores/'+store_id+'/products?page='+ (page - 1 )).then(function(res){
      var pages = res.headers().pages;
      q.resolve({ data: res.data, pages: pages }); // sucesso
    }, function(){
      q.reject(); // caso de erro
    });

    return q.promise;
  };

  factory.get = function(id){
    var q = $q.defer();

    $http.get('/api/products/'+ id).then(function(result){
      q.resolve(result.data); // sucesso
    }, function(){
      q.reject(); // caso de erro
    });

    return q.promise;
  };

  function formaterPrice(value){
    value = String(value);
    value = value.replace(/\.|\,/g,'');

    if(value.length >= 3 ){
      var start = value.substring(0, value.length - 2);
      var end = value.substring(value.length - 2, value.length);
      return Number(start + '.' + end).toFixed(2);
    } else{
      return Number(value).toFixed(2);
    }
  };

  factory.create = function(data) {
    var q = $q.defer();

    data.price =  formaterPrice(data.price);

    if(data.old_price) data.old_price =  formaterPrice(data.old_price);
    if(isNaN(data.old_price)) data.old_price = undefined ;

    $http.post('/api/products', data).then(function(result){
      q.resolve(result); // sucesso
    }, function(data){
      q.reject(data); // caso de erro
    });

    return q.promise;
  };

  factory.update = function(id, data) {
    var q = $q.defer();

    data.product.price =  formaterPrice(data.product.price);
    if(data.product.old_price) data.product.old_price =  formaterPrice(data.product.old_price);
    if(isNaN(data.product.old_price)) data.product.old_price = undefined ;

    console.log('price: ' + data.product.price);
    console.log('old_price: ' + data.product.old_price);

    $http.put('/api/products/' + id, data).then(function(result){
      q.resolve(result); // sucesso
    }, function(data){
      q.reject(data); // caso de erro
    });

    return q.promise;
  };

  factory.delete = function(id, data) {
    var q = $q.defer();

    $http.delete('/api/products/' + id).then(function(result){
      q.resolve(result); // sucesso
    }, function(){
      q.reject(); // caso de erro
    });

    return q.promise;
  };


  factory.byCategory = function(category, page){
    var q = $q.defer();

    $http.get('/api/category/products?category='+category+'&page='+page).then(function(res){
      q.resolve(res.data); // sucesso
    }, function(){
      q.reject(); // caso de erro
    });

    return q.promise;
  };

  factory.categoryProductsByStore = function(store_id, category, page){
    var q = $q.defer();
    if(!page) page = 1;

    $http.get('/api/category/'+ store_id +'/products?category='+category+'&page='+ (page -1 ) ).then(function(res){
      var pages = res.headers().pages;
      q.resolve({ data: res.data, pages: pages }); // sucesso
    }, function(){
      q.reject(); // caso de erro
    });

    return q.promise;
  };


  factory.search = function(query){
    var q = $q.defer();

    $http.get('/api/search/products?query=' + query).then(function(result){
      q.resolve(result.data); // sucesso
    }, function(){
      q.reject(); // caso de erro
    });

    return q.promise;
  };

  factory.searchInStore = function(query, store_id){
    var q = $q.defer();

    $http.get('/api/search/' + store_id + '/products?query=' + query)
    .then(function(result){
      q.resolve(result.data); // sucesso
    }, function(){
      q.reject(); // caso de erro
    });

    return q.promise;
  };

  factory.upload = function(image) {
    var q = $q.defer();

    $http.post('/api/upload', { image: image }).then(function(res){
      q.resolve(res.data); // sucesso
    }, function(){
      q.reject(); // caso de erro
    });

    return q.promise;
  };



  return factory;
});
app.factory('Reports', function($http, $q){
  var factory = {};

  factory.allProducts = function(){
    var q = $q.defer();

    $http.get('/api/reports/all_products').then(function(result){
      q.resolve(result.data.total); // sucesso
    }, function(){
      q.reject(); // caso de erro
    });

    return q.promise;
  };

  factory.allViews = function(){
    var q = $q.defer();

    $http.get('/api/reports/all_views').then(function(result){
      q.resolve(result.data.total); // sucesso
    }, function(){
      q.reject(); // caso de erro
    });

    return q.promise;
  };

  factory.allLovers = function(){
    var q = $q.defer();

    $http.get('/api/reports/all_lovers').then(function(result){
      q.resolve(result.data.total); // sucesso
    }, function(){
      q.reject(); // caso de erro
    });

    return q.promise;
  };

  factory.allStores = function(){
    var q = $q.defer();

    $http.get('/api/reports/all_stores').then(function(result){
      q.resolve(result.data.total); // sucesso
    }, function(){
      q.reject(); // caso de erro
    });

    return q.promise;
  };

  factory.allClients = function(){
    var q = $q.defer();

    $http.get('/api/reports/all_clients').then(function(result){
      q.resolve(result.data.total); // sucesso
    }, function(){
      q.reject(); // caso de erro
    });

    return q.promise;
  };

  factory.monthRequests = function(){
    var q = $q.defer();

    $http.get('/api/reports/monthRequests').then(function(result){
      q.resolve(result.data); // sucesso
    }, function(){
      q.reject(); // caso de erro
    });

    return q.promise;
  };

  factory.dailyRequests = function(){
    var q = $q.defer();

    $http.get('/api/reports/dailyRequests').then(function(result){
      q.resolve(result.data); // sucesso
    }, function(){
      q.reject(); // caso de erro
    });

    return q.promise;
  };

  // Para as Stores
  factory.allProductsStore = function(id){
    var q = $q.defer();

    $http.get('/api/reports/all_products/'+id).then(function(result){
      q.resolve(result.data.total); // sucesso
    }, function(){
      q.reject(); // caso de erro
    });

    return q.promise;
  };

  factory.allViewsStore = function(id){
    var q = $q.defer();

    $http.get('/api/reports/all_views/' + id).then(function(result){
      q.resolve(result.data.total); // sucesso
    }, function(){
      q.reject(); // caso de erro
    });

    return q.promise;
  };

  factory.allLoversStore = function(id){
    var q = $q.defer();

    $http.get('/api/reports/all_lovers/' + id).then(function(result){
      q.resolve(result.data.total); // sucesso
    }, function(){
      q.reject(); // caso de erro
    });

    return q.promise;
  };

  factory.monthRequestsStore = function(id){
    var q = $q.defer();

    $http.get('/api/reports/monthRequests/'+id).then(function(result){
      q.resolve(result.data); // sucesso
    }, function(){
      q.reject(); // caso de erro
    });

    return q.promise;
  };

  factory.dailyRequestsStore = function(id){
    var q = $q.defer();

    $http.get('/api/reports/dailyRequests/'+id).then(function(result){
      q.resolve(result.data); // sucesso
    }, function(){
      q.reject(); // caso de erro
    });

    return q.promise;
  };

  return factory;
});
app.factory('Requests', function($http, $q){
  var factory = {};

  factory.index = function(){
    var q = $q.defer();

    $http.get('/api/requests').then(function(result){
      var data = result.data.results;
      q.resolve(data); // sucesso
    }, function(){
      q.reject(); // caso de erro
    });

    return q.promise;
  };

  factory.getItens = function(id){
    var q = $q.defer();
    $http.get('/api/requests/itens/'+id).then(function(result){
      var data = result.data.results;
      q.resolve(data); // sucesso
    }, function(){
      q.reject(); // caso de erro
    });
    return q.promise;
  };

  factory.getItensStore = function(id, store_id){
    var q = $q.defer();
    $http.get('/api/store_itens/resquest?id='+id+'&store_id='+store_id).then(function(result){
      var data = result.data.results;
      q.resolve(data); // sucesso
    }, function(){
      q.reject(); // caso de erro
    });
    return q.promise;
  };

  factory.finalizar = function(id) {
    var q = $q.defer();
    var data = {
      current_status: 'completed'
    };
    
    $http.post('/api/requests/'+id+'/update', data).then(function(result){
      var data = result.data;
      q.resolve(data); // sucesso
    }, function(){
      q.reject(); // caso de erro
    });
    
    return q.promise;
  };

  factory.estornar = function(req) {
    var q = $q.defer();
    var data = {
      store_id: req.store_id,
      transaction_id: req.transaction_id
    };
    $http.post('/api/requests/'+req._id+'/refund', data).then(function(result){
      var data = result.data;
      q.resolve(data); // sucesso
    }, function(){
      q.reject(); // caso de erro
    });
    
    return q.promise;
  };

  factory.store = function(id){
    var q = $q.defer();

    $http.get('/api/stores/'+id+'/requests').then(function(result){
      var data = result.data;
      q.resolve(data); // sucesso
    }, function(){
      q.reject(); // caso de erro
    });

    return q.promise;
  };

  return factory;
});
app.factory('Store', function($http, $q, PAYMENT_GATEWAY){
  var factory = {};

  factory.index = function(page){
    var q = $q.defer();
    if(!page) page = 1;

    $http.get('/api/stores/?page=' + (page - 1 ) ).then(function(result){
      var pages = result.headers().pages;
      var stores = result.headers().stores
      q.resolve({ data: result.data, pages: pages, stores: stores}); // sucesso
    }, function(){
      q.reject(); // caso de erro
    });

    return q.promise;
  };

  factory.homologar = function(id, data){
    var q = $q.defer();

    $http.put('/api/homologar/'+ id, data).then(function(){
      q.resolve(); // sucesso
    }, function(data){
      q.reject(data); // caso de erro
    });

    return q.promise;
  };

  factory.delete = function(id){
    var q = $q.defer();

    $http.delete('/api/stores/'+id).then(function(result){
      q.resolve(result.data.results); // sucesso
    }, function(){
      q.reject(); // caso de erro
    });

    return q.promise;
  };

  factory.get = function(id){
    var q = $q.defer();

    if(id === 'undefined' || !id) {  q.reject(); return q.promise; }

    $http.get('/api/stores/'+id).then(function(result){
      var data = result.data;
      if(data.address) {
        if(data.address.longitude) data.address.longitude = Number(data.address.longitude);
        if(data.address.latitude) data.address.latitude = Number(data.address.latitude);
      }
      q.resolve(data); // sucesso
    }, function(){
      q.reject(); // caso de erro
    });

    return q.promise;
  };

  factory.user = function(id){
    var q = $q.defer();
    if(id === 'undefined' || !id) {  q.reject(); return q.promise; }

    $http.get('/api/stores/'+id+"/user").then(function(result){
      q.resolve(result.data); // sucesso
    }, function(){
      q.reject(); // caso de erro
    });

    return q.promise;
  };

  factory.create = function(data){
    var q = $q.defer();

    $http.post('/api/stores', data).then(function(result){
      q.resolve(result.data); // sucesso
    }, function(result){
      q.reject('internet ou error 500'); // caso de erro
    });

    return q.promise;
  };

  factory.update = function(id, data){
    var q = $q.defer();

    $http.put('/api/stores/'+ id, data).then(function(result){
      q.resolve(result); // sucesso
    }, function(data){
      q.reject(data); // caso de erro
    });

    return q.promise;
  };


  /* Verifica se email está dísponível para cadastro */
  factory.checkEmail = function(email){
    var q = $q.defer();

    $http.post('/api/duplicate_email/stores', { email: email }).then(function(res){
      q.resolve(res.data.unique); // sucesso
    }, function(){
      q.reject(); // caso de erro
    });

    return q.promise;
  };

  factory.getBanks = function(){
    return [{"code":"001","name":"Banco do Brasil S.A.","website":"<a rel=\"nofollow\" href=\"http://www.bb.com.br\">www.bb.com.br</a>"}
    ,{"code":"033","name":"Banco Santander (Brasil) S.A.","website":"<a rel=\"nofollow\" href=\"http://www.santander.com.br\">www.santander.com.br</a>"},
    {"code":"204","name":"Banco Bradesco Cartões S.A.","website":"<a rel=\"nofollow\" href=\"http://www.iamex.com.br\">www.iamex.com.br</a>"},{"code":"394","name":"Banco Bradesco Financiamentos S.A.","website":"<a rel=\"nofollow\" href=\"http://www.bmc.com.br\">www.bmc.com.br</a>"},{"code":"237","name":"Banco Bradesco S.A.","website":"<a rel=\"nofollow\" href=\"http://www.bradesco.com.br\">www.bradesco.com.br</a>"},
    {"code":"356","name":"Banco Real S.A.","website":"<a rel=\"nofollow\" href=\"http://www.bancoreal.com.br\">www.bancoreal.com.br</a>"},
    {"code":"104","name":"Caixa Econômica Federal","website":"<a rel=\"nofollow\" href=\"http://www.caixa.gov.br\">www.caixa.gov.br</a>"},
    {"code":"652","name":"Itaú Unibanco Holding S.A.","website":"<a rel=\"nofollow\" href=\"http://www.itau.com.br\">www.itau.com.br</a>"},
    {"code":"341","name":"Itaú Unibanco S.A.","website":"<a rel=\"nofollow\" href=\"http://www.itau.com.br\">www.itau.com.br</a>"}];
  };

  factory.search = function(query){
    var q = $q.defer();

    $http.get('/api/search/stores?query='+query).then(function(res){
      q.resolve(res.data); // sucesso
    }, function(){
      q.reject(); // caso de erro
    });

    return q.promise;
  };

  factory.homologarStores = function(page){
    var q = $q.defer();
    if(!page) page = 1;

    $http.get('/api/homologar/stores?page=' + (page - 1 )).then(function(result){
      var pages = result.headers().pages;
      q.resolve({ data: result.data, pages: pages}); // sucesso
    }, function(){
      q.reject(); // caso de erro
    });

    return q.promise;
  };

  return factory;
});
app.factory('User', function($http, $q){
  var factory = {};

  factory.get = function(id) {
    var q = $q.defer();

    $http.get('/api/users/'+id).then(function(result){
      q.resolve(result); // sucesso
    }, function(){
      q.reject(); // caso de erro
    });

    return q.promise;
  };

  factory.auth = function(data){
    var q = $q.defer();

    $http.post('/authenticate', data).then(function(result){
      q.resolve(result); // sucesso
    }, function(){
      q.reject(); // caso de erro
    });

    return q.promise;
  };

  factory.update = function(id, data){
    var q = $q.defer();
    console.log(data);

    $http.put('/api/users/'+ id, data).then(function(result){
      q.resolve(result); // sucesso
    }, function(){
      q.reject(); // caso de erro
    });

    return q.promise;
  };

  /* Verifica se email está dísponível para cadastro */
  factory.checkEmail = function(email){
    var q = $q.defer();

    $http.post('/api/duplicate_email/', { email: email }).then(function(res){
      q.resolve(res.data.unique); // sucesso
    }, function(){
      q.reject(); // caso de erro
    });

    return q.promise;
  };

  /* Atualiza a senha do usuário */
  factory.changePassword = function(data){
    var q = $q.defer();

    if(!data.id || !data.password || !data.new_password)
      q.reject();

    $http.put('/api/change-password', data).then(function(result){
      q.resolve(result); // sucesso
    }, function(){
      q.reject(); // caso de erro
    });

    return q.promise;
  };


  /* Atualiza a senha do usuário */
  factory.getByStore = function(id){
    var q = $q.defer();

    // rejeite caso o id seja inválido.
    if(id == null || id.length < 8) q.reject();

    $http.get('/api/stores/'+ id +'/user').then(function(result){
      q.resolve(result.data); // sucesso
    }, function(){
      q.reject(); // caso de erro
    });

    return q.promise;
  };


  return factory;
});
app.controller('nav', function($scope){
  $scope.$on('$routeChangeSuccess', function(next, current) {
    $scope.role = localStorage.getItem("role");
  });
});
app.controller('EsqueciController', function(User, $scope, $http){

  var colorFunc = function(x, y) {
      return 'hsl('+Math.floor(Math.abs(x*y)*360)+',80%,60%)';
  };

  var pattern = Trianglify({
        width: window.innerWidth,
        height: window.innerHeight,
        x_colors: ['#6dad5c', '#578a4a', '#406636','#33522c','#6dad5c']
  });

  var elm = document.getElementById('modalback');
  elm.appendChild(pattern.canvas());

  function disabled() {
    var form = document.getElementById('form');
    form.style['pointer-events'] = 'none';
    form.style['opacity'] = '0.5';
  }

  function enable(){
    var form = document.getElementById('form');
    form.style['pointer-events'] = 'auto';
    form.style['opacity'] = '1';
  }

  $scope.send = function(){
    var email = document.getElementById('email').value;
    if(email == null || email.length == 0){
      $scope.message = "E-mail não pode está vazio!";
      return false;
    }

    disabled();

    $http.post('/api/forget_password/',{ email: email }).then(function(){
      enable();
      alert('Enviado com sucesso! Verifique sua caixa de email.')

    }, function(){
      enable();
      alert('E-mail não encontrado em nosso sistema.')

    });
  };

});
app.controller('login', function(User, $scope, $window, $rootScope, $http){

  var colorFunc = function(x, y) {
      return 'hsl('+Math.floor(Math.abs(x*y)*360)+',80%,60%)';
  };

  var pattern = Trianglify({
        width: window.innerWidth,
        height: window.innerHeight,
        x_colors: ['#6dad5c', '#578a4a', '#406636','#33522c','#6dad5c']
  });

  // x_colors: ['#5885ef', '#496dc5', '#395497','#324a86','#5885ef']
  //  x_colors: ['#c3ddee', '#3498db', '#38b0ff','#2e7eae','#c3ddee']

  var elm = document.getElementById('modalback');
  elm.appendChild(pattern.canvas());

  if(localStorage.getItem("username")){

    if(localStorage.getItem("role") == "owner"){
      $window.location = '#/stores/show/'+localStorage.getItem("store_id");
    } else {
      $window.location = '#/stores';
    }
  }

  function disabled() {
    var form = document.getElementById('form');
    form.style['pointer-events'] = 'none';
    form.style['opacity'] = '0.5';
  }

  function enable(){
    var form = document.getElementById('form');
    form.style['pointer-events'] = 'auto';
    form.style['opacity'] = '1';
  }

  $scope.auth = function(){
    disabled();

    var credentials = {
      email: $scope.email,
      password: $scope.password
    };

    User.auth(credentials).then(function(res){
      console.log(res);
      $rootScope.username = res.data.user.name;
      $rootScope.role     = res.data.user.role;
      $rootScope.email    = res.data.user.email;

      // Token
      localStorage.setItem("token",res.data.token);
      $http.defaults.headers.common['Authorization'] = 'Bearer ' + res.data.token;
      // User
      localStorage.setItem("username", $rootScope.username);
      localStorage.setItem("user_id", res.data.user._id);
      localStorage.setItem("role", res.data.user.role);
      localStorage.setItem("store_id", res.data.user.store_id);
      localStorage.setItem("email", res.data.user.email);

      if($rootScope.role == "owner"){
        $window.location = '#/loja';
      } else {
        $window.location = '#/stores';
      }
    },function(){
      enable()
      $scope.message = "E-mail ou senha incorreta!";
    });
  };

});
app.controller('CategoriesController', function($scope, Category, $window, Auth){
  Auth.role(['admin']);
  $scope.name = null;
  $scope.isSend = false;

  $scope.categories = [];
  $scope.search = {};

  $scope.load = function(){
    Category.index().then(function(data){
      $scope.categories = data;
    });
  };

  $scope.load();

  $scope.create = function(){
    if(!$scope.name) return alert("Nome não pode está vazio!");
    $scope.isSend = true;

    Category.create({ name: $scope.name }).then(function(){
      $scope.isSend = false;
      $scope.name = null;
      $scope.load(); // reload data
    });
  };

  $scope.delete = function(id){
  if(!confirm('Você tem certeza?')) return;
    Category.delete(id);
    $scope.load();
  };



});
app.controller('EditarProdutoController', function($scope, Product, $filter, $window, $routeParams, Auth, Category){
  Auth.role('owner');

  $scope.product = {};
  $scope.remove = true;


  Category.index().then(function(data){
    $scope.categorys = data;
  });

  $scope.confirmSave = function(){
    Product.update($scope.product._id , { product: $scope.product }).then(function(){
      $window.location = '#/products';
    },function(res){
      window.dispatchEvent(new Event('buttonsend.fail'));
      // error
    });
  };

  $scope.closeModalconfirm = function(){
    window.dispatchEvent(new Event('buttonsend.fail'));
    $scope.confirmModal = false;
  };


  $scope.changeColor = function(index){
    var code = '';

    switch ($scope.product.colors[index].name) {
      case 'Roxo': code = '#721090'; break;
      case 'Berinjela': code = '#570084'; break;
      case 'Magenta': code = '#E03A8E'; break;
      case 'Lilás': code = '#DF8DB7'; break;
      case 'Marinho': code = '#0F174E'; break;
      case 'Azul': code = '#3871C7'; break;
      case 'Azul Royal': code = '#1700A4'; break;
      case 'Turquesa': code = '#42D3FF'; break;
      case 'Petróleo': code = '#154B62'; break;
      case 'Verde Militar': code = '#435022'; break;
      case 'Verde Bandeira': code = '#176336'; break;
      case 'Verde': code = '#229A53'; break;
      case 'Menta': code = '#9FDBC4'; break;
      case 'Bordô': code = '#970A27'; break;
      case 'Vermelho': code = '#D8160A'; break;
      case 'Carmim': code = '#F5000D'; break;
      case 'Pêssego': code = '#EF8A70'; break;
      case 'Caramelo': code = '#B74A1A'; break;
      case 'Telha': code = '#D4371A'; break;
      case 'Laranja': code = '#F76809'; break;
      case 'Coral': code = '#EE6845'; break;
      case 'Amarelo': code = '#FEFF00'; break;
      case 'Mostarda': code = '#E7CB27'; break;
      case 'Areia': code = '#E5E36D'; break;
      case 'Dourado': code = '#E6AC32'; break;
      case 'Café': code = '#4C3224'; break;
      case 'Marrom': code = '#642C1D'; break;
      case 'Paprica': code = '#74261C'; break;
      case 'Camel': code = '#955B37'; break;
      case 'Pão': code = '#CBB595'; break;
      case 'Nude': code = '#D9C9B2'; break;
      case 'Creme': code = '#FDFFD7'; break;
      case 'Branco': code = '#FFFFFF'; break;
      case 'Preto': code = '#000'; break;
      case 'Carbono': code = '#333'; break;
      case 'Cinza': code = '#999'; break;
      case 'Prata': code = '#CCC'; break;
    }
    $scope.product.colors[index].code = code;
  };

  $scope.addColor = function(){
    $scope.product.colors.push({ name: null, code: null });
  };

  $scope.removeColor = function(i){
    $scope.product.colors.splice(i, 1);
  };

  $scope.addSize = function(){
    $scope.product.sizes.push({ name: null});
  };

  $scope.removeSize = function(i){
    $scope.product.sizes.splice(i, 1);
  };

  $scope.quantityChange = function(){
    if($scope.product.quantity < 0) $scope.product.quantity = 0;
  };

  Product.get($routeParams.id).then(function(data){
    $scope.product = data;

    if(data.categories)
      $scope.product.category = data.categories[0];

    $scope.product.price = $filter('money')($scope.product.price);
    $scope.product.old_price = $filter('money')($scope.product.old_price);

    $scope.myImage = data.image_1;
  });

  // Fechar modal crop
  $scope.closeCrop = function(){
    $scope.crop = false;
    Product.upload($scope.croped).then(function(data){
      console.log("sucesso upload");
      $scope.product.images.push(data);
    });
  };

  $scope.closeCropCancel = function(){
    $scope.crop = false;
  };

  $scope.delete = function(){
    var remove = confirm('Tem certeza?');
    if(remove){
      Product.delete($scope.product._id).then(function(){
        $window.location = '#/products';
      });
    }
  };

  $scope.isEdit = true;

  $scope.send = function(){
    if(!checkFieldsProduct($scope)){
       return window.dispatchEvent(new Event('buttonsend.fail'));
    }

    // válidando cores
    var colors = $scope.product.colors;
    if(colors.length > 0){
      $scope.product.colors = colors.filter(function(value){
        return (!!value.name && !!value.code)
      });
    }

    // válidando tamanhos
    var sizes = $scope.product.sizes;
    if(sizes.length > 0){
      $scope.product.sizes = sizes.filter(function(value){
        return (!!value.name)
      });
    }

    $scope.isSend = true;
    $scope.product.categories = [$scope.product.category];

    $scope.confirmModal = true;
  };

  $scope.addImage = function(){
    var filePicker = document.createElement("input");
    filePicker.type = 'file';

    filePicker.onchange = function(evt){
        var file = evt.currentTarget.files[0];
        var reader = new FileReader();

        reader.onload = function (evt) {
          var img = document.createElement("img");
          img.src = evt.target.result;

          Product.upload(evt.target.result).then(function(data){
            console.log("sucesso upload");
            $scope.product.images.push(data);
          });
        };

        reader.readAsDataURL(file);
    };

    filePicker.click();
  };

  $scope.removeImage = function(index){
    $scope.product.images.splice(index, 1);
  };

});

app.controller('ProdutoController', function($scope, Product, $window, Auth, Category, $routeParams){
  Auth.role(['admin','owner']);

  $scope.products = [];

  $scope.pageClass = function(i){
    if(i == $routeParams.page || (i == 1 && $routeParams.page == undefined)){
      return 'mark';
    } else {
      return '';
    }
  };

  var store_id = localStorage.getItem('store_id');

  // pegando todas as categorias
  $scope.categories = [];
  Category.index().then(function(data){
    $scope.categories = data;
  });

  var categoryTarget = "";

  function cleanCategoryClass(){
    $scope.categories.forEach(function(elm, i, array) {
        array[i].class = '';
    });
  }

  $scope.byCategory = function($index){
    categoryTarget = $scope.categories[$index].name;
    $scope.term = categoryTarget; // termo da pesquisa para aparecer pro usuário

    $scope.categories.forEach(function(elm, i, array) {
      if(elm.name === categoryTarget){
        array[i].class = 'selected';
      } else {
        array[i].class = '';
      }
    });

    if(!categoryTarget) return;
    Product.categoryProductsByStore(store_id, categoryTarget , $routeParams.page).then(function(res){
      console.log(res);
      $scope.pages = [];

      $scope.products = res.data;
    })
  };

  $scope.edit = function(id){
    $window.location = '#/products/edit/' + id;
  };

  $scope.delete = function(id){
    var remove = confirm('Tem certeza?');
    if(remove){
      Product.delete(id).then(function(){
        pull();
      });
    }
  };

  $scope.search = function(){
    var query = $scope.query;
    $scope.term = query; // termo da pesquisa para aparecer pro usuário
    cleanCategoryClass();

    var store_id = localStorage.getItem('store_id');
    if(!query) return;
    Product.searchInStore(query, store_id).then(function(data){
      $scope.products = data;
    });
  };

  function pull() {
    cleanCategoryClass();
    Product.index($routeParams.page, store_id).then(function(res){
      $scope.products = res.data;
      $scope.pages = [];
      for (var i = 1; i <= res.pages; i++) {
        $scope.pages.push(i)
      }

    });
  }

  $scope.resetProducts = function(){
    $scope.term = null;
    $scope.query = '';
    pull();
  };

  pull();

});


/* Função para validar o formulário para criação e edição
  de produtos*/
  function checkFieldsProduct($scope){
    if($scope.product.images.length == 0) {
      $scope.message = "O produto tem que ter pelo menos uma imagem cadastrada.";
      return false;
    }

    if(!$scope.product.name) {
      $scope.message = "Campo nome não pode estar vazio.";
      return false;
    }

    if(!$scope.product.category) {
      $scope.message = "Campo categoria não pode estar vazio.";
      return false;
    }

    if(!$scope.product.price) {
      $scope.message = "Campo preço não pode estar vazio.";
      return false;
    }

    if(!$scope.product.description) {
      $scope.message = "Campo descrição não pode estar vazio.";
      return false;
    }

    return true;
  }

app.controller('NovoProdutoController', function($scope, Product, $window, Auth, Category){
  Auth.role('owner');

  $scope.product = {};
  $scope.product.quantity = 0;
  $scope.product.store_id = localStorage.getItem('store_id');
  $scope.product.images = [];

  $scope.quantityChange = function(){
    if($scope.product.quantity < 0) $scope.product.quantity = 0;
  };


  Category.index().then(function(data){
    $scope.categorys = data;
  });

  $scope.confirmSave = function(){
    Product.create($scope.product).then(function(){
      $window.location = '#/products';
    },function(res){
      window.dispatchEvent(new Event('buttonsend.fail'));

      if(res.status == 400){
        $scope.message = res.data.msg;
      }
    });
  };

  $scope.closeModalconfirm = function(){
    window.dispatchEvent(new Event('buttonsend.fail'));
    $scope.confirmModal = false;
  };


  $scope.changeColor = function(index){
    var code = '';

    switch ($scope.product.colors[index].name) {
      case 'Roxo': code = '#721090'; break;
      case 'Berinjela': code = '#570084'; break;
      case 'Magenta': code = '#E03A8E'; break;
      case 'Lilás': code = '#DF8DB7'; break;
      case 'Marinho': code = '#0F174E'; break;
      case 'Azul': code = '#3871C7'; break;
      case 'Azul Royal': code = '#1700A4'; break;
      case 'Turquesa': code = '#42D3FF'; break;
      case 'Petróleo': code = '#154B62'; break;
      case 'Verde Militar': code = '#435022'; break;
      case 'Verde Bandeira': code = '#176336'; break;
      case 'Verde': code = '#229A53'; break;
      case 'Menta': code = '#9FDBC4'; break;
      case 'Bordô': code = '#970A27'; break;
      case 'Vermelho': code = '#D8160A'; break;
      case 'Carmim': code = '#F5000D'; break;
      case 'Pêssego': code = '#EF8A70'; break;
      case 'Caramelo': code = '#B74A1A'; break;
      case 'Telha': code = '#D4371A'; break;
      case 'Laranja': code = '#F76809'; break;
      case 'Coral': code = '#EE6845'; break;
      case 'Amarelo': code = '#FEFF00'; break;
      case 'Mostarda': code = '#E7CB27'; break;
      case 'Areia': code = '#E5E36D'; break;
      case 'Dourado': code = '#E6AC32'; break;
      case 'Café': code = '#4C3224'; break;
      case 'Marrom': code = '#642C1D'; break;
      case 'Paprica': code = '#74261C'; break;
      case 'Camel': code = '#955B37'; break;
      case 'Pão': code = '#CBB595'; break;
      case 'Nude': code = '#D9C9B2'; break;
      case 'Creme': code = '#FDFFD7'; break;
      case 'Branco': code = '#FFFFFF'; break;
      case 'Preto': code = '#000'; break;
      case 'Carbono': code = '#333'; break;
      case 'Cinza': code = '#999'; break;
      case 'Prata': code = '#CCC'; break;
    }
    $scope.product.colors[index].code = code;
  };

  $scope.product.colors = [];
  $scope.product.sizes = [];

  $scope.addColor = function(){
    $scope.product.colors.push({ name: null, code: null });
  };

  $scope.removeColor = function(i){
    $scope.product.colors.splice(i, 1);
  };

  $scope.addSize = function(){
    $scope.product.sizes.push({ name: null});
  };

  $scope.removeSize = function(i){
    $scope.product.sizes.splice(i, 1);
  };

  // Criar produto
  $scope.send = function(){
    if(!checkFieldsProduct($scope)){
       return window.dispatchEvent(new Event('buttonsend.fail'));
    }

    // válidando cores
    var colors = $scope.product.colors;
    if(colors.length > 0){
      $scope.product.colors = colors.filter(function(value){
        return (!!value.name && !!value.code)
      });
    }

    // válidando tamanhos
    var sizes = $scope.product.sizes;
    if(sizes.length > 0){
      $scope.product.sizes = sizes.filter(function(value){
        return (!!value.name)
      });
    }

    $scope.isSend = true;
    $scope.product.categories = [ $scope.product.category ];

    $scope.confirmModal = true;
  };


  $scope.addImage = function(){
    var filePicker = document.createElement("input");
    filePicker.type = 'file';

    filePicker.onchange = function(evt){
        var file = evt.currentTarget.files[0];
        var reader = new FileReader();

        reader.onload = function (evt) {
          var img = document.createElement("img");
          img.src = evt.target.result;

          Product.upload(evt.target.result).then(function(data){
            console.log("sucesso upload");
            $scope.product.images.push(data);
          });

        };

        reader.readAsDataURL(file);
    };

    filePicker.click();
  };


  $scope.removeImage = function(index){
    $scope.product.images.splice(index, 1);
  };

});
app.controller("HomologarController", function($scope, Auth, Store, User, $window, $routeParams){
  Auth.role('admin');

  $scope.stores = [];

  $scope.pageClass = function(i){
    if(i == $routeParams.page || (i == 1 && $routeParams.page == undefined)){
      return 'mark';
    } else {
      return '';
    }
  };

  Store.homologarStores($routeParams.page).then(function(res){
    $scope.pages = [];
    for (var i = 1; i <= res.pages; i++) {
      $scope.pages.push(i)
    }

    $scope.stores = res.data;
    for (var i = 0; i < $scope.stores.length; i++) {
      (function(i){
        User.getByStore($scope.stores[i]._id).then(function(res){
          console.log(res);
          $scope.stores[i].user = res;
          $scope.stores[i].due_time = moment($scope.stores[i].created_at).add(72, 'hours').format('DD/MM/YYYY  h:mm a');
        });
      })(i)
    }
  });

  $scope.show = function(id){
    $window.location = "#/homologar/store/"+id;
  };

});

app.controller('EditarLojaController', function($scope, Store, $window, $routeParams, User, Auth){
  Auth.role(['admin', 'owner']);

  $scope.isRole = function(role){
    return Auth.isRole(role);
  }

  $scope.store = null;
  // Conseguindo os bancos cadastrados.
  $scope.banks = Store.getBanks();

  var store_id = $routeParams.id;
  console.log(store_id);

  // get store data
  Store.get(store_id).then(function(data){
    console.log(data);
    data.account = data.account || {};
    $scope.store = data;
  }, function(){
    console.err("problemas com get store!");
  });

  // get user of store
  Store.user(store_id).then(function(data){
    $scope.user = data;
  });

  $scope.back = function(){
    $window.history.back();
  };

  $scope.onlyNumbers = function(name){
    if($scope.store.bank_account[name])
    $scope.store.bank_account[name] =   $scope.store.bank_account[name].replace(/[^0-9]/g, '');
  };

  $scope.update = function(){
    function fail() {
      window.dispatchEvent(new Event('buttonsend.fail'));
    }

    if($scope.store.vitrine == false){

      if(!$scope.store.partner_admin){
        $scope.store.partner_admin = {};
      }

      if(!$scope.store.partner_admin.name){
        $scope.message = "Nome do sócio administrador não pode estar vazio.";
        return fail();
      }

      if(!$scope.store.partner_admin.cpf){
        $scope.message = "CPF do sócio administrador não pode estar vazio.";
        return fail();
      }

      if(!TestaCPF($scope.store.partner_admin.cpf)){
        $scope.message = "CPF do sócio administrador não é válido";
        return fail();
      }

      if(!$scope.store.partner_admin.phone){
        $scope.message = "Telefone do sócio administrador não pode estar vazio.";
        return fail();
      }

      if(!$scope.store.bank_account){
        $scope.store.bank_account = {};
      }


      if(!$scope.store.bank_account.legal_name){
        $scope.message = "Nome legal associado a conta não pode estar vazio.";
        return fail();
      }

      if(!$scope.store.bank_account.bank_code){
        $scope.message = "Banco não pode estar vazio.";
        return fail();
      }

      if(!$scope.store.bank_account.agencia){
        $scope.message = "Agência do banco não pode estar vazia.";
        return fail();
      }

      if(!$scope.store.bank_account.agencia_dv){
        $scope.message = "DV da Agência do banco não pode estar vazia.";
        return fail();
      }

      if(!$scope.store.bank_account.conta){
        $scope.message = "Conta do banco não pode estar vazia.";
        return fail();
      }

      if(!$scope.store.bank_account.conta_dv){
        $scope.message = "DV da Conta do banco não pode estar vazia.";
        return fail();
      }
    }

    if(!checkFieldsStore($scope)) return fail();

    var body = {
       store: $scope.store
    };

    if($scope.image) body.image = $scope.image;

    Store.update(store_id, body).then(function(){
      var role = localStorage.getItem("role");
      if(role == 'admin'){
        $window.location = '#/stores';
      } else {
        $window.location = '#/store';
      }
    }, function(){
      window.dispatchEvent(new Event('buttonsend.fail'));
    });

  };

  $scope.addImage = function(){
    var filePicker = document.createElement("input");
    filePicker.type = 'file';

    filePicker.onchange = function(evt){
        var file = evt.currentTarget.files[0];
        var reader = new FileReader();

        reader.onload = function (evt) {
          var img = document.createElement("img");
          img.src = evt.target.result;

          $scope.$apply(function(){
            $scope.image = evt.target.result ;
          });
        };

        reader.readAsDataURL(file);
    };

    filePicker.click();
  };


  // Fechar modal crop
  $scope.closeCrop = function(){
    $scope.crop = false;
    $scope.image = $scope.croped;
  };

  $scope.isEditRoute = function(){
    return true;
  };


  $scope.cepChange = function(){
    $.ajax({
     type: "POST",
     url: "http://cep.republicavirtual.com.br/web_cep.php?formato=json&cep="+$scope.store.address.cep,
     dataType: "json"

     }).done(function(data) {
       console.log(data);
        $scope.$apply(function(){
          $scope.store.address.district = data.bairro;
          $scope.store.address.city = data.cidade;
          $scope.store.address.street = data.logradouro;
          $scope.store.address.state = data.uf;
        });
     });
  };

});

app.controller('ListaLojaController', function($scope, Store, User, $window, Auth, $routeParams){
  Auth.role('admin');

  $scope.stores = [];

  $scope.pageClass = function(i){
    if(i == $routeParams.page || (i == 1 && $routeParams.page == undefined)){
      return 'mark';
    } else {
      return '';
    }
  };

  function loadStores(){
    Store.index($routeParams.page).then(function(res){
      $scope.pages = [];
      for (var i = 1; i <= res.pages; i++) {
        $scope.pages.push(i)
      }

      $scope.total_stores = res.stores;

      console.log($scope.pages);
      $scope.stores = res.data;
      for (var i = 0; i < $scope.stores.length; i++) {
        (function(i){
          User.getByStore($scope.stores[i]._id).then(function(res){
            $scope.stores[i].user = res;
          });
        })(i)
      }
    });
  }

  loadStores();

  $scope.show = function(id){
    $window.location = '#/stores/show/'+id;
  };

  $scope.edit = function(id){
    $window.location = '#/stores/edit/'+id;
  };

  $scope.delete = function(id){
  if(!confirm('Você tem certeza?')) return;
    Store.delete(id);
    loadStores();
  };


});
app.controller("HomologarStoreController", function($scope, Auth, Store, User, $window, $routeParams){
  Auth.role('admin');

  var store_id = $routeParams.id;
  $scope.store = {};

  Store.get(store_id).then(function(data){
    $scope.store = data;
    $scope.due_time = moment(data.created_at).add(72, 'hours').format('DD/MM/YYYY  h:mm a');
  });

  Store.user(store_id).then(function(data){
    $scope.user = data;
  });

  $scope.update = function(){
    Store.homologar(store_id, {  pagarmeApiKey: $scope.pagarmeApiKey }).then(function(){
      $window.location = "#/homologar";
    });
  };
});
app.controller('CadastraLojaController', function($scope, Store, $window, Auth, User){
  Auth.role('admin');

  $scope.isRole = function(role){
    return Auth.isRole(role);
  }

  $scope.store = {};
  $scope.store.address = {};
  $scope.store.account = {};
  $scope.user = {};

  // Conseguindo os bancos cadastrados.
  $scope.banks = Store.getBanks();


  // váriaveis pra validar emails do formulário
  $scope.emails = {};
  $scope.emails.store = false;
  $scope.emails.user = false;


  $scope.create = function(){

    if(!checkFieldsStore($scope)) {
      window.dispatchEvent(new Event('buttonsend.fail'));
      return;
    }

    if(!$scope.emails.store || !$scope.emails.user){
      window.dispatchEvent(new Event('buttonsend.fail'));
      return $scope.message = "Email da loja e do usuário não podem ser iguais.";
    }

    function validAccount(){
      if(!$scope.store.account.legal_name)
        return $scope.message = "Nome legal da conta bancária não pode está vazio.";
      if(!$scope.store.account.document_number)
        return $scope.message = "CNPJ associado a conta bancária não pode está vazio.";
      if(!$scope.store.account.bank_code)
        return $scope.message = "O banco da conta bancária não pode está vazio.";
      if(!$scope.store.account.agencia)
        return $scope.message = "A agência da conta bancária não pode está vazio.";
      if(!$scope.store.account.conta)
        return $scope.message = "A o número da conta bancária não pode está vazio.";

        return false;
    }

    if(validAccount()) {
      window.dispatchEvent(new Event('buttonsend.fail'));
      return false;
    }


    console.log($scope.store);

    $scope.isSend = true;
    Store.create({ image: $scope.image, store: $scope.store, user: $scope.user })
    .then(function(data){
      $window.location = '#/stores';
    },function(res){
        if(res === 'conta bancária'){
          $scope.message = "Problemas com a conta bancária, verifique os dados.";
        } else {
          $scope.message = "Problemas ao criar a loja.";
        }

        window.dispatchEvent(new Event('buttonsend.fail'));
    });
  };

  $scope.addImage = function(){
    var filePicker = document.createElement("input");
    filePicker.type = 'file';

    filePicker.onchange = function(evt){
        var file = evt.currentTarget.files[0];
        var reader = new FileReader();

        reader.onload = function (evt) {
          var img = document.createElement("img");
          img.src = evt.target.result;

          $scope.$apply(function(){
            $scope.image = evt.target.result ;
          });
        };

        reader.readAsDataURL(file);
    };

    filePicker.click();
  };


  // Verificando disponibilidade de email da loja
  $scope.checkEmail = function(){
    var elm  = document.getElementById('email-message');

    $scope.emails.store = false;
    function validateEmail(email) { return /\S+@\S+\.\S+/.test(email); }

    if(!validateEmail($scope.store.email))
      return elm.innerHTML = "<p class=\"message-error\">E-mail não é válido.</p>";

    elm.innerHTML = "<p class=\"message-alert\">Verificando disponibilidade.</p>";

    Store.checkEmail($scope.store.email).then(function(unique){
      if(unique){
        $scope.emails.store = true;
        elm.innerHTML = "<p class=\"message-sucess\">E-mail disponível.</p>";
      } else {

        elm.innerHTML = "<p class=\"message-error\">E-mail já está em uso.</p>";
      }
    });
  };


  // Verificando disponibilidade de email do usuário.
  $scope.checkEmailUser = function(){
    var elm  = document.getElementById('email-user-message');

    function validateEmail(email) { return /\S+@\S+\.\S+/.test(email); }

    $scope.emails.user = false;
   if(!validateEmail($scope.user.email))
     return elm.innerHTML = "<p class=\"message-error\">E-mail não é válido.</p>";

    elm.innerHTML = "<p class=\"message-alert\">Verificando disponibilidade.</p>";

    User.checkEmail($scope.user.email).then(function(unique){
      if(unique){
        $scope.emails.user = true;
        elm.innerHTML = "<p class=\"message-sucess\">E-mail disponível.</p>";
      } else {
        elm.innerHTML = "<p class=\"message-error\">E-mail já está em uso.</p>";
      }
    });
  };


});


/* Função para validar o formulário para criação de lojas*/
  function checkFieldsStore($scope){

    if(!$scope.store.avatar && !$scope.image) {
      $scope.message = "Imagem da loja não pode estar vazia.";
      return false;
    }

    if(!$scope.store.name) {
      $scope.message = "Campo nome da loja não pode estar vazio.";
      return false;
    }


    if($scope.store.address){
      if(!$scope.store.address.state) {
        $scope.message = "Campo estado não pode estar vazio.";
        return false;
      }

      if(!$scope.store.address.city) {
        $scope.message = "Campo cidade não pode estar vazio.";
        return false;
      }

      if(!$scope.store.address.street) {
        $scope.message = "Campo rua não pode estar vazio.";
        return false;
      }

      if(!$scope.store.address.district) {
        $scope.message = "Campo bairro não pode estar vazio.";
        return false;
      }

      if(!$scope.store.address.number) {
        $scope.message = "Campo número não pode estar vazio.";
        return false;
      }

    }

    return true;
  }
/* Controller responsável pela finalização do cadastro de uma loja. */
app.controller('RegisterController', function($scope, Store, User, $http, $window){

  // Conseguindo os bancos cadastrados.
  $scope.banks = Store.getBanks();

  $scope.user = { cdl: false };

  $scope.store = {
    company: {},
    docs: {},
    address: {},
    contact_responsible: {},
    vitrine: false
  };

  $scope.onlyNumbers = function(name){
    if($scope.store.bank_account[name])
    $scope.store.bank_account[name] =   $scope.store.bank_account[name].replace(/[^0-9]/g, '');
  };

  $scope.vitrineChange = function(){
    if($scope.store.vitrine == false ) $scope.store.hide_price = false;
  };

  function getElm(id){
    return document.getElementById(id);
  }

  var inputs = [
    // company
    { group: 'company', name: 'name',         elm: getElm('company-name')},
    { group: 'company', name: 'fantasy_name', elm: getElm('company-fantasy_name')},
    { group: 'company', name: 'cnpj',         elm: getElm('company-cnpj')},
    { group: 'company', name: 'phone',        elm: getElm('company-phone')},
    // address
    { group: 'address', name: 'cep',         elm: getElm('address_cep')},
    { group: 'address', name: 'street',      elm: getElm('address_street')},
    { group: 'address', name: 'number',      elm: getElm('address_number')},
    { group: 'address', name: 'district',    elm: getElm('address_district')},
    { group: 'address', name: 'city',        elm: getElm('address_city')},
    { group: 'address', name: 'state',       elm: getElm('address_state')},
    // contact responsible
    { group: 'contact_responsible', name: 'name',       elm: getElm('contact_responsible_name')}

  ];

  // Testar se o campo está nulo.
  function isBlank(str) {
    return (!str || /^\s*$/.test(str));
  }

  // Validar inputs
  function validateInputs(){
    var erros = false; // caso tenha sido encontrados erros
    var fisrt = null; // primeiro input com erro

    // verificando se nome de usuário está vazio.
    if(isBlank($scope.user.name)){
      var elm = document.getElementById('user-name');
      elm.className = 'input-error';
      fisrt = elm;
    }

    // verificando se o email do usuário está vazio.
    if(isBlank($scope.user.email)){
      var elm = document.getElementById('user-email');
      elm.className = 'input-error';
      if(!fisrt) fisrt = elm;
    }

    inputs.forEach(function(input, i, array){
      var value = $scope.store[input.group][input.name];
      if(isBlank(value)){
        erros = true;
        if(fisrt == null) fisrt = input.elm;

        if(input.elm == null ) return console.log(input);
        input.elm.className = 'input-error';

        return false;
      } else {

        if(input.name == 'cpf'){
          if(!TestaCPF(value)){
            erros = true;
            if(fisrt == null) fisrt = input.elm;
            input.elm.className = 'input-error';
            return false;
          } else {
            input.elm.className = '';
          }
        } else if(input.name == 'cnpj'){
          if(!validarCNPJ(value)){
            erros = true;
            if(fisrt == null) fisrt = input.elm;
            input.elm.className = 'input-error';
            return false;
          } else {
            input.elm.className = '';
           }
        } else if(input.elm == null ) {
          return console.log(input);
        } else {
          input.elm.className = '';
        }

      }
    });


    if(fisrt != null) fisrt.focus();
    return erros;
  }



  $scope.cepChange = function(){

      $.ajax({
       type: "POST",
       url: "http://cep.republicavirtual.com.br/web_cep.php?formato=json&cep="+$scope.store.address.cep,
       dataType: "json"
       }).done(function(data) {
         console.log(data);
          $scope.$apply(function(){
            $scope.store.address.district = data.bairro;
            $scope.store.address.city = data.cidade;
            $scope.store.address.street = data.logradouro;
            $scope.store.address.state = data.uf;
          });
       });

  };


  var inprogress = false;
  // para mostrar o último modal do fluxo da página.
  $scope.finish = false;

  var store = $scope.store;

  $scope.save = function(){
    if(inprogress) return;

    if(validateInputs()){
      $scope.message = "Formulário com dados incompletos!";
      return;
    } else {
      $scope.message = null;
    }

    store = $scope.store;
    store.cdl = $scope.user.cdl;
    store.cnpj = store.company.cnpj;
    store.phones = [ store.company.phone ];
    store.corporate_name = store.company.name;
    store.name = store.company.fantasy_name;

    if(store.vitrine == true) store.bank_account = {};

    $scope.modal = true;
  };

   $scope.aceito = false;

   $scope.close = function() {
     $scope.modal = false;
   };

   $scope.create = function(){
     if($scope.aceito == false) return;

     $scope.modal = false;
     inprogress = true;
     // criando loja
     Store.create({ store: store, user: $scope.user }).then(function(data){
       // mostrar modal dizendo para verificar email.. .
       $scope.finish = true;
     }, function(){
       inprogress = false;
       $scope.message = "Problemas ao criar a loja!";
     });
   };
});
app.controller('LojaController', function($scope, Store, $window, Auth, $routeParams){
  Auth.role(['admin','owner']);

  $scope.store = null;
  var store_id = $routeParams.id || localStorage.getItem("store_id");

  Store.get(store_id).then(function(data){
    $scope.store = data;
  });

  // get user of store
  Store.user(store_id).then(function(data){
    $scope.user = data;
  });

  $scope.editStore = function(){
    $window.location = "#/stores/edit/" + store_id;
  };

});


app.controller('StoreRedirect', function($window, Auth){
  Auth.role(['owner']);
  $window.location = '#/stores/show/'+localStorage.getItem("store_id");
});
app.controller('PromotionsController', function($scope, Auth, Banner, Store){
  Auth.role('admin');

  $scope.banners  = [];

  function pull(){
    Banner.index().then(function(data){
      $scope.banners = data;
      for (var i = 0; i < $scope.banners.length; i++) {
        // definindo dias restantes
        if($scope.banners[i].period.indeterminate == false)
        $scope.banners[i].days = moment().from($scope.banners[i].period.end, true);
        // pegando info de store
        (function(i){
          Store.get($scope.banners[i].store_id).then(function(data){
            $scope.banners[i].store = data;
          });
        })(i);
      }
    });
  }

  pull();

  $scope.delete = function($index){
    Banner.delete($scope.banners[$index]._id).then(function(){
      pull();
    });
  };

});
app.controller('NewBannerController', function($scope, Auth, Product, Banner, $window, $routeParams){
  Auth.role('owner');

  $scope.imageBanner = null;
  var store_id = localStorage.getItem("store_id");


});
app.controller('NewPromotionController', function($scope, Auth, Product, Banner, $window, $routeParams){
  Auth.role('admin');

  $scope.imageBanner = null;
  var store_id = localStorage.getItem("store_id");

  $scope.days = 15;
  $scope.pretty_days = 0;

  var banner_id = $routeParams.id ;

  $scope.storeAndProduct = {
    store_id: null,
    product_id: null,
    store_action: false
  };


  $scope.$watch('days',function(){
    $scope.pretty_days = moment().add($scope.days, 'd').format("DD [de] MMMM");
  });

  var elmdays = document.getElementById('daysdiv');
  $scope.checkbox_forever = false;

  $scope.$watch('checkbox_forever',function(){
    if($scope.checkbox_forever == true){
      elmdays.style['pointer-events'] = 'none';
      elmdays.style['opacity'] = '0.5';
    } else {
      elmdays.style['pointer-events'] = 'auto';
      elmdays.style['opacity'] = '1';
    }
  });


  // escolher uma imagem.
  var fileInput =   document.getElementById('file');

  fileInput.onchange = function(evt){
      var file = evt.currentTarget.files[0];
      var reader = new FileReader();

      reader.onload = function (evt) {
        $scope.$apply(function(){
          $scope.image = evt.target.result ;
        });
      };

      reader.readAsDataURL(file);
  };

  $scope.pickImage = function(){
    fileInput.click();
  };


  $scope.save = function(){
     var data = {
       image: $scope.image,
       store_id: $scope.storeAndProduct.store_id,
       period: {
         start: moment().toDate(),
         end: moment().add($scope.days, 'd').toDate()
       }
     };

     data.period.indeterminate = $scope.checkbox_forever;


     if(data.image == null)
       return alert("Não é possível salvar sem definir a imagem do banner.");

     if(data.store_id == null)
       return alert("Não é possível salvar sem definir a loja.");

       data.action = {};

       if($scope.storeAndProduct.store_action == true) {
         data.action.target = 'default';
         data.action.url = '#/loja/'+data.store_id;
       } else {
         data.action.target = 'default';
         data.action.url = '#/produto/'+ $scope.storeAndProduct.product_id;
       }

     Banner.create(data).then(function(data){
      window.location = "#/promotions";
     });
  };


});
app.controller('VendasAdminController', function($scope, Requests, Auth, User){
  Auth.role('admin');

  $scope.requests = [];
  var store_id = localStorage.getItem('store_id');

  Requests.index().then(function(data) {
    $scope.requests = data;
  });

  $scope.estornar = function(request){
    Requests.estornar(request).then(function(data){
      request.status = 'refunded';
      alert('Pedido estornado com sucesso!');
    }).catch(function(err){
      alert('Erro ao estornar o pedido!');
    });
  };
  
  $scope.finalizar = function(request){
    Requests.finalizar(request._id).then(function(data){
      request.status = 'completed';
      alert('Pedido finalizado com sucesso!');
    }).catch(function(err){
      alert('Erro ao finalizar o pedido!');
    });
  };

  $scope.loadUser = function(request){
    request.expanded = true;
    User.get(request.user_id).then(function(user){
      request.user = user.data;
    });
  };
});
app.controller('VendasController', function($scope, Requests, Auth, User){
  Auth.role('owner');

  $scope.requests = [];
  var store_id = localStorage.getItem('store_id');

  if(store_id){
    Requests.store(store_id).then(function(data) {
      $scope.requests = data;
    });
  }
  
  $scope.estornar = function(request){
    Requests.estornar(request).then(function(data){
      request.status = 'refunded';
      alert('Pedido estornado com sucesso!');
    }).catch(function(err){
      alert('Erro ao estornar o pedido!');
    });
  };
  
  $scope.finalizar = function(request){
    Requests.finalizar(request._id).then(function(data){
      request.status = 'completed';
      alert('Pedido finalizado com sucesso!');
    }).catch(function(err){
      alert('Erro ao finalizar o pedido!');
    });
  };

  $scope.loadUser = function(request){
    request.expanded = true;
    User.get(request.user_id).then(function(user){
      request.user = user.data;
    });
  };
});
app.controller('RelatorioAdminController', function($scope, Auth, Reports){
  Auth.role('admin');

  Reports.allProducts().then(function(total){
    $scope.totalProducts = total;
  });

  Reports.allViews().then(function(total){
    $scope.totalViews = total;
  });

  Reports.allLovers().then(function(total){
    $scope.totalLovers = total;
  });

  Reports.allStores().then(function(total){
    $scope.totalStores = total;
  });

  Reports.allClients().then(function(total){
    $scope.totalClients = total;
  });

  Reports.monthRequests().then(function(result){
    console.log(result);

    var labels = [];
    for (var i = 0; i < result.length; i++) {
      labels.push(result[i].month);
    }

    var values = [];
    for (var i = 0; i < result.length; i++) {
      values.push(result[i].total);
    }

    var data = {
      labels: labels,
      datasets: [
          {
              label: "My Second dataset",
              fillColor: "rgba(151,187,205,0.2)",
              strokeColor: "rgba(151,187,205,1)",
              pointColor: "rgba(151,187,205,1)",
              pointStrokeColor: "#fff",
              pointHighlightFill: "#fff",
              pointHighlightStroke: "rgba(151,187,205,1)",
              data: values
          }
      ]
    };

    var ctx = document.getElementById("myChart").getContext("2d");
    var myLineChart = new Chart(ctx).Line(data, {});
  });


  Reports.dailyRequests().then(function(result){
    console.log(result);

    var labels = [];
    for (var i = 0; i < result.length; i++) {
      labels.push(result[i].day);
    }

    var values = [];
    for (var i = 0; i < result.length; i++) {
      values.push(result[i].total);
    }

    var data = {
      labels: labels,
      datasets: [
          {
              label: "My Second dataset",
              fillColor: "rgba(151,187,205,0.2)",
              strokeColor: "rgba(151,187,205,1)",
              pointColor: "rgba(151,187,205,1)",
              pointStrokeColor: "#fff",
              pointHighlightFill: "#fff",
              pointHighlightStroke: "rgba(151,187,205,1)",
              data: values
          }
      ]
    };

    var ctx2 = document.getElementById("myChart2").getContext("2d");
    var myLineChart = new Chart(ctx2).Line(data, {});
  });




});
app.controller('RelatorioStoreController', function($scope, Auth, Reports){
  Auth.role('owner');

  var store_id = localStorage.getItem("store_id");

  Reports.allProductsStore(store_id).then(function(total){
    $scope.totalProducts = total;
  });

  Reports.allViewsStore(store_id).then(function(total){
    $scope.totalViews = total;
  });

  Reports.allLoversStore(store_id).then(function(total){
    $scope.totalLovers = total;
  });

  Reports.monthRequestsStore(store_id).then(function(result){

    var labels = [];
    for (var i = 0; i < result.length; i++) {
      labels.push(result[i].month);
    }

    var values = [];
    for (var i = 0; i < result.length; i++) {
      values.push(result[i].total);
    }

    var data = {
      labels: labels,
      datasets: [
          {
              label: "My Second dataset",
              fillColor: "rgba(151,187,205,0.2)",
              strokeColor: "rgba(151,187,205,1)",
              pointColor: "rgba(151,187,205,1)",
              pointStrokeColor: "#fff",
              pointHighlightFill: "#fff",
              pointHighlightStroke: "rgba(151,187,205,1)",
              data: values
          }
      ]
    };

    var ctx = document.getElementById("myChart").getContext("2d");
    var myLineChart = new Chart(ctx).Line(data, {});
  });


  Reports.dailyRequestsStore(store_id).then(function(result){

    var labels = [];
    for (var i = 0; i < result.length; i++) {
      labels.push(result[i].day);
    }

    var values = [];
    for (var i = 0; i < result.length; i++) {
      values.push(result[i].total);
    }

    var data = {
      labels: labels,
      datasets: [
          {
              label: "My Second dataset",
              fillColor: "rgba(151,187,205,0.2)",
              strokeColor: "rgba(151,187,205,1)",
              pointColor: "rgba(151,187,205,1)",
              pointStrokeColor: "#fff",
              pointHighlightFill: "#fff",
              pointHighlightStroke: "rgba(151,187,205,1)",
              data: values
          }
      ]
    };

    var ctx2 = document.getElementById("myChart2").getContext("2d");
    var myLineChart = new Chart(ctx2).Line(data, {});
  });




});
