app.controller('CategoriesController', function($scope, Category, $window, Auth){
  Auth.role(['admin']);
  $scope.name = null;
  $scope.isSend = false;

  $scope.categories = [];
  $scope.search = {};

  $scope.load = function(){
    Category.index().then(function(data){
      $scope.categories = data;
    });
  };

  $scope.load();

  $scope.create = function(){
    if(!$scope.name) return alert("Nome não pode está vazio!");
    $scope.isSend = true;

    Category.create({ name: $scope.name }).then(function(){
      $scope.isSend = false;
      $scope.name = null;
      $scope.load(); // reload data
    });
  };

  $scope.delete = function(id){
  if(!confirm('Você tem certeza?')) return;
    Category.delete(id);
    $scope.load();
  };



});
