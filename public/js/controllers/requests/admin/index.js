app.controller('VendasAdminController', function($scope, Requests, Auth, User){
  Auth.role('admin');

  $scope.requests = [];
  var store_id = localStorage.getItem('store_id');

  Requests.index().then(function(data) {
    $scope.requests = data;
  });

  $scope.estornar = function(request){
    Requests.estornar(request).then(function(data){
      request.status = 'refunded';
      alert('Pedido estornado com sucesso!');
    }).catch(function(err){
      alert('Erro ao estornar o pedido!');
    });
  };
  
  $scope.finalizar = function(request){
    Requests.finalizar(request._id).then(function(data){
      request.status = 'completed';
      alert('Pedido finalizado com sucesso!');
    }).catch(function(err){
      alert('Erro ao finalizar o pedido!');
    });
  };

  $scope.loadUser = function(request){
    request.expanded = true;
    User.get(request.user_id).then(function(user){
      request.user = user.data;
    });
  };
});
