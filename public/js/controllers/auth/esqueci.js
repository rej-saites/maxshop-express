app.controller('EsqueciController', function(User, $scope, $http){

  var colorFunc = function(x, y) {
      return 'hsl('+Math.floor(Math.abs(x*y)*360)+',80%,60%)';
  };

  var pattern = Trianglify({
        width: window.innerWidth,
        height: window.innerHeight,
        x_colors: ['#6dad5c', '#578a4a', '#406636','#33522c','#6dad5c']
  });

  var elm = document.getElementById('modalback');
  elm.appendChild(pattern.canvas());

  function disabled() {
    var form = document.getElementById('form');
    form.style['pointer-events'] = 'none';
    form.style['opacity'] = '0.5';
  }

  function enable(){
    var form = document.getElementById('form');
    form.style['pointer-events'] = 'auto';
    form.style['opacity'] = '1';
  }

  $scope.send = function(){
    var email = document.getElementById('email').value;
    if(email == null || email.length == 0){
      $scope.message = "E-mail não pode está vazio!";
      return false;
    }

    disabled();

    $http.post('/api/forget_password/',{ email: email }).then(function(){
      enable();
      alert('Enviado com sucesso! Verifique sua caixa de email.')

    }, function(){
      enable();
      alert('E-mail não encontrado em nosso sistema.')

    });
  };

});
