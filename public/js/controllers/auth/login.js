app.controller('login', function(User, $scope, $window, $rootScope, $http){

  var colorFunc = function(x, y) {
      return 'hsl('+Math.floor(Math.abs(x*y)*360)+',80%,60%)';
  };

  var pattern = Trianglify({
        width: window.innerWidth,
        height: window.innerHeight,
        x_colors: ['#6dad5c', '#578a4a', '#406636','#33522c','#6dad5c']
  });

  // x_colors: ['#5885ef', '#496dc5', '#395497','#324a86','#5885ef']
  //  x_colors: ['#c3ddee', '#3498db', '#38b0ff','#2e7eae','#c3ddee']

  var elm = document.getElementById('modalback');
  elm.appendChild(pattern.canvas());

  if(localStorage.getItem("username")){

    if(localStorage.getItem("role") == "owner"){
      $window.location = '#/stores/show/'+localStorage.getItem("store_id");
    } else {
      $window.location = '#/stores';
    }
  }

  function disabled() {
    var form = document.getElementById('form');
    form.style['pointer-events'] = 'none';
    form.style['opacity'] = '0.5';
  }

  function enable(){
    var form = document.getElementById('form');
    form.style['pointer-events'] = 'auto';
    form.style['opacity'] = '1';
  }

  $scope.auth = function(){
    disabled();

    var credentials = {
      email: $scope.email,
      password: $scope.password
    };

    User.auth(credentials).then(function(res){
      console.log(res);
      $rootScope.username = res.data.user.name;
      $rootScope.role     = res.data.user.role;
      $rootScope.email    = res.data.user.email;

      // Token
      localStorage.setItem("token",res.data.token);
      $http.defaults.headers.common['Authorization'] = 'Bearer ' + res.data.token;
      // User
      localStorage.setItem("username", $rootScope.username);
      localStorage.setItem("user_id", res.data.user._id);
      localStorage.setItem("role", res.data.user.role);
      localStorage.setItem("store_id", res.data.user.store_id);
      localStorage.setItem("email", res.data.user.email);

      if($rootScope.role == "owner"){
        $window.location = '#/loja';
      } else {
        $window.location = '#/stores';
      }
    },function(){
      enable()
      $scope.message = "E-mail ou senha incorreta!";
    });
  };

});
