app.controller('EditarProdutoController', function($scope, Product, $filter, $window, $routeParams, Auth, Category){
  Auth.role('owner');

  $scope.product = {};
  $scope.remove = true;


  Category.index().then(function(data){
    $scope.categorys = data;
  });

  $scope.confirmSave = function(){
    Product.update($scope.product._id , { product: $scope.product }).then(function(){
      $window.location = '#/products';
    },function(res){
      window.dispatchEvent(new Event('buttonsend.fail'));
      // error
    });
  };

  $scope.closeModalconfirm = function(){
    window.dispatchEvent(new Event('buttonsend.fail'));
    $scope.confirmModal = false;
  };


  $scope.changeColor = function(index){
    var code = '';

    switch ($scope.product.colors[index].name) {
      case 'Roxo': code = '#721090'; break;
      case 'Berinjela': code = '#570084'; break;
      case 'Magenta': code = '#E03A8E'; break;
      case 'Lilás': code = '#DF8DB7'; break;
      case 'Marinho': code = '#0F174E'; break;
      case 'Azul': code = '#3871C7'; break;
      case 'Azul Royal': code = '#1700A4'; break;
      case 'Turquesa': code = '#42D3FF'; break;
      case 'Petróleo': code = '#154B62'; break;
      case 'Verde Militar': code = '#435022'; break;
      case 'Verde Bandeira': code = '#176336'; break;
      case 'Verde': code = '#229A53'; break;
      case 'Menta': code = '#9FDBC4'; break;
      case 'Bordô': code = '#970A27'; break;
      case 'Vermelho': code = '#D8160A'; break;
      case 'Carmim': code = '#F5000D'; break;
      case 'Pêssego': code = '#EF8A70'; break;
      case 'Caramelo': code = '#B74A1A'; break;
      case 'Telha': code = '#D4371A'; break;
      case 'Laranja': code = '#F76809'; break;
      case 'Coral': code = '#EE6845'; break;
      case 'Amarelo': code = '#FEFF00'; break;
      case 'Mostarda': code = '#E7CB27'; break;
      case 'Areia': code = '#E5E36D'; break;
      case 'Dourado': code = '#E6AC32'; break;
      case 'Café': code = '#4C3224'; break;
      case 'Marrom': code = '#642C1D'; break;
      case 'Paprica': code = '#74261C'; break;
      case 'Camel': code = '#955B37'; break;
      case 'Pão': code = '#CBB595'; break;
      case 'Nude': code = '#D9C9B2'; break;
      case 'Creme': code = '#FDFFD7'; break;
      case 'Branco': code = '#FFFFFF'; break;
      case 'Preto': code = '#000'; break;
      case 'Carbono': code = '#333'; break;
      case 'Cinza': code = '#999'; break;
      case 'Prata': code = '#CCC'; break;
    }
    $scope.product.colors[index].code = code;
  };

  $scope.addColor = function(){
    $scope.product.colors.push({ name: null, code: null });
  };

  $scope.removeColor = function(i){
    $scope.product.colors.splice(i, 1);
  };

  $scope.addSize = function(){
    $scope.product.sizes.push({ name: null});
  };

  $scope.removeSize = function(i){
    $scope.product.sizes.splice(i, 1);
  };

  $scope.quantityChange = function(){
    if($scope.product.quantity < 0) $scope.product.quantity = 0;
  };

  Product.get($routeParams.id).then(function(data){
    $scope.product = data;

    if(data.categories)
      $scope.product.category = data.categories[0];

    $scope.product.price = $filter('money')($scope.product.price);
    $scope.product.old_price = $filter('money')($scope.product.old_price);

    $scope.myImage = data.image_1;
  });

  // Fechar modal crop
  $scope.closeCrop = function(){
    $scope.crop = false;
    Product.upload($scope.croped).then(function(data){
      console.log("sucesso upload");
      $scope.product.images.push(data);
    });
  };

  $scope.closeCropCancel = function(){
    $scope.crop = false;
  };

  $scope.delete = function(){
    var remove = confirm('Tem certeza?');
    if(remove){
      Product.delete($scope.product._id).then(function(){
        $window.location = '#/products';
      });
    }
  };

  $scope.isEdit = true;

  $scope.send = function(){
    if(!checkFieldsProduct($scope)){
       return window.dispatchEvent(new Event('buttonsend.fail'));
    }

    // válidando cores
    var colors = $scope.product.colors;
    if(colors.length > 0){
      $scope.product.colors = colors.filter(function(value){
        return (!!value.name && !!value.code)
      });
    }

    // válidando tamanhos
    var sizes = $scope.product.sizes;
    if(sizes.length > 0){
      $scope.product.sizes = sizes.filter(function(value){
        return (!!value.name)
      });
    }

    $scope.isSend = true;
    $scope.product.categories = [$scope.product.category];

    $scope.confirmModal = true;
  };

  $scope.addImage = function(){
    var filePicker = document.createElement("input");
    filePicker.type = 'file';

    filePicker.onchange = function(evt){
        var file = evt.currentTarget.files[0];
        var reader = new FileReader();

        reader.onload = function (evt) {
          var img = document.createElement("img");
          img.src = evt.target.result;

          Product.upload(evt.target.result).then(function(data){
            console.log("sucesso upload");
            $scope.product.images.push(data);
          });
        };

        reader.readAsDataURL(file);
    };

    filePicker.click();
  };

  $scope.removeImage = function(index){
    $scope.product.images.splice(index, 1);
  };

});
