
app.controller('ProdutoController', function($scope, Product, $window, Auth, Category, $routeParams){
  Auth.role(['admin','owner']);

  $scope.products = [];

  $scope.pageClass = function(i){
    if(i == $routeParams.page || (i == 1 && $routeParams.page == undefined)){
      return 'mark';
    } else {
      return '';
    }
  };

  var store_id = localStorage.getItem('store_id');

  // pegando todas as categorias
  $scope.categories = [];
  Category.index().then(function(data){
    $scope.categories = data;
  });

  var categoryTarget = "";

  function cleanCategoryClass(){
    $scope.categories.forEach(function(elm, i, array) {
        array[i].class = '';
    });
  }

  $scope.byCategory = function($index){
    categoryTarget = $scope.categories[$index].name;
    $scope.term = categoryTarget; // termo da pesquisa para aparecer pro usuário

    $scope.categories.forEach(function(elm, i, array) {
      if(elm.name === categoryTarget){
        array[i].class = 'selected';
      } else {
        array[i].class = '';
      }
    });

    if(!categoryTarget) return;
    Product.categoryProductsByStore(store_id, categoryTarget , $routeParams.page).then(function(res){
      console.log(res);
      $scope.pages = [];

      $scope.products = res.data;
    })
  };

  $scope.edit = function(id){
    $window.location = '#/products/edit/' + id;
  };

  $scope.delete = function(id){
    var remove = confirm('Tem certeza?');
    if(remove){
      Product.delete(id).then(function(){
        pull();
      });
    }
  };

  $scope.search = function(){
    var query = $scope.query;
    $scope.term = query; // termo da pesquisa para aparecer pro usuário
    cleanCategoryClass();

    var store_id = localStorage.getItem('store_id');
    if(!query) return;
    Product.searchInStore(query, store_id).then(function(data){
      $scope.products = data;
    });
  };

  function pull() {
    cleanCategoryClass();
    Product.index($routeParams.page, store_id).then(function(res){
      $scope.products = res.data;
      $scope.pages = [];
      for (var i = 1; i <= res.pages; i++) {
        $scope.pages.push(i)
      }

    });
  }

  $scope.resetProducts = function(){
    $scope.term = null;
    $scope.query = '';
    pull();
  };

  pull();

});


/* Função para validar o formulário para criação e edição
  de produtos*/
  function checkFieldsProduct($scope){
    if($scope.product.images.length == 0) {
      $scope.message = "O produto tem que ter pelo menos uma imagem cadastrada.";
      return false;
    }

    if(!$scope.product.name) {
      $scope.message = "Campo nome não pode estar vazio.";
      return false;
    }

    if(!$scope.product.category) {
      $scope.message = "Campo categoria não pode estar vazio.";
      return false;
    }

    if(!$scope.product.price) {
      $scope.message = "Campo preço não pode estar vazio.";
      return false;
    }

    if(!$scope.product.description) {
      $scope.message = "Campo descrição não pode estar vazio.";
      return false;
    }

    return true;
  }
