app.controller('PromotionsController', function($scope, Auth, Banner, Store){
  Auth.role('admin');

  $scope.banners  = [];

  function pull(){
    Banner.index().then(function(data){
      $scope.banners = data;
      for (var i = 0; i < $scope.banners.length; i++) {
        // definindo dias restantes
        if($scope.banners[i].period.indeterminate == false)
        $scope.banners[i].days = moment().from($scope.banners[i].period.end, true);
        // pegando info de store
        (function(i){
          Store.get($scope.banners[i].store_id).then(function(data){
            $scope.banners[i].store = data;
          });
        })(i);
      }
    });
  }

  pull();

  $scope.delete = function($index){
    Banner.delete($scope.banners[$index]._id).then(function(){
      pull();
    });
  };

});
