app.controller('NewPromotionController', function($scope, Auth, Product, Banner, $window, $routeParams){
  Auth.role('admin');

  $scope.imageBanner = null;
  var store_id = localStorage.getItem("store_id");

  $scope.days = 15;
  $scope.pretty_days = 0;

  var banner_id = $routeParams.id ;

  $scope.storeAndProduct = {
    store_id: null,
    product_id: null,
    store_action: false
  };


  $scope.$watch('days',function(){
    $scope.pretty_days = moment().add($scope.days, 'd').format("DD [de] MMMM");
  });

  var elmdays = document.getElementById('daysdiv');
  $scope.checkbox_forever = false;

  $scope.$watch('checkbox_forever',function(){
    if($scope.checkbox_forever == true){
      elmdays.style['pointer-events'] = 'none';
      elmdays.style['opacity'] = '0.5';
    } else {
      elmdays.style['pointer-events'] = 'auto';
      elmdays.style['opacity'] = '1';
    }
  });


  // escolher uma imagem.
  var fileInput =   document.getElementById('file');

  fileInput.onchange = function(evt){
      var file = evt.currentTarget.files[0];
      var reader = new FileReader();

      reader.onload = function (evt) {
        $scope.$apply(function(){
          $scope.image = evt.target.result ;
        });
      };

      reader.readAsDataURL(file);
  };

  $scope.pickImage = function(){
    fileInput.click();
  };


  $scope.save = function(){
     var data = {
       image: $scope.image,
       store_id: $scope.storeAndProduct.store_id,
       period: {
         start: moment().toDate(),
         end: moment().add($scope.days, 'd').toDate()
       }
     };

     data.period.indeterminate = $scope.checkbox_forever;


     if(data.image == null)
       return alert("Não é possível salvar sem definir a imagem do banner.");

     if(data.store_id == null)
       return alert("Não é possível salvar sem definir a loja.");

       data.action = {};

       if($scope.storeAndProduct.store_action == true) {
         data.action.target = 'default';
         data.action.url = '#/loja/'+data.store_id;
       } else {
         data.action.target = 'default';
         data.action.url = '#/produto/'+ $scope.storeAndProduct.product_id;
       }

     Banner.create(data).then(function(data){
      window.location = "#/promotions";
     });
  };


});
