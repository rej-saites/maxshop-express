app.controller('LojaController', function($scope, Store, $window, Auth, $routeParams){
  Auth.role(['admin','owner']);

  $scope.store = null;
  var store_id = $routeParams.id || localStorage.getItem("store_id");

  Store.get(store_id).then(function(data){
    $scope.store = data;
  });

  // get user of store
  Store.user(store_id).then(function(data){
    $scope.user = data;
  });

  $scope.editStore = function(){
    $window.location = "#/stores/edit/" + store_id;
  };

});


app.controller('StoreRedirect', function($window, Auth){
  Auth.role(['owner']);
  $window.location = '#/stores/show/'+localStorage.getItem("store_id");
});
