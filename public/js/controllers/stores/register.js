/* Controller responsável pela finalização do cadastro de uma loja. */
app.controller('RegisterController', function($scope, Store, User, $http, $window){

  // Conseguindo os bancos cadastrados.
  $scope.banks = Store.getBanks();

  $scope.user = { cdl: false };

  $scope.store = {
    company: {},
    docs: {},
    address: {},
    contact_responsible: {},
    vitrine: false
  };

  $scope.onlyNumbers = function(name){
    if($scope.store.bank_account[name])
    $scope.store.bank_account[name] =   $scope.store.bank_account[name].replace(/[^0-9]/g, '');
  };

  $scope.vitrineChange = function(){
    if($scope.store.vitrine == false ) $scope.store.hide_price = false;
  };

  function getElm(id){
    return document.getElementById(id);
  }

  var inputs = [
    // company
    { group: 'company', name: 'name',         elm: getElm('company-name')},
    { group: 'company', name: 'fantasy_name', elm: getElm('company-fantasy_name')},
    { group: 'company', name: 'cnpj',         elm: getElm('company-cnpj')},
    { group: 'company', name: 'phone',        elm: getElm('company-phone')},
    // address
    { group: 'address', name: 'cep',         elm: getElm('address_cep')},
    { group: 'address', name: 'street',      elm: getElm('address_street')},
    { group: 'address', name: 'number',      elm: getElm('address_number')},
    { group: 'address', name: 'district',    elm: getElm('address_district')},
    { group: 'address', name: 'city',        elm: getElm('address_city')},
    { group: 'address', name: 'state',       elm: getElm('address_state')},
    // contact responsible
    { group: 'contact_responsible', name: 'name',       elm: getElm('contact_responsible_name')}

  ];

  // Testar se o campo está nulo.
  function isBlank(str) {
    return (!str || /^\s*$/.test(str));
  }

  // Validar inputs
  function validateInputs(){
    var erros = false; // caso tenha sido encontrados erros
    var fisrt = null; // primeiro input com erro

    // verificando se nome de usuário está vazio.
    if(isBlank($scope.user.name)){
      var elm = document.getElementById('user-name');
      elm.className = 'input-error';
      fisrt = elm;
    }

    // verificando se o email do usuário está vazio.
    if(isBlank($scope.user.email)){
      var elm = document.getElementById('user-email');
      elm.className = 'input-error';
      if(!fisrt) fisrt = elm;
    }

    inputs.forEach(function(input, i, array){
      var value = $scope.store[input.group][input.name];
      if(isBlank(value)){
        erros = true;
        if(fisrt == null) fisrt = input.elm;

        if(input.elm == null ) return console.log(input);
        input.elm.className = 'input-error';

        return false;
      } else {

        if(input.name == 'cpf'){
          if(!TestaCPF(value)){
            erros = true;
            if(fisrt == null) fisrt = input.elm;
            input.elm.className = 'input-error';
            return false;
          } else {
            input.elm.className = '';
          }
        } else if(input.name == 'cnpj'){
          if(!validarCNPJ(value)){
            erros = true;
            if(fisrt == null) fisrt = input.elm;
            input.elm.className = 'input-error';
            return false;
          } else {
            input.elm.className = '';
           }
        } else if(input.elm == null ) {
          return console.log(input);
        } else {
          input.elm.className = '';
        }

      }
    });


    if(fisrt != null) fisrt.focus();
    return erros;
  }



  $scope.cepChange = function(){

      $.ajax({
       type: "POST",
       url: "http://cep.republicavirtual.com.br/web_cep.php?formato=json&cep="+$scope.store.address.cep,
       dataType: "json"
       }).done(function(data) {
         console.log(data);
          $scope.$apply(function(){
            $scope.store.address.district = data.bairro;
            $scope.store.address.city = data.cidade;
            $scope.store.address.street = data.logradouro;
            $scope.store.address.state = data.uf;
          });
       });

  };


  var inprogress = false;
  // para mostrar o último modal do fluxo da página.
  $scope.finish = false;

  var store = $scope.store;

  $scope.save = function(){
    if(inprogress) return;

    if(validateInputs()){
      $scope.message = "Formulário com dados incompletos!";
      return;
    } else {
      $scope.message = null;
    }

    store = $scope.store;
    store.cdl = $scope.user.cdl;
    store.cnpj = store.company.cnpj;
    store.phones = [ store.company.phone ];
    store.corporate_name = store.company.name;
    store.name = store.company.fantasy_name;

    if(store.vitrine == true) store.bank_account = {};

    $scope.modal = true;
  };

   $scope.aceito = false;

   $scope.close = function() {
     $scope.modal = false;
   };

   $scope.create = function(){
     if($scope.aceito == false) return;

     $scope.modal = false;
     inprogress = true;
     // criando loja
     Store.create({ store: store, user: $scope.user }).then(function(data){
       // mostrar modal dizendo para verificar email.. .
       $scope.finish = true;
     }, function(){
       inprogress = false;
       $scope.message = "Problemas ao criar a loja!";
     });
   };
});
