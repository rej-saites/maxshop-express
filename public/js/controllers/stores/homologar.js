app.controller("HomologarController", function($scope, Auth, Store, User, $window, $routeParams){
  Auth.role('admin');

  $scope.stores = [];

  $scope.pageClass = function(i){
    if(i == $routeParams.page || (i == 1 && $routeParams.page == undefined)){
      return 'mark';
    } else {
      return '';
    }
  };

  Store.homologarStores($routeParams.page).then(function(res){
    $scope.pages = [];
    for (var i = 1; i <= res.pages; i++) {
      $scope.pages.push(i)
    }

    $scope.stores = res.data;
    for (var i = 0; i < $scope.stores.length; i++) {
      (function(i){
        User.getByStore($scope.stores[i]._id).then(function(res){
          console.log(res);
          $scope.stores[i].user = res;
          $scope.stores[i].due_time = moment($scope.stores[i].created_at).add(72, 'hours').format('DD/MM/YYYY  h:mm a');
        });
      })(i)
    }
  });

  $scope.show = function(id){
    $window.location = "#/homologar/store/"+id;
  };

});
