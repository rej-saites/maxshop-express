app.controller("HomologarStoreController", function($scope, Auth, Store, User, $window, $routeParams){
  Auth.role('admin');

  var store_id = $routeParams.id;
  $scope.store = {};

  Store.get(store_id).then(function(data){
    $scope.store = data;
    $scope.due_time = moment(data.created_at).add(72, 'hours').format('DD/MM/YYYY  h:mm a');
  });

  Store.user(store_id).then(function(data){
    $scope.user = data;
  });

  $scope.update = function(){
    Store.homologar(store_id, {  pagarmeApiKey: $scope.pagarmeApiKey }).then(function(){
      $window.location = "#/homologar";
    });
  };
});
