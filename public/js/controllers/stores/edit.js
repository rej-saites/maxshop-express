
app.controller('EditarLojaController', function($scope, Store, $window, $routeParams, User, Auth){
  Auth.role(['admin', 'owner']);

  $scope.isRole = function(role){
    return Auth.isRole(role);
  }

  $scope.store = null;
  // Conseguindo os bancos cadastrados.
  $scope.banks = Store.getBanks();

  var store_id = $routeParams.id;
  console.log(store_id);

  // get store data
  Store.get(store_id).then(function(data){
    console.log(data);
    data.account = data.account || {};
    $scope.store = data;
  }, function(){
    console.err("problemas com get store!");
  });

  // get user of store
  Store.user(store_id).then(function(data){
    $scope.user = data;
  });

  $scope.back = function(){
    $window.history.back();
  };

  $scope.onlyNumbers = function(name){
    if($scope.store.bank_account[name])
    $scope.store.bank_account[name] =   $scope.store.bank_account[name].replace(/[^0-9]/g, '');
  };

  $scope.update = function(){
    function fail() {
      window.dispatchEvent(new Event('buttonsend.fail'));
    }

    if($scope.store.vitrine == false){

      if(!$scope.store.partner_admin){
        $scope.store.partner_admin = {};
      }

      if(!$scope.store.partner_admin.name){
        $scope.message = "Nome do sócio administrador não pode estar vazio.";
        return fail();
      }

      if(!$scope.store.partner_admin.cpf){
        $scope.message = "CPF do sócio administrador não pode estar vazio.";
        return fail();
      }

      if(!TestaCPF($scope.store.partner_admin.cpf)){
        $scope.message = "CPF do sócio administrador não é válido";
        return fail();
      }

      if(!$scope.store.partner_admin.phone){
        $scope.message = "Telefone do sócio administrador não pode estar vazio.";
        return fail();
      }

      if(!$scope.store.bank_account){
        $scope.store.bank_account = {};
      }


      if(!$scope.store.bank_account.legal_name){
        $scope.message = "Nome legal associado a conta não pode estar vazio.";
        return fail();
      }

      if(!$scope.store.bank_account.bank_code){
        $scope.message = "Banco não pode estar vazio.";
        return fail();
      }

      if(!$scope.store.bank_account.agencia){
        $scope.message = "Agência do banco não pode estar vazia.";
        return fail();
      }

      if(!$scope.store.bank_account.agencia_dv){
        $scope.message = "DV da Agência do banco não pode estar vazia.";
        return fail();
      }

      if(!$scope.store.bank_account.conta){
        $scope.message = "Conta do banco não pode estar vazia.";
        return fail();
      }

      if(!$scope.store.bank_account.conta_dv){
        $scope.message = "DV da Conta do banco não pode estar vazia.";
        return fail();
      }
    }

    if(!checkFieldsStore($scope)) return fail();

    var body = {
       store: $scope.store
    };

    if($scope.image) body.image = $scope.image;

    Store.update(store_id, body).then(function(){
      var role = localStorage.getItem("role");
      if(role == 'admin'){
        $window.location = '#/stores';
      } else {
        $window.location = '#/store';
      }
    }, function(){
      window.dispatchEvent(new Event('buttonsend.fail'));
    });

  };

  $scope.addImage = function(){
    var filePicker = document.createElement("input");
    filePicker.type = 'file';

    filePicker.onchange = function(evt){
        var file = evt.currentTarget.files[0];
        var reader = new FileReader();

        reader.onload = function (evt) {
          var img = document.createElement("img");
          img.src = evt.target.result;

          $scope.$apply(function(){
            $scope.image = evt.target.result ;
          });
        };

        reader.readAsDataURL(file);
    };

    filePicker.click();
  };


  // Fechar modal crop
  $scope.closeCrop = function(){
    $scope.crop = false;
    $scope.image = $scope.croped;
  };

  $scope.isEditRoute = function(){
    return true;
  };


  $scope.cepChange = function(){
    $.ajax({
     type: "POST",
     url: "http://cep.republicavirtual.com.br/web_cep.php?formato=json&cep="+$scope.store.address.cep,
     dataType: "json"

     }).done(function(data) {
       console.log(data);
        $scope.$apply(function(){
          $scope.store.address.district = data.bairro;
          $scope.store.address.city = data.cidade;
          $scope.store.address.street = data.logradouro;
          $scope.store.address.state = data.uf;
        });
     });
  };

});
