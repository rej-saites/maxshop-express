app.controller('CadastraLojaController', function($scope, Store, $window, Auth, User){
  Auth.role('admin');

  $scope.isRole = function(role){
    return Auth.isRole(role);
  }

  $scope.store = {};
  $scope.store.address = {};
  $scope.store.account = {};
  $scope.user = {};

  // Conseguindo os bancos cadastrados.
  $scope.banks = Store.getBanks();


  // váriaveis pra validar emails do formulário
  $scope.emails = {};
  $scope.emails.store = false;
  $scope.emails.user = false;


  $scope.create = function(){

    if(!checkFieldsStore($scope)) {
      window.dispatchEvent(new Event('buttonsend.fail'));
      return;
    }

    if(!$scope.emails.store || !$scope.emails.user){
      window.dispatchEvent(new Event('buttonsend.fail'));
      return $scope.message = "Email da loja e do usuário não podem ser iguais.";
    }

    function validAccount(){
      if(!$scope.store.account.legal_name)
        return $scope.message = "Nome legal da conta bancária não pode está vazio.";
      if(!$scope.store.account.document_number)
        return $scope.message = "CNPJ associado a conta bancária não pode está vazio.";
      if(!$scope.store.account.bank_code)
        return $scope.message = "O banco da conta bancária não pode está vazio.";
      if(!$scope.store.account.agencia)
        return $scope.message = "A agência da conta bancária não pode está vazio.";
      if(!$scope.store.account.conta)
        return $scope.message = "A o número da conta bancária não pode está vazio.";

        return false;
    }

    if(validAccount()) {
      window.dispatchEvent(new Event('buttonsend.fail'));
      return false;
    }


    console.log($scope.store);

    $scope.isSend = true;
    Store.create({ image: $scope.image, store: $scope.store, user: $scope.user })
    .then(function(data){
      $window.location = '#/stores';
    },function(res){
        if(res === 'conta bancária'){
          $scope.message = "Problemas com a conta bancária, verifique os dados.";
        } else {
          $scope.message = "Problemas ao criar a loja.";
        }

        window.dispatchEvent(new Event('buttonsend.fail'));
    });
  };

  $scope.addImage = function(){
    var filePicker = document.createElement("input");
    filePicker.type = 'file';

    filePicker.onchange = function(evt){
        var file = evt.currentTarget.files[0];
        var reader = new FileReader();

        reader.onload = function (evt) {
          var img = document.createElement("img");
          img.src = evt.target.result;

          $scope.$apply(function(){
            $scope.image = evt.target.result ;
          });
        };

        reader.readAsDataURL(file);
    };

    filePicker.click();
  };


  // Verificando disponibilidade de email da loja
  $scope.checkEmail = function(){
    var elm  = document.getElementById('email-message');

    $scope.emails.store = false;
    function validateEmail(email) { return /\S+@\S+\.\S+/.test(email); }

    if(!validateEmail($scope.store.email))
      return elm.innerHTML = "<p class=\"message-error\">E-mail não é válido.</p>";

    elm.innerHTML = "<p class=\"message-alert\">Verificando disponibilidade.</p>";

    Store.checkEmail($scope.store.email).then(function(unique){
      if(unique){
        $scope.emails.store = true;
        elm.innerHTML = "<p class=\"message-sucess\">E-mail disponível.</p>";
      } else {

        elm.innerHTML = "<p class=\"message-error\">E-mail já está em uso.</p>";
      }
    });
  };


  // Verificando disponibilidade de email do usuário.
  $scope.checkEmailUser = function(){
    var elm  = document.getElementById('email-user-message');

    function validateEmail(email) { return /\S+@\S+\.\S+/.test(email); }

    $scope.emails.user = false;
   if(!validateEmail($scope.user.email))
     return elm.innerHTML = "<p class=\"message-error\">E-mail não é válido.</p>";

    elm.innerHTML = "<p class=\"message-alert\">Verificando disponibilidade.</p>";

    User.checkEmail($scope.user.email).then(function(unique){
      if(unique){
        $scope.emails.user = true;
        elm.innerHTML = "<p class=\"message-sucess\">E-mail disponível.</p>";
      } else {
        elm.innerHTML = "<p class=\"message-error\">E-mail já está em uso.</p>";
      }
    });
  };


});


/* Função para validar o formulário para criação de lojas*/
  function checkFieldsStore($scope){

    if(!$scope.store.avatar && !$scope.image) {
      $scope.message = "Imagem da loja não pode estar vazia.";
      return false;
    }

    if(!$scope.store.name) {
      $scope.message = "Campo nome da loja não pode estar vazio.";
      return false;
    }


    if($scope.store.address){
      if(!$scope.store.address.state) {
        $scope.message = "Campo estado não pode estar vazio.";
        return false;
      }

      if(!$scope.store.address.city) {
        $scope.message = "Campo cidade não pode estar vazio.";
        return false;
      }

      if(!$scope.store.address.street) {
        $scope.message = "Campo rua não pode estar vazio.";
        return false;
      }

      if(!$scope.store.address.district) {
        $scope.message = "Campo bairro não pode estar vazio.";
        return false;
      }

      if(!$scope.store.address.number) {
        $scope.message = "Campo número não pode estar vazio.";
        return false;
      }

    }

    return true;
  }
