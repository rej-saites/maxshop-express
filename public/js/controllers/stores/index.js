
app.controller('ListaLojaController', function($scope, Store, User, $window, Auth, $routeParams){
  Auth.role('admin');

  $scope.stores = [];

  $scope.pageClass = function(i){
    if(i == $routeParams.page || (i == 1 && $routeParams.page == undefined)){
      return 'mark';
    } else {
      return '';
    }
  };

  function loadStores(){
    Store.index($routeParams.page).then(function(res){
      $scope.pages = [];
      for (var i = 1; i <= res.pages; i++) {
        $scope.pages.push(i)
      }

      $scope.total_stores = res.stores;

      console.log($scope.pages);
      $scope.stores = res.data;
      for (var i = 0; i < $scope.stores.length; i++) {
        (function(i){
          User.getByStore($scope.stores[i]._id).then(function(res){
            $scope.stores[i].user = res;
          });
        })(i)
      }
    });
  }

  loadStores();

  $scope.show = function(id){
    $window.location = '#/stores/show/'+id;
  };

  $scope.edit = function(id){
    $window.location = '#/stores/edit/'+id;
  };

  $scope.delete = function(id){
  if(!confirm('Você tem certeza?')) return;
    Store.delete(id);
    loadStores();
  };


});
