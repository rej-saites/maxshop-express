app.controller('RelatorioStoreController', function($scope, Auth, Reports){
  Auth.role('owner');

  var store_id = localStorage.getItem("store_id");

  Reports.allProductsStore(store_id).then(function(total){
    $scope.totalProducts = total;
  });

  Reports.allViewsStore(store_id).then(function(total){
    $scope.totalViews = total;
  });

  Reports.allLoversStore(store_id).then(function(total){
    $scope.totalLovers = total;
  });

  Reports.monthRequestsStore(store_id).then(function(result){

    var labels = [];
    for (var i = 0; i < result.length; i++) {
      labels.push(result[i].month);
    }

    var values = [];
    for (var i = 0; i < result.length; i++) {
      values.push(result[i].total);
    }

    var data = {
      labels: labels,
      datasets: [
          {
              label: "My Second dataset",
              fillColor: "rgba(151,187,205,0.2)",
              strokeColor: "rgba(151,187,205,1)",
              pointColor: "rgba(151,187,205,1)",
              pointStrokeColor: "#fff",
              pointHighlightFill: "#fff",
              pointHighlightStroke: "rgba(151,187,205,1)",
              data: values
          }
      ]
    };

    var ctx = document.getElementById("myChart").getContext("2d");
    var myLineChart = new Chart(ctx).Line(data, {});
  });


  Reports.dailyRequestsStore(store_id).then(function(result){

    var labels = [];
    for (var i = 0; i < result.length; i++) {
      labels.push(result[i].day);
    }

    var values = [];
    for (var i = 0; i < result.length; i++) {
      values.push(result[i].total);
    }

    var data = {
      labels: labels,
      datasets: [
          {
              label: "My Second dataset",
              fillColor: "rgba(151,187,205,0.2)",
              strokeColor: "rgba(151,187,205,1)",
              pointColor: "rgba(151,187,205,1)",
              pointStrokeColor: "#fff",
              pointHighlightFill: "#fff",
              pointHighlightStroke: "rgba(151,187,205,1)",
              data: values
          }
      ]
    };

    var ctx2 = document.getElementById("myChart2").getContext("2d");
    var myLineChart = new Chart(ctx2).Line(data, {});
  });




});
