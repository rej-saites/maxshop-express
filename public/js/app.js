var app = angular.module('my-app',['ngRoute','naif.base64', 'angular-loading-bar', 'ngMask']);

app.config(['$routeProvider',
  function($routeProvider) {
    var rp = $routeProvider;

    rp.when('/', {
      templateUrl: 'views/auth/login.html',
      controller: 'login'
    });

    rp.when('/esqueci-senha', {
      templateUrl: 'views/auth/esqueci.html',
      controller: 'EsqueciController'
    });

    rp.when('/products', {
      templateUrl: 'views/products/index.html',
      controller: 'ProdutoController'
    });

    rp.when('/products/new', {
      templateUrl: 'views/products/form.html',
      controller: 'NovoProdutoController'
    });

    rp.when('/products/edit/:id', {
      templateUrl: 'views/products/form.html',
      controller: 'EditarProdutoController'
    });

    // vendas para lojas
    rp.when('/vendas', {
      templateUrl: 'views/desativadas/vendas.html',
      controller: 'VendasController'
    });

    //
    rp.when('/vendas/visao_geral', {
      templateUrl: 'views/vendas-admin.html',
      controller: 'VendasAdminController'
    });

    rp.when('/stores/new', {
      templateUrl: 'views/stores/new.html',
      controller: 'CadastraLojaController'
    });

    rp.when('/stores/edit/:id', {
      templateUrl: 'views/stores/edit.html',
      controller: 'EditarLojaController'
    });

    rp.when('/stores', {
      templateUrl: 'views/stores/index.html',
      controller: 'ListaLojaController'
    });



    rp.when('/stores/show/:id', {
      templateUrl: 'views/stores/show.html',
      controller: 'LojaController'
    });

    rp.when('/store', {
      templateUrl: 'views/stores/show.html',
      controller: 'StoreRedirect'
    });

    rp.when('/relatorio/admin', {
      templateUrl: 'views/relatorio-admin.html',
      controller: 'RelatorioAdminController'
    });

    rp.when('/relatorio/store', {
      templateUrl: 'views/relatorio-store.html',
      controller: 'RelatorioStoreController'
    });

    rp.when('/promotions', {
      templateUrl: 'views/banners/admin/index.html',
      controller: 'PromotionsController'
    });

    rp.when('/promotions/new', {
      templateUrl: 'views/banners/admin/new.html',
      controller: 'NewPromotionController'
    });


    rp.when('/categories', {
      templateUrl: 'views/categories/index.html',
      controller: 'CategoriesController'
    });

    rp.when('/register', {
      templateUrl: 'views/stores/register.html',
      controller: 'RegisterController'
    });

    rp.when('/homologar', {
      templateUrl: 'views/stores/homologar.html',
      controller: 'HomologarController'
    });

    rp.when('/homologar/store/:id', {
      templateUrl: 'views/stores/homologar_store.html',
      controller: 'HomologarStoreController'
    });

    rp.when('/suporte', {
      templateUrl: 'views/faq/suporte.html',
      controller: 'CategoriesController'
    });

    rp.when('/faq', {
      templateUrl: 'views/faq/index.html',
      controller: 'CategoriesController'
    });

    rp.otherwise({
      redirectTo: '/'
    });

  }]);


app.constant("PAYMENT_GATEWAY","http://proxy.maxshoplus.com.br");

app.run(function($rootScope, $window, $http, User){
  $rootScope.username = localStorage.getItem("username");
  $rootScope.email = localStorage.getItem("email");
  var token = localStorage.getItem("token");

  if(token) {
    $http.defaults.headers.common['Authorization'] = 'Bearer ' + token;
  }

  $rootScope.signOut = function(){
    $rootScope.changePassword = false;
    localStorage.clear();
    $window.location = "#/";
  };

  $rootScope.editPassword = function(){
    $rootScope.changePassword = true;
  };

  $rootScope.closeEditPassword = function(){
    $rootScope.changePassword = false;
  };

  $rootScope.updatePassword = function(){
    var elm1 = document.getElementById('password');
    var elm2 =  document.getElementById('new_password');

    var data = {
      id: localStorage.getItem('user_id'),
      password: elm1.value,
      new_password: elm2.value,
    };

    if(!data.password){
      elm1.focus();
      elm1.style.outline = '1px solid red';
      return;
    }

    elm1.style.outline = 'none';

    if(!data.new_password){
      elm2.focus();
      elm2.style.outline = '1px solid red';
      return;
    }

    elm2.style.outline = 'none';

    User.changePassword(data).then(function(){
      alert('Senha alterada com sucesso!');
      $rootScope.changePassword = false;
      elm1.value = '';
      elm2.value = '';
    }, function(){
      alert('Problema com a senha atual, verifique e tente novamente!');
    });
  };

});


function TestaCPF(strCPF) {
    strCPF = strCPF.replace(/[^\d]+/g, '');
    var Soma;
    var Resto;
    Soma = 0;
	if (strCPF == "00000000000") return false;

	for (i=1; i<=9; i++) Soma = Soma + parseInt(strCPF.substring(i-1, i)) * (11 - i);
	Resto = (Soma * 10) % 11;

    if ((Resto == 10) || (Resto == 11))  Resto = 0;
    if (Resto != parseInt(strCPF.substring(9, 10)) ) return false;

	Soma = 0;
    for (i = 1; i <= 10; i++) Soma = Soma + parseInt(strCPF.substring(i-1, i)) * (12 - i);
    Resto = (Soma * 10) % 11;

    if ((Resto == 10) || (Resto == 11))  Resto = 0;
    if (Resto != parseInt(strCPF.substring(10, 11) ) ) return false;
    return true;
}


function validarCNPJ(cnpj) {

	cnpj = cnpj.replace(/[^\d]+/g,'');

	if(cnpj == '') return false;

	if (cnpj.length != 14)
		return false;

	// Elimina CNPJs invalidos conhecidos
	if (cnpj == "00000000000000" ||
		cnpj == "11111111111111" ||
		cnpj == "22222222222222" ||
		cnpj == "33333333333333" ||
		cnpj == "44444444444444" ||
		cnpj == "55555555555555" ||
		cnpj == "66666666666666" ||
		cnpj == "77777777777777" ||
		cnpj == "88888888888888" ||
		cnpj == "99999999999999")
		return false;

	// Valida DVs
	tamanho = cnpj.length - 2
	numeros = cnpj.substring(0,tamanho);
	digitos = cnpj.substring(tamanho);
	soma = 0;
	pos = tamanho - 7;
	for (i = tamanho; i >= 1; i--) {
	  soma += numeros.charAt(tamanho - i) * pos--;
	  if (pos < 2)
			pos = 9;
	}
	resultado = soma % 11 < 2 ? 0 : 11 - soma % 11;
	if (resultado != digitos.charAt(0))
		return false;

	tamanho = tamanho + 1;
	numeros = cnpj.substring(0,tamanho);
	soma = 0;
	pos = tamanho - 7;
	for (i = tamanho; i >= 1; i--) {
	  soma += numeros.charAt(tamanho - i) * pos--;
	  if (pos < 2)
			pos = 9;
	}
	resultado = soma % 11 < 2 ? 0 : 11 - soma % 11;
	if (resultado != digitos.charAt(1))
		  return false;

	return true;

}
