/* Componente que faz pesquisa entre produtos e lojas.. sendo que pode selecionar
apenas um elemento dessas duas entidades. Feito primordiamente para ser usado
na telas banners, mas pode vir a ser útil em alguma outra situação. */
app.directive('maxTwoSearch', function(Store, Product){
  return {
    restrict: 'E',
    require: 'ngModel',
    templateUrl: './views/templates/directives/two_search.html',
    link: function($scope, element, attrs, ngModel) {
      // DOM element: Div of product search
      var elm_product = angular.element('.two-tab-product')[0];
      var elm_result_search_store = angular.element('.result-search')[0];
      var elm_result_search_product = angular.element('.result-search')[1];

      $scope.model = {};
      $scope.checkbox_store = false;

      setTimeout(function () {
        $scope.model = ngModel.$modelValue;

        if($scope.model.store_id)
        Store.get($scope.model.store_id).then(function(data){
          $scope.store = data;
        });

        if($scope.model.product_id)
        Product.get($scope.model.product_id).then(function(data){
          $scope.product = data;
        });

        $scope.$apply(function(){
          $scope.checkbox_store = ngModel.$modelValue.store_action;
        });
      }, 10);



      $scope.$watch('checkbox_store', function(){
        $scope.model.store_action = $scope.checkbox_store;

        if($scope.checkbox_store == true){
          elm_product.style['pointer-events'] = 'none';
          elm_product.style['opacity'] = '0.5';
        } else {
          elm_product.style['pointer-events'] = 'auto';
          elm_product.style['opacity'] = '1';
        }
      });

      $scope.store_query = "";
      $scope.store_results = [];

      $scope.storeSearch = function(){
        $scope.store = null;
        Store.search($scope.store_query).then(function(res){
          $scope.isStoreSearch = true;
          $scope.store_results = res;

          if(res.length > 0) elm_result_search_store.style.height = '150px';
        });
      };

      $scope.setStore = function(index){
        $scope.store =  $scope.store_results[index];
        $scope.isStoreSearch = false;
        elm_result_search_store.style.height = '60px';

        $scope.model.store_id = $scope.store._id
      };

      $scope.product_query = "";
      $scope.product_results = [];
      $scope.productSearch = function(){
        $scope.product = null;
        Product.search($scope.product_query).then(function(res){
          $scope.isProductSearch = true;
          $scope.product_results = res;
          console.log(res);

          if(res.length > 0) elm_result_search_product.style.height = '150px';
        });
      };

      $scope.setProduct = function(index){
        $scope.product =  $scope.product_results[index];
        $scope.isProductSearch = false;
        elm_result_search_product.style.height = '60px';

        $scope.model.product_id = $scope.product._id
      };

    }
  };
});
