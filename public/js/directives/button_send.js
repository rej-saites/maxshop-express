/* Botão pra enviar ajax, animação */
app.directive('maxButtonSend', function(Store, Product){
  return {
    restrict: 'E',
    transclude: true,
    scope: {
      action: "&"
    },
    templateUrl: './views/templates/directives/button_send.html',
    link: function($scope, element, attrs) {

      window.addEventListener('buttonsend.fail', function() {
        $scope.loading = false;
      });

      $scope.send = function(){
        $scope.loading = true;
        $scope.action();
      };
    }
  }
})
