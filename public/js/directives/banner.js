
/* Botão pra enviar ajax, animação */
app.directive('maxBanner', function(Store, Product){
  return {
    restrict: 'E',
    transclude: true,
    scope: {},
    templateUrl: './views/templates/directives/banner.html',
    link: function($scope, element, attrs) {

      function safeApply(scope, fn) {
          (scope.$$phase || scope.$root.$$phase) ? fn() : scope.$apply(fn);
      }

     var objs = [
       {
         type: 'image',
         x: 120,
         y: 160,
         width: '100px',
         height: '100px',
         src: 'http://maxshop-redhateste.rhcloud.com/uploads/n0sredeb3ej0pb9ufr89edqpzy6i529.png'
       },
       {
         type: 'image',
         x: 50,
         y: 50,
         width: '100px',
         height: '100px',
         src: 'http://maxshop-redhateste.rhcloud.com/uploads/86n2tpa0e6sqncdi8o57y0t0flvm42t9.png'
       },
       {
         type: 'textarea',
         x: 100,
         y: 200,
         text: 'Hello World',
         size: '18px',
         font: 'Arial',
         align: 'left',
         width: '100px',
         height: '100px',
         color: '#333'
       }
     ];

     $scope.layers = [];

     $scope.textElement = {};

     $scope.trigerElement = function(i){
       $scope.layers[i].elm.click();
     };

     $scope.setAlign = function(name){
       $scope.textElement.style.textAlign = name;
     }

     var editor = document.getElementById('banner-editor');

     var arrow = document.getElementById('scale-elements');
     var arrowdrag = false;
     var targetarrow = null;
     var pointstart = 0;

     var arrowRight = document.getElementById('scaleX');
     var arrowX = false;

     var arrowTop = document.getElementById('scaleY');
     var arrowY = false;

     var textEditor = document.getElementById('editor-text');

     function cleanOptions(){
       if(targetarrow) targetarrow.style['border'] = 'none';
       if(dragObj) dragObj.style['border'] = 'none';

       arrow.style['display'] = 'none';
       arrowRight.style['display'] = 'none';
       arrowTop.style['display'] = 'none';
       move.style['display'] = 'none';
       textEditor.style['display'] = 'none';
     }

     var move = document.getElementById('move');
     move.onmousedown = function(e){
       diff_x = (e.pageX - editor.offsetLeft) - targetarrow.offsetLeft;
       diff_y = (e.pageY - editor.offsetTop) - targetarrow.offsetTop;
       cleanOptions();
       draggable(targetarrow);
       targetarrow.style['border'] = '1px dashed #23bef7';
     };

     arrow.onmousedown = function(e){
       targetarrow.style['border'] = '1px dashed #23bef7';

       targetWidth = targetarrow.clientWidth;
       targetHeight = targetarrow.clientHeight;

       arrowdrag = true;
       diff_y = (e.pageY - editor.offsetTop) - arrow.offsetTop;
       diff_x = (e.pageX - editor.offsetLeft) - arrow.offsetLeft;
       pointstart = e.pageY;
     };

     arrowRight.onmousedown = function(e){
       targetarrow.style['border'] = '1px dashed #23bef7';

       targetWidth = targetarrow.clientWidth;
       targetHeight = targetarrow.clientHeight;

       arrowX = true;
       diff_x = (e.pageX - editor.offsetLeft) - arrow.offsetLeft;
       pointstart = e.pageX;
     };

     arrowTop.onmousedown = function(e){
       targetarrow.style['border'] = '1px dashed #23bef7';

       targetWidth = targetarrow.clientWidth;
       targetHeight = targetarrow.clientHeight;

       arrowY = true;
       diff_y = (e.pageY - editor.offsetTop) - arrow.offsetTop;
       pointstart = e.pageY;
     };

     function createElement(obj){
       switch (obj.type) {
         case 'image':
          (function(){
             var elm = new Image(obj.width, obj.height);
             elm.src = obj.src;
             elm.style['position'] = 'absolute';
             elm.style['left'] = obj.x + 'px';
             elm.style['top']  = obj.y + 'px';
             elm.style['width']  = obj.width;
             elm.style['height']  = obj.height;

             elm.ondragstart = function() { return false; };
             elm.onmousedown = function(e){
               diff_x = (e.pageX - editor.offsetLeft) - elm.offsetLeft;
               diff_y = (e.pageY - editor.offsetTop) - elm.offsetTop;
               cleanOptions();
               draggable(elm);
               elm.style['border'] = '1px dashed #23bef7';
             };

             elm.onclick = function(){
               cleanOptions();
               elm.style['border'] = '1px dashed #23bef7';

               targetarrow = elm;
               targetWidth = elm.clientWidth;
               targetHeight = elm.clientHeight;

               arrow.style['display'] = 'block';
               arrow.style['left'] = (elm.offsetLeft + elm.clientWidth - 5) + 'px';
               arrow.style['top'] = (elm.offsetTop + elm.clientHeight - 5) + 'px';
             }

             $scope.layers.push({
               name: 'Imagem ',
               elm: elm,
               type: 'image'
             });

             editor.appendChild(elm);
          })();
           break;

          case 'textarea':
          (function(){
            var elm = document.createElement("textarea");
            elm.style['position'] = 'absolute';
            elm.style['left'] = obj.x + 'px';
            elm.style['top']  = obj.y + 'px';
            elm.style['width']  = obj.width;
            elm.style['height']  = obj.height;
            elm.style['font-size']  = obj.size;
            elm.style['color']  = obj.color;
            elm.style['font-family']  = obj.font;
            elm.style['text-align']  = obj.align;
            elm.value =  obj.text;


            elm.onclick = function(){
              cleanOptions();
              elm.style['border'] = '1px dashed #23bef7';

              targetarrow = elm;
              targetWidth = elm.clientWidth;
              targetHeight = elm.clientHeight;

              textEditor.style['display'] = 'block';
              $scope.textElement = elm;
              safeApply($scope,function(){});

              arrowRight.style['display'] = 'block';
              arrowRight.style['left'] = (elm.offsetLeft + elm.clientWidth - 5) + 'px';
              arrowRight.style['top'] = (elm.offsetTop + (elm.clientHeight / 2) - 7) + 'px';

              arrowTop.style['display'] = 'block';
              arrowTop.style['left'] = (elm.offsetLeft + (elm.clientWidth / 2) - 5) + 'px';
              arrowTop.style['top'] = (elm.offsetTop + elm.clientHeight - 5) + 'px';

              move.style['display'] = 'block';
              move.style['left'] = (elm.offsetLeft + elm.clientWidth - 5) + 'px';
              move.style['top'] = (elm.offsetTop + elm.clientHeight - 5) + 'px';
            }

            $scope.layers.push({
              name: 'Texto ',
              elm: elm,
              type: 'textarea'
            });

            editor.appendChild(elm);
          })();
          break;
       }
     }

     for (var i = 0; i < objs.length; i++) {
       createElement(objs[i]);
     }

     var dragObj = null;
     var diff_x = 0, diff_y = 0;

     function draggable(elm){
          dragObj = elm;
      }

      editor.onmouseup = function(e){
          if(dragObj){
            dragObj.style['border'] = 'none';
          }

          dragObj = null;
          arrowdrag = false;
          arrowX = false;
          arrowY = false;
          safeApply($scope,function(){});
      };

      editor.onmousemove = function(e){
          if(dragObj != null){
            var x = e.pageX - editor.offsetLeft - diff_x;
            var y = e.pageY - editor.offsetTop - diff_y;
            dragObj.style.left = x +"px";
            dragObj.style.top= y +"px";
          }

          if(arrowdrag){
            var diff = (e.pageY - editor.offsetTop) - (pointstart - editor.offsetTop);

            targetarrow.style['width'] = (targetWidth + diff) + 'px';
            targetarrow.style['height'] = (targetHeight + diff) + 'px';
            arrow.style.left = (targetarrow.offsetLeft + targetarrow.clientWidth - 5) + "px";
            arrow.style['top'] = (targetarrow.offsetTop + targetarrow.clientHeight - 5) + 'px';
          }

          if(arrowX){
            var diff = (e.pageX - editor.offsetLeft) - (pointstart - editor.offsetLeft);
            targetarrow.style['width'] = (targetWidth + diff) + 'px';

            arrowRight.style['left'] = (targetarrow.offsetLeft + targetarrow.clientWidth - 5) + 'px';
            arrowRight.style['top'] = (targetarrow.offsetTop + (targetarrow.clientHeight / 2) - 7) + 'px';

            arrowTop.style['display'] = 'block';
            arrowTop.style['left'] = (targetarrow.offsetLeft + (targetarrow.clientWidth / 2) - 5) + 'px';
            arrowTop.style['top'] = (targetarrow.offsetTop + targetarrow.clientHeight - 5) + 'px';

            move.style['display'] = 'block';
            move.style['left'] = (targetarrow.offsetLeft + targetarrow.clientWidth - 5) + 'px';
            move.style['top'] = (targetarrow.offsetTop + targetarrow.clientHeight - 5) + 'px';
          }

          if(arrowY){
            var diff = (e.pageY - editor.offsetTop) - (pointstart - editor.offsetTop);
            targetarrow.style['height'] = (targetHeight + diff) + 'px';

            arrowRight.style['left'] = (targetarrow.offsetLeft + targetarrow.clientWidth - 5) + 'px';
            arrowRight.style['top'] = (targetarrow.offsetTop + (targetarrow.clientHeight / 2) - 7) + 'px';

            arrowTop.style['left'] = (targetarrow.offsetLeft + (targetarrow.clientWidth / 2) - 5) + 'px';
            arrowTop.style['top'] = (targetarrow.offsetTop + targetarrow.clientHeight - 5) + 'px';

            move.style['display'] = 'block';
            move.style['left'] = (targetarrow.offsetLeft + targetarrow.clientWidth - 5) + 'px';
            move.style['top'] = (targetarrow.offsetTop + targetarrow.clientHeight - 5) + 'px';
          }
      };


      // escolher uma imagem.
      var fileInput =   document.getElementById('file');

      fileInput.onchange = function(evt){
          var file = evt.currentTarget.files[0];
          var reader = new FileReader();

          reader.onload = function (evt) {
            createElement({
              type: 'image',
              x: 150,
              y: 150,
              width: '100px',
              height: 'auto',
              src: evt.target.result
            });

            safeApply($scope,function(){});
          };

          reader.readAsDataURL(file);
      };

      $scope.pickImage = function(){
        fileInput.click();
      };

      $scope.addText = function(){
        createElement({
          type: 'textarea',
          x: 150,
          y: 150,
          text: 'Novo texto!',
          size: '18px',
          font: 'Arial',
          align: 'left',
          width: '120px',
          height: '50px',
          color: '#333'
        });
      };

      $scope.remove = function(i){
        editor.removeChild($scope.layers[i].elm);
        $scope.layers.splice(i, 1);
      };

      $scope.ganerate = function(){
        arrow.style['display'] = 'none';
        arrowRight.style['display'] = 'none';
        arrowTop.style['display'] = 'none';
        move.style['display'] = 'none';
        textEditor.style['display'] = 'none';

        for (var i = 0; i < $scope.layers.length; i++) {
          $scope.layers[i].elm.style.border = 'none';
        }

        html2canvas(editor, {
           onrendered: function(canvas) {
               Canvas2Image.saveAsPNG(canvas, 640, 300);
           }
       });
      };

    // end link function
    }
  }
});
