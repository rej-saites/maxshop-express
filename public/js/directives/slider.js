app.directive('maxSlider', function() {
  return {
    restrict: 'E',
    require: 'ngModel',
    templateUrl: './views/templates/directives/slider.html',
    link: function($scope, element, attrs, ngModel) {

      var selected = null, // Object of the element to be moved
      x_pos = 0, y_pos = 0; // Stores x & y coordinates of the mouse pointer

      // Will be called when user starts dragging an element
      function _drag_init(elem) {
        // Store the object of the element which needs to be moved
        selected = elem;
        x_elem = x_pos - selected.offsetLeft;
      }

      var slider = angular.element('.slider')[0];
      var fill = angular.element('.fill')[0];
      var value = angular.element('.value')[0];

      setTimeout(function () {
        var days = ngModel.$modelValue;
        // aplicando mudanças baseada no valor atual de days
        var px = (((slider.clientWidth-9) * days) / 100) + 'px';
        angular.element('.handler')[0].style.left = px;
        fill.style.width = px;
        value.style.left = ((((slider.clientWidth-9) * days) / 100) + 20) + 'px';
        if(days == 1) {
            value.innerHTML = '1 dia';
        } else {
          value.innerHTML = days + ' dias';
        }
      }, 10);


      // Will be called when user dragging an element
      function _move_elem(e) {
        x_pos = document.all ? window.event.clientX : e.pageX;
        if (selected !== null) {
            var x = (x_pos - x_elem)
            if((x_pos - x_elem) < 0) x = 0;
            if((x_pos - x_elem) > slider.clientWidth-9) x = slider.clientWidth - 9;
            selected.style.left = x + 'px';
            fill.style.width = x + 'px';
            // value
            value.style.left = (x + 20) + 'px';
            value.style.color = '#333';
            if(x + 60 > (slider.clientWidth-9)){
              value.style.left = (x - 60) + 'px';
              value.style.color = '#fff';
            }

            days = Math.floor((( x * 100 ) / (slider.clientWidth-9)));
            ngModel.$setViewValue(days);

            if(days == 1) {
                value.innerHTML = '1 dia';
            } else {
              value.innerHTML = days + ' dias';
            }

        }
      }

      // Destroy the object when we are done
      function _destroy() {
        selected = null;
      }

      // Bind the functions...
      angular.element('.handler')[0].onmousedown = function () {
        _drag_init(this);
        return false;
      };

      document.onmousemove = _move_elem;
      document.onmouseup = _destroy;
    }
  };
});
