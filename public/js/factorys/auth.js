app.factory('Auth', function($window, $http){
  var factory = {};

  function expireAuth(done){
    $http.get('/expire_auth').then(function(){
      done();
    },
    function(res){
      console.log(res.status);
      localStorage.clear();
      $window.location = "#/";
    })
  }

  factory.role = function(role){
    var user_role =  localStorage.getItem("role");

    expireAuth(function(){
      /* verificando se store_id está presente.
       caso de falha suponha-se que o usuário não inicio o
       cadastro dos dados da loja. */
       if(user_role != 'admin'){
         var store_id = localStorage.getItem('store_id');
        if(store_id == 'undefined' || !store_id) return $window.location = "#/register";
      }

      // Verificando permissões de usuários
      if(role.constructor === Array){
        var macth = false;
        for (var i = 0; i < role.length; i++) {
          if(role[i] == user_role){
            macth = true;
          }
        }

        if(macth == false)
          $window.location = "/401.html";
      } else {
        if(user_role != role)
          $window.location = "/401.html";
      }
    });

  };

  factory.isRole = function(role){
    var user_role =  localStorage.getItem("role");

    if(role.constructor === Array){
      var macth = false;
      for (var i = 0; i < role.length; i++) {
        if(role[i] == user_role){
          macth = true;
        }
      }

      return macth;
    } else {
      if(user_role != role){
        return false;
      } else{
        return true;
      }
    }
  };

  return factory;
});
