app.factory('Store', function($http, $q, PAYMENT_GATEWAY){
  var factory = {};

  factory.index = function(page){
    var q = $q.defer();
    if(!page) page = 1;

    $http.get('/api/stores/?page=' + (page - 1 ) ).then(function(result){
      var pages = result.headers().pages;
      var stores = result.headers().stores
      q.resolve({ data: result.data, pages: pages, stores: stores}); // sucesso
    }, function(){
      q.reject(); // caso de erro
    });

    return q.promise;
  };

  factory.homologar = function(id, data){
    var q = $q.defer();

    $http.put('/api/homologar/'+ id, data).then(function(){
      q.resolve(); // sucesso
    }, function(data){
      q.reject(data); // caso de erro
    });

    return q.promise;
  };

  factory.delete = function(id){
    var q = $q.defer();

    $http.delete('/api/stores/'+id).then(function(result){
      q.resolve(result.data.results); // sucesso
    }, function(){
      q.reject(); // caso de erro
    });

    return q.promise;
  };

  factory.get = function(id){
    var q = $q.defer();

    if(id === 'undefined' || !id) {  q.reject(); return q.promise; }

    $http.get('/api/stores/'+id).then(function(result){
      var data = result.data;
      if(data.address) {
        if(data.address.longitude) data.address.longitude = Number(data.address.longitude);
        if(data.address.latitude) data.address.latitude = Number(data.address.latitude);
      }
      q.resolve(data); // sucesso
    }, function(){
      q.reject(); // caso de erro
    });

    return q.promise;
  };

  factory.user = function(id){
    var q = $q.defer();
    if(id === 'undefined' || !id) {  q.reject(); return q.promise; }

    $http.get('/api/stores/'+id+"/user").then(function(result){
      q.resolve(result.data); // sucesso
    }, function(){
      q.reject(); // caso de erro
    });

    return q.promise;
  };

  factory.create = function(data){
    var q = $q.defer();

    $http.post('/api/stores', data).then(function(result){
      q.resolve(result.data); // sucesso
    }, function(result){
      q.reject('internet ou error 500'); // caso de erro
    });

    return q.promise;
  };

  factory.update = function(id, data){
    var q = $q.defer();

    $http.put('/api/stores/'+ id, data).then(function(result){
      q.resolve(result); // sucesso
    }, function(data){
      q.reject(data); // caso de erro
    });

    return q.promise;
  };


  /* Verifica se email está dísponível para cadastro */
  factory.checkEmail = function(email){
    var q = $q.defer();

    $http.post('/api/duplicate_email/stores', { email: email }).then(function(res){
      q.resolve(res.data.unique); // sucesso
    }, function(){
      q.reject(); // caso de erro
    });

    return q.promise;
  };

  factory.getBanks = function(){
    return [{"code":"001","name":"Banco do Brasil S.A.","website":"<a rel=\"nofollow\" href=\"http://www.bb.com.br\">www.bb.com.br</a>"}
    ,{"code":"033","name":"Banco Santander (Brasil) S.A.","website":"<a rel=\"nofollow\" href=\"http://www.santander.com.br\">www.santander.com.br</a>"},
    {"code":"204","name":"Banco Bradesco Cartões S.A.","website":"<a rel=\"nofollow\" href=\"http://www.iamex.com.br\">www.iamex.com.br</a>"},{"code":"394","name":"Banco Bradesco Financiamentos S.A.","website":"<a rel=\"nofollow\" href=\"http://www.bmc.com.br\">www.bmc.com.br</a>"},{"code":"237","name":"Banco Bradesco S.A.","website":"<a rel=\"nofollow\" href=\"http://www.bradesco.com.br\">www.bradesco.com.br</a>"},
    {"code":"356","name":"Banco Real S.A.","website":"<a rel=\"nofollow\" href=\"http://www.bancoreal.com.br\">www.bancoreal.com.br</a>"},
    {"code":"104","name":"Caixa Econômica Federal","website":"<a rel=\"nofollow\" href=\"http://www.caixa.gov.br\">www.caixa.gov.br</a>"},
    {"code":"652","name":"Itaú Unibanco Holding S.A.","website":"<a rel=\"nofollow\" href=\"http://www.itau.com.br\">www.itau.com.br</a>"},
    {"code":"341","name":"Itaú Unibanco S.A.","website":"<a rel=\"nofollow\" href=\"http://www.itau.com.br\">www.itau.com.br</a>"}];
  };

  factory.search = function(query){
    var q = $q.defer();

    $http.get('/api/search/stores?query='+query).then(function(res){
      q.resolve(res.data); // sucesso
    }, function(){
      q.reject(); // caso de erro
    });

    return q.promise;
  };

  factory.homologarStores = function(page){
    var q = $q.defer();
    if(!page) page = 1;

    $http.get('/api/homologar/stores?page=' + (page - 1 )).then(function(result){
      var pages = result.headers().pages;
      q.resolve({ data: result.data, pages: pages}); // sucesso
    }, function(){
      q.reject(); // caso de erro
    });

    return q.promise;
  };

  return factory;
});
