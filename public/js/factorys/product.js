app.factory('Product', function($http, $q){
  var factory = {};

  factory.index = function(page, store_id){
    var q = $q.defer();
    if(!page) page = 1;

    $http.get('/api/stores/'+store_id+'/products?page='+ (page - 1 )).then(function(res){
      var pages = res.headers().pages;
      q.resolve({ data: res.data, pages: pages }); // sucesso
    }, function(){
      q.reject(); // caso de erro
    });

    return q.promise;
  };

  factory.get = function(id){
    var q = $q.defer();

    $http.get('/api/products/'+ id).then(function(result){
      q.resolve(result.data); // sucesso
    }, function(){
      q.reject(); // caso de erro
    });

    return q.promise;
  };

  function formaterPrice(value){
    value = String(value);
    value = value.replace(/\.|\,/g,'');

    if(value.length >= 3 ){
      var start = value.substring(0, value.length - 2);
      var end = value.substring(value.length - 2, value.length);
      return Number(start + '.' + end).toFixed(2);
    } else{
      return Number(value).toFixed(2);
    }
  };

  factory.create = function(data) {
    var q = $q.defer();

    data.price =  formaterPrice(data.price);

    if(data.old_price) data.old_price =  formaterPrice(data.old_price);
    if(isNaN(data.old_price)) data.old_price = undefined ;

    $http.post('/api/products', data).then(function(result){
      q.resolve(result); // sucesso
    }, function(data){
      q.reject(data); // caso de erro
    });

    return q.promise;
  };

  factory.update = function(id, data) {
    var q = $q.defer();

    data.product.price =  formaterPrice(data.product.price);
    if(data.product.old_price) data.product.old_price =  formaterPrice(data.product.old_price);
    if(isNaN(data.product.old_price)) data.product.old_price = undefined ;

    console.log('price: ' + data.product.price);
    console.log('old_price: ' + data.product.old_price);

    $http.put('/api/products/' + id, data).then(function(result){
      q.resolve(result); // sucesso
    }, function(data){
      q.reject(data); // caso de erro
    });

    return q.promise;
  };

  factory.delete = function(id, data) {
    var q = $q.defer();

    $http.delete('/api/products/' + id).then(function(result){
      q.resolve(result); // sucesso
    }, function(){
      q.reject(); // caso de erro
    });

    return q.promise;
  };


  factory.byCategory = function(category, page){
    var q = $q.defer();

    $http.get('/api/category/products?category='+category+'&page='+page).then(function(res){
      q.resolve(res.data); // sucesso
    }, function(){
      q.reject(); // caso de erro
    });

    return q.promise;
  };

  factory.categoryProductsByStore = function(store_id, category, page){
    var q = $q.defer();
    if(!page) page = 1;

    $http.get('/api/category/'+ store_id +'/products?category='+category+'&page='+ (page -1 ) ).then(function(res){
      var pages = res.headers().pages;
      q.resolve({ data: res.data, pages: pages }); // sucesso
    }, function(){
      q.reject(); // caso de erro
    });

    return q.promise;
  };


  factory.search = function(query){
    var q = $q.defer();

    $http.get('/api/search/products?query=' + query).then(function(result){
      q.resolve(result.data); // sucesso
    }, function(){
      q.reject(); // caso de erro
    });

    return q.promise;
  };

  factory.searchInStore = function(query, store_id){
    var q = $q.defer();

    $http.get('/api/search/' + store_id + '/products?query=' + query)
    .then(function(result){
      q.resolve(result.data); // sucesso
    }, function(){
      q.reject(); // caso de erro
    });

    return q.promise;
  };

  factory.upload = function(image) {
    var q = $q.defer();

    $http.post('/api/upload', { image: image }).then(function(res){
      q.resolve(res.data); // sucesso
    }, function(){
      q.reject(); // caso de erro
    });

    return q.promise;
  };



  return factory;
});
