app.factory('User', function($http, $q){
  var factory = {};

  factory.get = function(id) {
    var q = $q.defer();

    $http.get('/api/users/'+id).then(function(result){
      q.resolve(result); // sucesso
    }, function(){
      q.reject(); // caso de erro
    });

    return q.promise;
  };

  factory.auth = function(data){
    var q = $q.defer();

    $http.post('/authenticate', data).then(function(result){
      q.resolve(result); // sucesso
    }, function(){
      q.reject(); // caso de erro
    });

    return q.promise;
  };

  factory.update = function(id, data){
    var q = $q.defer();
    console.log(data);

    $http.put('/api/users/'+ id, data).then(function(result){
      q.resolve(result); // sucesso
    }, function(){
      q.reject(); // caso de erro
    });

    return q.promise;
  };

  /* Verifica se email está dísponível para cadastro */
  factory.checkEmail = function(email){
    var q = $q.defer();

    $http.post('/api/duplicate_email/', { email: email }).then(function(res){
      q.resolve(res.data.unique); // sucesso
    }, function(){
      q.reject(); // caso de erro
    });

    return q.promise;
  };

  /* Atualiza a senha do usuário */
  factory.changePassword = function(data){
    var q = $q.defer();

    if(!data.id || !data.password || !data.new_password)
      q.reject();

    $http.put('/api/change-password', data).then(function(result){
      q.resolve(result); // sucesso
    }, function(){
      q.reject(); // caso de erro
    });

    return q.promise;
  };


  /* Atualiza a senha do usuário */
  factory.getByStore = function(id){
    var q = $q.defer();

    // rejeite caso o id seja inválido.
    if(id == null || id.length < 8) q.reject();

    $http.get('/api/stores/'+ id +'/user').then(function(result){
      q.resolve(result.data); // sucesso
    }, function(){
      q.reject(); // caso de erro
    });

    return q.promise;
  };


  return factory;
});
