app.factory('Category', function($http, $q){
  var factory = {};

  factory.index = function(){
    var q = $q.defer();

    $http.get('/api/categories').then(function(result){
      q.resolve(result.data); // sucesso
    }, function(){
      q.reject(); // caso de erro
    });

    return q.promise;
  };

  factory.delete = function(id){
    var q = $q.defer();

    $http.delete('/api/categories/'+id).then(function(result){
      q.resolve(result.data.results); // sucesso
    }, function(){
      q.reject(); // caso de erro
    });

    return q.promise;
  };


  factory.create = function(data){
    var q = $q.defer();

    $http.post('/api/categories', data).then(function(res){
      q.resolve(res); // sucesso
    }, function(res){
      q.reject(res); // caso de erro
    });

    return q.promise;
  };

  factory.update = function(id, data){
    var q = $q.defer();

    $http.put('/api/categories/'+ id, data).then(function(result){
      q.resolve(result); // sucesso
    }, function(data){
      q.reject(data); // caso de erro
    });

    return q.promise;
  };

  return factory;
});
