app.factory('Banner', function($http, $q){
  var factory = {};

  factory.create = function(data){
    var q = $q.defer();

    $http.post('/api/banners', data).then(function(result){
      q.resolve(result); // sucesso
    }, function(){
      q.reject(); // caso de erro
    });

    return q.promise;
  };

  factory.index = function(){
    var q = $q.defer();

    $http.get('/api/banners/').then(function(res){
      q.resolve(res.data); // sucesso
    }, function(){
      q.reject(); // caso de erro
    });

    return q.promise;
  };

  factory.get = function(id){
    var q = $q.defer();

    $http.get('/api/banner/' + id).then(function(res){
      q.resolve(res.data); // sucesso
    }, function(){
      q.reject(); // caso de erro
    });

    return q.promise;
  };

  factory.delete = function(id) {
    var q = $q.defer();

    $http.delete('/api/banners/' + id).then(function(result){
      q.resolve(result); // sucesso
    }, function(){
      q.reject(); // caso de erro
    });

    return q.promise;
  };

  return factory;
});
