app.factory('Requests', function($http, $q){
  var factory = {};

  factory.index = function(){
    var q = $q.defer();

    $http.get('/api/requests').then(function(result){
      var data = result.data.results;
      q.resolve(data); // sucesso
    }, function(){
      q.reject(); // caso de erro
    });

    return q.promise;
  };

  factory.getItens = function(id){
    var q = $q.defer();
    $http.get('/api/requests/itens/'+id).then(function(result){
      var data = result.data.results;
      q.resolve(data); // sucesso
    }, function(){
      q.reject(); // caso de erro
    });
    return q.promise;
  };

  factory.getItensStore = function(id, store_id){
    var q = $q.defer();
    $http.get('/api/store_itens/resquest?id='+id+'&store_id='+store_id).then(function(result){
      var data = result.data.results;
      q.resolve(data); // sucesso
    }, function(){
      q.reject(); // caso de erro
    });
    return q.promise;
  };

  factory.finalizar = function(id) {
    var q = $q.defer();
    var data = {
      current_status: 'completed'
    };
    
    $http.post('/api/requests/'+id+'/update', data).then(function(result){
      var data = result.data;
      q.resolve(data); // sucesso
    }, function(){
      q.reject(); // caso de erro
    });
    
    return q.promise;
  };

  factory.estornar = function(req) {
    var q = $q.defer();
    var data = {
      store_id: req.store_id,
      transaction_id: req.transaction_id
    };
    $http.post('/api/requests/'+req._id+'/refund', data).then(function(result){
      var data = result.data;
      q.resolve(data); // sucesso
    }, function(){
      q.reject(); // caso de erro
    });
    
    return q.promise;
  };

  factory.store = function(id){
    var q = $q.defer();

    $http.get('/api/stores/'+id+'/requests').then(function(result){
      var data = result.data;
      q.resolve(data); // sucesso
    }, function(){
      q.reject(); // caso de erro
    });

    return q.promise;
  };

  return factory;
});
