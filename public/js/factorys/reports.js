app.factory('Reports', function($http, $q){
  var factory = {};

  factory.allProducts = function(){
    var q = $q.defer();

    $http.get('/api/reports/all_products').then(function(result){
      q.resolve(result.data.total); // sucesso
    }, function(){
      q.reject(); // caso de erro
    });

    return q.promise;
  };

  factory.allViews = function(){
    var q = $q.defer();

    $http.get('/api/reports/all_views').then(function(result){
      q.resolve(result.data.total); // sucesso
    }, function(){
      q.reject(); // caso de erro
    });

    return q.promise;
  };

  factory.allLovers = function(){
    var q = $q.defer();

    $http.get('/api/reports/all_lovers').then(function(result){
      q.resolve(result.data.total); // sucesso
    }, function(){
      q.reject(); // caso de erro
    });

    return q.promise;
  };

  factory.allStores = function(){
    var q = $q.defer();

    $http.get('/api/reports/all_stores').then(function(result){
      q.resolve(result.data.total); // sucesso
    }, function(){
      q.reject(); // caso de erro
    });

    return q.promise;
  };

  factory.allClients = function(){
    var q = $q.defer();

    $http.get('/api/reports/all_clients').then(function(result){
      q.resolve(result.data.total); // sucesso
    }, function(){
      q.reject(); // caso de erro
    });

    return q.promise;
  };

  factory.monthRequests = function(){
    var q = $q.defer();

    $http.get('/api/reports/monthRequests').then(function(result){
      q.resolve(result.data); // sucesso
    }, function(){
      q.reject(); // caso de erro
    });

    return q.promise;
  };

  factory.dailyRequests = function(){
    var q = $q.defer();

    $http.get('/api/reports/dailyRequests').then(function(result){
      q.resolve(result.data); // sucesso
    }, function(){
      q.reject(); // caso de erro
    });

    return q.promise;
  };

  // Para as Stores
  factory.allProductsStore = function(id){
    var q = $q.defer();

    $http.get('/api/reports/all_products/'+id).then(function(result){
      q.resolve(result.data.total); // sucesso
    }, function(){
      q.reject(); // caso de erro
    });

    return q.promise;
  };

  factory.allViewsStore = function(id){
    var q = $q.defer();

    $http.get('/api/reports/all_views/' + id).then(function(result){
      q.resolve(result.data.total); // sucesso
    }, function(){
      q.reject(); // caso de erro
    });

    return q.promise;
  };

  factory.allLoversStore = function(id){
    var q = $q.defer();

    $http.get('/api/reports/all_lovers/' + id).then(function(result){
      q.resolve(result.data.total); // sucesso
    }, function(){
      q.reject(); // caso de erro
    });

    return q.promise;
  };

  factory.monthRequestsStore = function(id){
    var q = $q.defer();

    $http.get('/api/reports/monthRequests/'+id).then(function(result){
      q.resolve(result.data); // sucesso
    }, function(){
      q.reject(); // caso de erro
    });

    return q.promise;
  };

  factory.dailyRequestsStore = function(id){
    var q = $q.defer();

    $http.get('/api/reports/dailyRequests/'+id).then(function(result){
      q.resolve(result.data); // sucesso
    }, function(){
      q.reject(); // caso de erro
    });

    return q.promise;
  };

  return factory;
});
