var mongoose = require('mongoose');

var Schema = mongoose.Schema;

var bannerSchema = new Schema({
  store_id: String,
  /* Período que o banner irá aparecer para os usuários. */
  period: {
    start: Date,
    end: Date,
    /* Caso seja indeterminado irá ignorar o inicio e o fim. */
    indeterminate: { type: Boolean, required: true  }
  },
  avatar: {
    url: { type: String, required: true  }
  },
  /* Ação ao clicar no banner. */
  action: {
     /* Tipo da ação do link 'default' , 'blank' etc.. */
     target: String,
     url: String
  },
  /* Posição da loja, para facilitar cálculos com distância. */
  position: {
    latitude: Number,
    longitude: Number
  },
  created_at: Date
});

var Banner  = mongoose.model('Banner', bannerSchema);

// Export model
module.exports = Banner;
