var mongoose = require('mongoose');
var uniqueValidator = require('mongoose-unique-validator');

var Schema = mongoose.Schema;

var storeSchema = new Schema({
  name:  { type: String, required: true  },
  /* Razão social */
  corporate_name: String,
  /* CNPJ */
  cnpj: String,
  phones: [String],
  site: String,
  email: String,
  /* Chave de acesso à api do pagarme */
  pagarmeApiKey: String,
  /* plano da loja*/
  plan: String,
  /* Data de expiração do Plano */
  plan_expiration: Date,
  /* Se é associado CDL. */
  cdl: { type: Boolean, default: false },
  /* Se está visível ou não no sistema. */
  visible: { type: Boolean, default: false },
  /* modo vitrine */
  vitrine: { type: Boolean, default: false },
  /* Esconder preços dos produtos. */
  hide_price: { type: Boolean, default: false },
  /* Se foi aprovado ou não */
  homologado: { type: Boolean, default: false  },
  /* Participante do liguida Natal */
  liquida: { type: Boolean, default: false  },
  address: {
    cep: String,
    state: String,
    city: String,
    district: String,
    street: String,
    complement: String,
    number: String,
    latitude: String,
    longitude: String
  },
  /* Sócio administrador */
  partner_admin : {
    name: String,
    cpf: String,
    phone: String
  },
  /* Contato responsável */
  contact_responsible: {
    name: String,
    cpf: String,
    email: String
  },
  /* Conta bancária */
  bank_account: {
    legal_name: String,
    bank_code: String,
    agencia: Number,
    agencia_dv: Number,
    conta: Number,
    conta_dv: Number
  },
  avatar: {
    url: String,
    thumb: String
  },
  /* Quantidade máxima de parcelas */
  maxInstallments: Number,
  description: String,
  created_at: Date
});


storeSchema.index({ name: 'text' });

var Store  = mongoose.model('Store', storeSchema);

// Export model
module.exports = Store;
