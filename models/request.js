var mongoose = require('mongoose');
var autoIncrement = require('mongoose-auto-increment');
var Schema = mongoose.Schema;

var requestSchema = new Schema({
  service:  { type: String, required: true  },
  transaction_id: String,
  store_id: { type: String, required: true  },
  status: String,
  checkout: String,
  user_id: { type: String, required: true  },
  total: Number,
  products: [{
    name: String,
    price: Number,
    quantity: Number,
    description: String,
    images: [{
      _id: String,
      url: String,
      thumb: String,
    }],
    colors: [{
      name: String,
      code: String
    }],
    sizes: [{
      name: String
    }],
    store_id: { type: String, required: true  }
  }],
  deliveryAddress: {
    logradouro: String,
    numero: String,
    complemento: String,
    bairro: String,
    cidade: String,
    uf: String,
    cep: String
  },
  pickUpInStore: Boolean,
  created_at: Date
});

// transactions: [{
//   id: String,
//   status: String,
//   products: [
//     {
//       product_id: String,
//       name: String,
//       price: Number,
//       quantity: Number,
//       description: String,
//       store_id: { type: String, required: true  }
//     }
//   ]
// }],
/*
requestSchema.methods.isPaid = function(){
  return this.transactions.every(function(e){
    return e.status == 'paid'
  });
};

requestSchema.methods.idPending = function(){
  return this.transactions.some(function(e){
    return e.status != 'paid'
  });
};
*/

autoIncrement.initialize(mongoose.connection);

requestSchema.plugin(autoIncrement.plugin, {
    model: 'Request',
    field: 'code',
    startAt: 1000
});

var Request  = mongoose.model('Request', requestSchema);

// Export model
module.exports = Request;
