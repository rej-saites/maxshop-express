var mongoose = require('mongoose');

var Schema = mongoose.Schema;

var categorySchema = new Schema({
  name: { type: String, required: true  },
  created_at: Date
});

var Category  = mongoose.model('Categories', categorySchema);

// Export model
module.exports = Category;
