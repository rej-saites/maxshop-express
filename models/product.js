var mongoose = require('mongoose');

var Schema = mongoose.Schema;

var productSchema = new Schema({
  name:  { type: String, required: true  },
  visible: Boolean,
  /* Esconder preços dos produtos. */
  hide_price: { type: Boolean, default: false },
  /* Modo vitrine */
  vitrine: { type: Boolean, default: false },
  categories: [String],
  /* Preço atual do produto */
  price: { type: Number, required: true  },
  /* Preço antigo do produto */
  old_price: Number,
  images: [{
    url: String,
    thumb: String
  }],
  videos: [String],
  description: String,
  quantity: Number,
  /* Quantidade de visualizações de usuários */
  views: Number,
  /* Quantidade de usuários que gostou do produto */
  lovers: Number,
  colors: [{
    name: String,
    code: String
  }],
  sizes: [{
    name: String
  }],
  /* Nota do produto a partir da avaliação dos usuários. */
  classification: {
    total: Number,
    users: [{
      user_id: String,
      note: Number
    }]
  },
  /* Loja a qual o produto está associado */
  store_id: String,
  created_at: Date
});

productSchema.index({ name: 'text', description: 'text' });

var Product  = mongoose.model('Product', productSchema);


// Export model
module.exports = Product;
