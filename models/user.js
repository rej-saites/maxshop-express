var mongoose = require('mongoose');
var uniqueValidator = require('mongoose-unique-validator');

var Schema = mongoose.Schema;

var userSchema = new Schema({
  name:  { type: String, required: true  },
  email: { type: String, required: true , unique : true },
  password: { type: String, required: true  },
  /* associado CDL?*/
  cdl: { type: Boolean, default: false },
  avatar: {
    url: String,
    thumb: String
  },
  categories: [String],
  /* Token para push notification */
  token_push: String,
  /* Função dentro do sistema. Ex: Admin, Dono de loja ... */
  role: { type: String, required: true  },
  /* Lista de produtos que o usuário gostou. */
  beloved_products: [String],
  /* Cartões de crédito */
  cards: [{
    _id: String,
    card_number: { type: String, required: true },
    card_holder_name: { type: String, required: true },
    card_expiration_month: { type: String, minlength: 2, maxlength: 2, required: true },
    card_expiration_year: { type: String, minlength: 2, maxlength: 2, required: true },
    card_cvv: { type: String, minlength: 3, maxlength: 3, required: true },
    card_brand: { type: String, required: true },
    favorite: Boolean,
    created_at: Date
  }],
  /* Quando está associado a uma loja. */
  store_id: String,
  /* Id do login social utilizado pelo usuário */
  social_id: String,
  created_at: Date,

  addresses: [{
    logradouro: { type: String, required: true },
    numero: { type: String, required: true },
    bairro: { type: String, required: true },
    complemento: String,
    cep: { type: String, minlength: 8, maxlength: 8 },
    cidade: { type: String, required: true },
    uf: { type: String, minlength: 2, maxlength: 2, required: true }
  }]
});

userSchema.plugin(uniqueValidator);

var User  = mongoose.model('User', userSchema);

// Export model
module.exports = User;
