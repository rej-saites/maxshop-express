var pg = require('pg');

var runsql = {};

runsql.connectionString = process.env.OPENSHIFT_POSTGRESQL_DB_URL || 'postgres://localhost:5432/maxshop';


runsql.query = function(sql, callback){

  // Get all instances in db
  pg.connect(this.connectionString, function(err, client, done) {

    var query = client.query(sql, function(err, result) {
      if(err) {
          console.error('error running query', err);
          callback(err);
        }
    });

    var results = [];

    query.on('row', function(row) {
      for(var prop in row)
        row[prop] = unescape(row[prop]);

      results.push(row);
    });

    query.on('end', function() {
      client.end();
      callback(null, results);
    });

    if(err) {
      console.log(err);
      callback(500,err);
    }

  });
};

module.exports = runsql;
