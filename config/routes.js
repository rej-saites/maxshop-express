
// Routes manager ==================================
module.exports = function(app) {

  var multer = require('multer');
  var upload = multer({ dest: './public/uploads/' });

  app.all('/*', function(req, res, next) {
	  res.header("Access-Control-Allow-Origin", "*");
    res.header('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE');
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
	  next();
	});

  var match = require('./match')(app);

  /* Auth
  ============================================================================*/
  match('post','/authenticate', 'user', 'authenticate');
  match('post','/authenticate-client', 'user', 'authenticateClient');
  match('get','/expire_auth', 'user', 'expireAuth')


  /* Usuário
  ============================================================================*/
  // criar um usuário cliente
  match('post','/register', 'user', 'create');
  // criar um usuário lojista.
  match('post','/register/owner', 'user', 'createOwner');

  // Retorna um usuário baseado no id social.
  match('get','/showBySocial/user/:id', 'user', 'showBySocial');
  // CRUD
  match('get', '/api/users', 'user', 'index');
  match('get', '/api/users/:id', 'user', 'show');
  match('put', '/api/users/:id', 'user', 'update');
  match('delete', '/api/users/:id', 'user', 'delete');
  // Rota para enviar um email quando o usuário esquecer a senha
  match('post', '/api/forget_password/', 'user', 'forget');
  // verificar se existe algum user com esse email
  match('post', '/api/duplicate_email/', 'user', 'duplicateEmail');
  // Alterar a senha do usuário
  match('put', '/api/change-password', 'user', 'changePassword');

  // faer doc
  match('post', '/api/update_user_client', 'user', 'updateUserClient');

  /* Endereço */
  match('get', '/api/users/:id/addresses', 'user', 'addresses');
  match('post', '/api/users/:id/addresses', 'user', 'addAddress');

  /* Interesse em produtos
  ================================*/
  // Checando se usuário tem Interesse no produto.
  match('get', '/api/lover/:user/:product_id', 'user', 'checkLover');
  match('post', '/api/lover/:user/:product_id', 'user', 'addLover');
  match('delete', '/api/lover/:user/:product_id', 'user', 'deleteLover');


  /* Cartões de crédito
  ================================*/
  // retorna todos os cartões
  match('get', '/api/cards/:user', 'user', 'cards');
  // retorna o cartão favorito
  match('get', '/api/card/:user', 'user', 'card');
  // atualiza o cartão favorito
  match('put', '/api/card/:user', 'user', 'updateCard');
  // cria um novo cartão
  match('post', '/api/cards/:user', 'user', 'createCard');
  // deleta um cartão
  match('delete', '/api/cards/:user', 'user', 'deleteCard');

  /* Retorna o usuário relacionado a uma loja especifica
  ================================*/
  match('get', '/api/stores/:id/user', 'user', 'storeUser');


  /* Loja
  ============================================================================*/
  // CRUD
  match('get', '/api/stores', 'store', 'index');
  match('get', '/api/stores/:id', 'store', 'show');
  match('post', '/api/stores', 'store', 'create');
  match('put', '/api/stores/:id', 'store', 'update');
  match('delete', '/api/stores/:id', 'store', 'delete');

  // doc pedente
  match('put', '/api/homologar/:id', 'store', 'homologar');

  // verifica emails duplicados
  // Retorna os pedidos de uma loja
  match('get', '/api/stores/:id/requests', 'store', 'requests');
  
  match('post', '/api/duplicate_email/stores', 'store', 'duplicateEmail');

  // pesquisa em lojas
  match('get', '/api/search/stores', 'store', 'search');

  // Retorna todas as lojas que precisam ser homologadas.
  match('get', '/api/homologar/stores', 'store', 'homologarStores');
  /* Produto
  ============================================================================*/
  // CRUD
  match('get', '/api/products', 'product', 'index');
  match('get', '/api/products/:id', 'product', 'show');
  match('post', '/api/products', 'product', 'create');
  match('put', '/api/products/:id', 'product', 'update');
  match('delete', '/api/products/:id', 'product', 'delete');
  // Aumenta a visualização de um produto
  match('post', '/api/products/view/:id', 'product', 'view');
  // Retorna produtos baseado na categoria
  match('get', '/api/category/products', 'product', 'categoryProducts');
  // Retorna produtos baseado na categoria e loja.
  match('get', '/api/category/:id/products', 'product', 'categoryProductsByStore');


  // pesquisa em produtos
  match('get', '/api/search/products', 'product', 'search');
  // pesquisar produtos em loja
  match('get', '/api/search/:id/products', 'product', 'searchInStore');

  // Retorna os produtos de uma loja
  match('get', '/api/stores/:id/products', 'product', 'storeProducts');

  
  // upload de imagens
  match('post', '/api/upload', 'product', 'upload');
  // retorna produtos baseados em uma array de ids
  match('post', '/api/group/products', 'product', 'groupProducts');

  /* Carrinho de compras
  ============================================================================
  // CRUD
  match('get', '/api/cart/:id', 'cart', 'index');
  match('post', '/api/cart', 'cart', 'create');
  match('put', '/api/cart/:id', 'cart', 'update');
  match('delete', '/api/cart/:id', 'cart', 'delete');*/




  /* Pedidos
  ============================================================================*/

  // Retorna todos os pedidos
  match('get', '/api/requests', 'request', 'index');
  // Retorna um pedido por id
  match('get', '/api/requests/:id', 'request', 'show');
  // Cria um novo pedido
  match('post', '/api/requests', 'request', 'create');
  // Recebe o postback do pagarme e atualiza a Request
  match('post', '/api/requests/:id/update', 'request', 'updateStatus');
  // Estorna uma transação
  match('post', '/api/requests/:id/refund', 'request', 'refund');
  // Atualiza um pedido
  match('put', '/api/requests/:id', 'request', 'update');
  // Deleta um pedido
  match('delete', '/api/requests/:id', 'request', 'delete');

  // Retorna todos os pedidos relacionados a um usuário.
  match('get', '/api/requests/user/:id', 'request', 'requestsByUser');

  // retorna pedidos de uma loja.
  match('get', '/api/requests/store/:id', 'request', 'requestsByStore');


  /* RELATORIOS
  -------------------------*/


  /* Banners
  ============================================================================*/
  // Cria um novo banner
  match('post', '/api/banners', 'banner', 'create');
  // retorna um banner
  match('get', '/api/banner/:id', 'banner', 'show');
  // retorna todos os banners de um loja
  match('get', '/api/banners/:id', 'banner', 'bannersOfStore');
  // Atualiza banner
  match('put', '/api/banners/:id', 'banner', 'update');
  // Retorna todos os Banners
  match('get', '/api/banners', 'banner', 'index');
  // Deleta um banner
  match('delete', '/api/banners/:id', 'banner', 'delete');


  /* Categories
  ================================*/
  // Checando se usuário tem Interesse no produto.
  match('get', '/api/categories', 'category', 'index');
  match('post', '/api/categories', 'category', 'create');
  match('put', '/api/categories/:id', 'category', 'update');
  match('delete', '/api/categories/:id', 'category', 'delete');

};
