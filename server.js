  // set up ========================
  var express        = require('express');
  var morgan         = require('morgan');
  var bodyParser     = require('body-parser');
  var methodOverride = require('method-override');
  var multer         = require('multer');
  var mongoose       = require('mongoose');
  var expressJwt     = require('express-jwt');

  var app = express();

  // Configurações
  //============================================================================
  app.use(express.static(__dirname + '/public'));                 // set the static files location /public/img will be /img for users
  app.use(morgan('dev'));                                         // log every request to the console
  app.use(bodyParser.urlencoded({ limit: '10mb', 'extended':'true'}));            // parse application/x-www-form-urlencoded
  app.use(bodyParser.json({limit: '10mb'}));                                     // parse application/json
  app.use(bodyParser.json({ type: 'application/vnd.api+json' })); // parse application/vnd.api+json as json
  app.use(methodOverride());

  /* app.use('/api', expressJwt({secret: secret})); */
  app.use('/expire_auth', expressJwt({secret: 's558tuQ245a-33H-9t'}));

  const AuthToken = "wRjXj87KcyrgwRIy0OKMidFUm1YLF6mOYLZr6Fxy9lGtcCTcERzz0O4mLMjC0OEHKrq23yuzLxpeOtlW5VH6";
  const AppID = "500BC-E1339";

  // Configurando o banco de dados
  //============================================================================
  var db = mongoose.connection;
  db.on('error', console.error);
  db.once('open', function() {
    console.log('Conectado ao MongoDB com sucesso..')
  });

  // Carregando arquivo de configuração.
  var db_json = require("./database.json");
  var DATABASE_URL =  db_json.DATABASE_URL ;
  // Conectando ao banco de dados.
  mongoose.connect(DATABASE_URL);

  // Carregando as rotas do sistema
  //============================================================================
  require('./config/routes')(app);

  // Iniciando 'listen' do app. (start app with node server.js)
  //============================================================================
  var server_port =  3000;
  var server_ip_address = '0.0.0.0';

  app.listen(server_port, server_ip_address, function(){
    console.log("Listening on " + server_ip_address + ", server_port " + server_port);
  });





/* pushwoosh
var Pushwoosh = require('pushwoosh-client'),
    client= new Pushwoosh(AppID, AuthToken),
    options = {
        data: {
            username: 'bob smith',
            email: 'bob@example.com'
        }
    };

    // send to device unic - client.sendMessage('Hello world', 'device token', options, function(error, response) {
    client.sendMessage('Hello world', options, function(error, response) {
     if (error) {
        console.log('Some error occurs: ', error);
     }

     console.log('Pushwoosh API response is', response);
});
*/
