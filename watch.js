const concat = require('concat-files');
const watch = require('watch');
var exec = require('child_process').exec;

function onlyUnique(value, index, self) {
  return self.indexOf(value) === index;
}

// Concatenando os arquivos js
(function(){
  var files = [];

  var filter = function(file){
    // Removendo arquivos de lib e dist
    if(/lib|dist/.test(file)) {
      return false;
    }

    // adicionando todos os arquivos .js
    if(/.js$/.test(file)){
     var filepath = file.substring(file.lastIndexOf('public/js'), file.length);
     files.push(filepath);
    }

    return true;
  };

  watch.watchTree(__dirname + '/public/js',{ filter: filter , ignoreDirectoryPattern: /dist/},
  function (f, curr, prev) {
    concat(files.filter(onlyUnique), 'public/js/dist/app.js',function() {
      console.log('js concat sucess..');
    });
  });
})();


(function(){
  function puts(error, stdout, stderr) {
    console.log('doc md to html sucess...')
  }

  watch.watchTree(__dirname + '/doc/md',{}, function (f, curr, prev) {
    exec("generate-md --layout github --input ./doc/md --output ./doc/output", puts);
  });
})();
