## Schemas dos modelos do MongoDB

Documento para orientar a estrutura que os modelos no sistema devem seguir para o auxiliar todos os envolvido no projeto. Muito importante manter este doc atualizado.


### Produto
Abaixo segue a estrutura padrão de Produtos. Depedendo das categorias que o produto possua ele terá no decorrer de sua vida outros atributos referentes aquelas categorias.

**Product**
```js
{
  name: "String",
  visible: "Boolean",
  categories: [],
  /* Preço atual do produto */
  price: "Float",
  /* Preço antigo do produto */
  old_price: "Float",
  images: [{  
    url: "String",
    thumb: "String"
  }],
  videos: [],
  description: "String",
  quantity: "int",
  /* Quantidade de visualizações de usuários. */
  views: "Int",
  /* Quantidade de usuários que gostou do produto */
  lovers: "Int",
  colors: [{
    name: "String",
    code: "String"
  }],
  sizes: [{
    name: String
  }],
  /* Nota do produto a partir da avialiação dos usuários. */
  classification: {
    total: "Int",
    users: [{
      user_id: "String",
      note: "Int"
    }]
  },
  /* Loja a qual o produto está associado */
  store_id: "String",
  created_at: "Date"
}
```

Para cada categoria é necessário modelar a estrutura necessária para o fucionamento correto no sistema.


### Usuário

Estrutura padrão para usuários.

**User**

```js
{
  name: "String",
  email: "String",
  password: "String",
  avatar: [
    url: "String",
    thumb: "String"
  ],
  /* associado CDL?*/
  cdl: "Boolean",
  categories: ["String"],
  /* Token para push notification */
  token_push: "String",
  /* Função dentro do sistema. Ex: Admin, Dono de loja ... */
  role: "String",
  /* Lista de produtos que o usuário gostou. */
  beloved_products: ["String"],
  /* Cartões de crédito */
  cards: [{
    _id: "String",
    card_number: "String",
    card_holder_name: "String",
    card_expiration_month: "String",
    card_expiration_year: "String",
    card_cvv: "String",
    card_brand: "String",
    favorite: "Boolean",
    created_at: "Date"
  }],
  /* Quando está associado a uma loja. */
  store_id: "String",
  /* Id do login social utilizado pelo usuário */
  social_id: "String",
  created_at: "Date"
}
```

### Loja

Estrutura padrão para lojas.

**Store**

```js
{
  /* name é nome fantasia */
  name: "String",
  /* Razão social */
  corporate_name: "String",
  /* CNPJ */
  cnpj: "String",
  phones: ["String"],
  site: "String",
  email: "String",
  /* Chave de acesso à api do pagarme */
  pagarmeApiKey: "String",
  /* Plano da loja */
  plan: "String",
  /* Data de expiração do Plano */
  plan_expiration: "Date",
  /* Se está visível ou não no sistema. */
  visible: "Boolean",
  /* Se foi aprovado ou não */
  homologado: "Boolean",
  /* Participante do liguida Natal */
  liquida: "Boolean",
  address: {
    cep: "String",
    state: "String",
    city: "String",
    district: "String",
    street: "String",
    complement: "String",
    number: "String",
    latitude: "Float",
    longitude: "Float"
  },
  /* Sócio administrador */
  partner_admin : {
    name: "String",
    cpf: "String",
    phones: ["String"]
  },
  /* Contato responsável */
  contact_responsible: {
    name: "String",
    cpf: "String",
    email: "String"
  },
  /* Conta bancária */
  bank_account: {
    legal_name: "String",
    bank_code: "String",
    agencia: "Number",
    agencia_dv: "Number",
    conta: "Number",
    conta_dv: "Number"
  },
  avatar: {
    url: "String",
    thumb: "String"
  },
  /* Quantidade máxima de parcelas */
  maxInstallments: "Number",
  description: "String",
  created_at: "Date"
}
```

### Banners

Estrutura padrão para Banners.

**Banner**

```js
{
  store_id: "String",
  /* Período que o banner irá aparecer para os usuários.*/
  period: {
    start: "Date",
    end: "Date",
    /* Caso seja indeterminado irá ignorar o inicio e o fim */
    indeterminate: "Boolean"
  }
  avatar: {
    url: "String"
  },
  /* Ação ao clicar no banner */
  action: {
    /* Tipo da ação do link 'default', '_blank' */
    target: "String",
    url: "String"
  },
  /* Posição da loja, para facilitar cálculos com distância. */
  position: {
    latitude: "Float",
    longitude: "Float"
  },
  created_at: "Date"
}
```

### Requesição de compra

Uma 'request' nada mais é que um pedido de compra realizado pelo usuário. Abaixo segue a estrutura padrão para esse modelo.

**Request**

```js
{
  /* Serviço de pagamento utilizado. Ex: pagseguro */
  service: "String",
  /* Id da transação. */
  transaction_id: "String",
  /* Status da transação. */
  status: "String",
  /* Id do checkout do serviço. */
  checkout: "String",
  /* Usuário que realizou a compra */
  user_id: "String",
  /* Total das compras. */
  total: "Float",
  /* Lista de produtos comprados */
  products: [{
    name: "String",
    price: "Float",
    quantity: "Int",
    description: "String",
    /* Id da loja */
    store_id: "String"
  }],
  /* Endereço para entrega */
  deliveryAddress: {
    logradouro: String,
    numero: String,
    complemento: String,
    bairro: String,
    cidade: String,
    uf: String,
    cep: String
  },
  /* Indica se produto deve ser pego na loja */
  pickUpInStore: Boolean,
  created_at: "Date"
}
```

### Categoria

Modelo para o controle das categorias dísponiveis dentro da aplicação.

**Request**

```js
{
  name: "String"
  created_at: "Date"
}
```
