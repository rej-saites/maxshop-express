## Modelo Banner

Neste documento será descrito todas as rotas díponíveis do servidor. O schema de todos os modelos podem ser encontrados na raíz do projeto no arquivo chamado SCHEMAS.md.


#### ``[X]`` GET /api/banners

`` [X] teste ``

Retorna todos os banners.

#### ``[X]`` GET /api/banner/:id

`` [X] teste ``

Retorna um banner baseado no id da busca.

######  Parâmetros
 * ``id`` - Id do banner.

#### ``[X]`` GET /api/banners/:id

`` [X] teste ``

Retorna todos os banners de uma loja.

######  Parâmetros
 * ``id`` - Id da loja.

#### ``[X]`` POST /api/banners/

`` [X] teste ``

Cria um novo banner.

######  Parâmetros
* ``*`` - Verificar schema para conferir os atributos obrigatórios.

#### ``[X]`` PUT /api/banners/:id

`` [ ] teste `` `` [ ] segurança ``

Atualiza banner.

######  Parâmetros
* ``id`` - id do banner.
* ``banner`` - Dentro desse obj vai os attrs a serem atualizados.

#### ``[X]`` DELETE /api/banners/:id

`` [X] teste `` `` [ ] segurança ``

remove um banner.

######  Parâmetros
* ``id`` - id do banner.
