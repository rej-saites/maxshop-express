## Modelo Pedido

Neste documento será descrito todas as rotas díponíveis do servidor. O schema de todos os modelos podem ser encontrados na raíz do projeto no arquivo chamado SCHEMAS.md.


#### ``[X]`` GET /api/requests

`` [X] teste `` ``[ ] segurança``

Retorna todos os pedidos.

######  Parâmetros
* ``page`` - Página solicitada.

#### ``[X]`` GET /api/requests/:id

`` [X] teste `` ``[ ] segurança``

Retorna um pedido por id

######  Parâmetros
* ``id`` - id do pedido.



#### ``[X]`` POST /api/requests

`` [X] teste `` ``[ ] segurança``

Cria um novo pedido de compra.

######  Parâmetros
* ``*`` - Verificar schema para conferir os atributos obrigatórios.

#### ``[X]`` PUT /api/requests/:id

`` [X] teste `` ``[ ] segurança``

Atualiza um pedido.

######  Parâmetros
* ``id`` - id do pedido.
* ``request`` - Dentro desse obj vai os attrs a serem atualizados.

#### ``[X]`` DELETE /api/requests/:id

`` [X] teste `` ``[ ] segurança``

Deleta um pedido. Existem condições para que isso seja realizado com sucesso.

######  Parâmetros
* ``id`` - id do pedido.


#### ``[X]`` GET /api/requests/store/:id

`` [X] teste `` ``[ ] segurança``

Retorna pedidos de uma loja.

######  Parâmetros
* ``id`` - id da loja.
* ``page`` - página solicitada.

#### ``[X]`` GET /api/requests/user/:id

`` [X] teste `` ``[ ] segurança``

Retorna todos os pedidos relacionados a um usuário.

######  Parâmetros
* ``id`` - id do usuário.
* ``page`` - página solicitada.
