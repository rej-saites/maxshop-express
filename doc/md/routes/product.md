## Modelo Produto

Neste documento será descrito todas as rotas díponíveis do servidor. O schema de todos os modelos podem ser encontrados na raíz do projeto no arquivo chamado SCHEMAS.md.


#### ``[X]`` GET /api/products

`` [X] teste ``

Retorna todos os produtos ordernados de forma decrescente.

######  Parâmetros
 * ``page`` - Página solicitada.


#### ``[X]`` GET /api/products/:id

`` [X] teste ``

Retorna um produto com base no id.

######  Parâmetros
* ``id`` - Id do produto.

#### ``[X]`` POST /api/products

`` [X] teste ``

Cria um novo produto.

######  Parâmetros
* ``*`` - Verificar schema para conferir os atributos obrigatórios.


#### ``[X]`` PUT /api/products/:id

`` [X] teste `` `` [ ] segurança ``

Atualiza produto.

######  Parâmetros
* ``id`` - id do produto.
* ``product`` - Dentro desse obj vai os attrs a serem atualizados.

#### ``[X]`` DELETE /api/products/:id

``[X] teste`` ``[  ] segurança``

Deleta um produto com base no id.

###### Parâmetros
* ``id`` - id do produto.

#### ``[X]`` POST /api/products/view/:id

``[X] teste``

Aumenta a visualização de um produto

###### Parâmetros
* ``id`` - id do produto.

#### ``[X]`` GET /api/category/products

``[X] teste``

Retorna produtos baseado na categoria.

###### Parâmetros
* ``category`` - Categoria da pesquisa.
* ``page`` -  Página solicitada.

#### ``[X]`` GET /api/category/:id/products

``[x] teste``

Retorna produtos baseado na categoria e loja.

###### Parâmetros
* ``id`` - id da loja.
* ``category`` - Categoria da pesquisa.
* ``page`` -  Página solicitada.


#### ``[X]`` GET /api/search/products

``[X] teste``

Procurar produtos.

###### Parâmetros
* ``query`` - query da pesquisa.

#### ``[X]`` GET /api/search/:id/products

``[X] teste``

Procurar produtos de uma loja.

###### Parâmetros
* ``id`` - id da loja.
* ``query`` - query da pesquisa.


#### ``[X]`` GET /api/stores/:id/products

`` [X] teste ``

Retorna os produtos relacionados a uma loja. Cada página retorna 4 itens.

######  Parâmetros
 * ``id`` - Id da loja.
 * ``page`` - página solicitada.

#### ``[X]`` POST /api/upload

`` [X] teste ``

Faz upload de uma imagem em base64, retorna a url.

######  Parâmetros
* ``image`` - Imagem em base64.

#### ``[X]`` POST /api/group/products

`` [X] teste ``

Retorna produtos baseado no array de Id's passados como parâmetro.

######  Parâmetros
* ``ids`` - Array de Id's dos produtos.
