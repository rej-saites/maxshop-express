## Modelo Category

Neste documento será descrito todas as rotas díponíveis do servidor. O schema de todos os modelos podem ser encontrados na raíz do projeto no arquivo chamado SCHEMAS.md.


#### ``[X]`` GET /api/categories

`` [X] teste ``

Retorna todas as categorias do sistema.



#### ``[X]`` POST /api/categories/

`` [X] teste ``

Cria um nova categoria.

######  Parâmetros
* ``*`` - Verificar schema para conferir os atributos obrigatórios.


#### ``[X]`` PUT /api/categories/:id

`` [X] teste `` `` [ ] segurança ``

Atualiza categoria.

######  Parâmetros
* ``id`` - id da categoria.
* ``category`` - Dentro desse obj vai os attrs a serem atualizados.

#### ``[X]`` DELETE /api/categories/:id

`` [X] teste `` `` [ ] segurança ``

remove uma categoria.

######  Parâmetros
* ``id`` - id da categoria.
