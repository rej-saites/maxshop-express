## Modelo Usuário

Neste documento será descrito todas as rotas díponíveis do servidor. O schema de todos os modelos podem ser encontrados na raíz do projeto no arquivo chamado SCHEMAS.md.

#### ``[X]`` POST /authenticate

`` [X] teste ``

Authenticar usuário não cliente. Apenas usuários admin e donos de lojas podem se logar nessa rota.

######  Parâmetros
 * ``email`` - Email do usuário.
 * ``password`` - Senha do usuário.

#### ``[X]`` POST /authenticate-client

`` [ ] teste ``

Authenticar usuário cliente. Apenas usuários clientes podem logar nessa rota.

###### Parâmetros
* ``email`` - Email do usuário.
* ``password`` - Senha do usuário.


#### ``[X]`` GET /expire_auth

`` [X] teste `` ``[X] segurança``

Verifica a válidade do token de segurança JWT gerado quando o usuário owner ou admin faz login.


#### ``[X]`` POST /register

``[X] teste ``

Cria um novo usuário cliente.

#### ``[X]`` POST /register/owner

``[X] teste ``

Cria um novo usuário dono de loja.

###### Parâmetros
* ``name`` - Nome do usuário.
* ``email`` - Email do usuário.

#### ``[X]`` GET /showBySocial/user/:id

``[ ] teste ``

Procura um usuário baseado no id social na coleção do mongo. Com base nisso é possível fazer login social ou criar um novo usuário.

###### Parâmetros
* ``id`` - Id social que vem no login social seja facebook, google ou twitter.

#### ``[X]`` GET /api/users

``[ ] teste ``

Retorna todos os usuários cadastrados no sistema.

#### ``[X]`` GET /api/users/:id

`` [X] teste ``

Retorna um usuário com base no id.

###### Parâmetros
* ``id`` - id do usuário.


#### ``[X]`` PUT /api/users/:id

``[ ] teste`` ``[ ] segurança``

Atualiza um usuário com base no id.

###### Parâmetros
* ``id`` - id do usuário.
* ``user`` - Dentro desse obj vai os attrs a serem atualizados.

#### ``[X]`` DELETE /api/users/:id

``[X] teste`` ``[ ] segurança``

Deleta um usuário com base no id.

###### Parâmetros
* ``id`` - id do usuário.

#### ``[X]`` POST /api/forget_password

``[ ] teste``

Envia para o email do usuário a senha dele caso esteja cadastrado. Não é possível recuperar senhas de usuários admins.

###### Parâmetros
* ``email`` - E-mail do usuário do sistema.

#### ``[X]`` POST /api/duplicate_email

``[X] teste``

Verifica se o email enviado como parâmetro já existe ou não.

###### Parâmetros
* ``email`` - E-mail do usuário do sistema.

#### ``[X]`` PUT /api/change-password

``[ ] teste ``

Altera a senha de um usuário.

###### Parâmetros
* ``id`` - id do usuário.
* ``password`` - senha atual.
* ``new_password`` - nova senha.

## Interesse por produto


#### ``[X]`` GET /api/lover/:user/:product_id

``[X] teste``

Verifica se o usuário tem interesse em produdo x.

###### Parâmetros
* ``user`` - id do usuário.
* ``product_id`` - id do produto.

#### ``[X]`` POST /api/lover/:user/:product_id

``[X] teste``

Atribui interesse a um produto.

###### Parâmetros
* ``user`` - id do usuário.
* ``product_id`` - id do produto.

#### ``[X]`` DELETE /api/lover/:user/:product_id

``[X] teste``

Remove o interesse do usuário pelo produto.

###### Parâmetros
* ``user`` - id do usuário.
* ``product_id`` - id do produto.

## Store

#### ``[X]`` GET /api/stores/:id/user

`` [X] teste ``

Retorna o usuário admin de uma loja.

######  Parâmetros
* ``id`` - Id da loja.

## Cartões de crédito

#### ``[X]`` GET /api/cards/:user

`` [X] teste ``

Retorna todos os cartões do usuário.

######  Parâmetros
* ``user`` - Id do usuário.

#### ``[X]`` GET /api/card/:user

`` [X] teste ``

Retorna o cartão favorito do usuário.

######  Parâmetros
* ``user`` - Id do usuário.

#### ``[X]`` PUT /api/card/:user

`` [X] teste ``

seta um cartão como favorito.

######  Parâmetros
* ``user`` - Id do usuário.
* ``card_id`` - Id do cartão.


#### ``[X]`` POST /api/cards/:user

`` [X] teste ``

Cria um novo cartão

######  Parâmetros
* ``user`` - Id do usuário.
* ``card`` - hash do cartão.
* ``favorite`` - favorito ou não (boolean), parametro opcional.

#### ``[X]`` DELETE /api/cards/:user

`` [X] teste ``

Remove um  cartão do usuário.

######  Parâmetros
* ``user`` - Id do usuário.
* ``card_id`` - id do cartão.
