## Modelo Loja

Neste documento será descrito todas as rotas díponíveis do servidor. O schema de todos os modelos podem ser encontrados na raíz do projeto no arquivo chamado SCHEMAS.md.

#### ``[X]`` GET /api/stores

`` [X] teste ``

Retorna todas as lojas do sistema. A rota utiliza paginação então é necessário informar a página que deseja que o sistema lhe retorne. O sistema retorna vinte elementos por requisição.

######  Parâmetros
 * ``page`` - Página solicitada .


#### ``[X]`` GET /api/stores/:id

`` [X] teste ``

Retorna uma loja específica.

######  Parâmetros
 * ``id`` -  Id da loja.


#### ``[X]`` POST /api/stores

`` [X] teste `` ``[  ] segurança``

Cria uma loja  a partir dos parâmetros passados.

######  Parâmetros
 * ``*`` - Favor conferir os atributos obrigatórios no documento de schema.

#### ``[X]`` PUT /api/stores/:id

`` [X] teste `` ``[  ] segurança``

 Atualiza uma loja com base no id.

######  Parâmetros
* ``id`` - id da loja.
* ``store`` - Dentro desse obj vai os attrs a serem atualizados.

#### ``[X]`` DELETE /api/stores/:id

``[X] teste`` ``[  ] segurança``

Deleta uma loja com base no id.

###### Parâmetros
* ``id`` - id da loja.

#### ``[X]`` GET /api/search/stores

``[X] teste``

Pesquisa em lojas.

###### Parâmetros
* ``query`` - query da pesquisa.


#### ``[X]`` GET /api/homologar/stores

``[X] teste``

Retorna todas as lojas do sistema que precisam ser homologadas. A rota utiliza paginação então é necessário informar a página que deseja que o sistema lhe retorne. O sistema retorna vinte elementos por requisição.

######  Parâmetros
 * ``page`` - Página solicitada .
