## README

### Instalando o necessário

Antes de começar é necessário instalar as dependências para que o sistema funcione corretamente e também as dependências de desenvolvimento. Esse comando fará as duas coisas.

``$ npm install  -d ``

Alguns módulos necessitam ser instalados globalmente, segue abaixo a lista:

* ``sudo npm install -g markdown-styles``

### Desenvolvendo

Um arquivo chamado ``concat.js`` foi adicionado por Rafael para substituir um módulo que estava utilizando para fazer uma concatenação dos arquivos js , peguei esse arquivo e transformei em outro agora chamado de ``watch.js``. Esse arquivo fica monitorando mudanças nos arquivos na pasta ``public/js``, quando ocorre alguma modificação, arquivo novo e deletado ele gera uma nova build do arquivo ``public/js/dist/app.js``.  Não é necessário mais descrever manualmente quais arquivos irão ser concatenados, com base nos arquivos que estão na pasta o ``watch.js`` adiciona aqueles que ele encontrar na pasta para serem adicionados na build.  O watch só ignora duas pastas, sendo elas o ``dist`` e o ``lib``.

De modo semelhante esse processo também está sendo feito para a documentação que fica em ``doc/md``. Quando acontece alguma modificação desse arquivos automaticamente o watch gera o html desses arquivos que está disponível em ``doc/output``.

Para rodar o watch:

``$ npm run watch `` ou ``$ node watch``
