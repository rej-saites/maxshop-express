var fs = require('fs');
var im = require('imagemagick');
var path = require("path");
var image64 = {};

image64.decodeBase64Image = function(image, callback){
  var imageBuffer = decode(image);

  if(imageBuffer == null) return callback(true, null);

  var name = (Math.random().toString(36).slice(2)) + (Math.random().toString(36).slice(2))+'.png';
  var fullsize = '/uploads/' + name;
  var thumb    = '/uploads/thumb_' + name;

  try {
    fs.writeFile('./public' + fullsize, imageBuffer.data, function(err) {
      if (err) throw err;

         im.crop({
           srcPath: path.join(__dirname, '..', 'public/' + fullsize),
           dstPath: path.join(__dirname, '..', 'public/' + thumb),
           width: 100,
           height: 100
         }, function(err, stdout, stderr){
           if (err) throw err;
           // sucesso :D
           callback(null, { url: fullsize, thumb: thumb });
         });

    });
  } catch(e) {
    console.log(e);
    callback(true, null);
  }

}

// decodificando imagem de base64 pra buffer
function decode(dataString) {
  var matches = dataString.match(/^data:([A-Za-z-+\/]+);base64,(.+)$/),
    response = {};

  if(matches == null) return null;

  if (matches.length !== 3) {
    return new Error('Invalid input string');
  }

  response.type = matches[1];
  response.data = new Buffer(matches[2], 'base64');

  return response;
}

module.exports = image64;
