var User = require('../models/user.js');
var mongoose       = require('mongoose');

// Carregando arquivo de configuração.
var db_json = require("../database.json");
var DATABASE_URL = process.env.CDLSHOPPING_DATABASE_DEVELOPER || db_json.DATABASE_URL ;

console.log('URL do banco de dados carregado.');
console.log(DATABASE_URL);
console.log('Tentando se conectar com o banco de dados.');

var db = mongoose.connection;
db.on('error', console.error);
db.once('open', function() {
  console.log('Conectado ao MongoDB com sucesso..');

  // INICIANDO CADASTRO DE DOS DADOS INICIAS MÍNIMOS DO SISTEMA.
  var data = {
    name: 'Admin',
    email: 'admin@email.com',
    password: '123mudar',
    role: "admin",
    created_at: new Date()
  };

  var user = new User(data);

  user.validate(function(err){
    if(err) return console.log(err);
  });

  user.save(function(err, data) {
    if(err) return console.log(err);
    console.log("sucesso ao salvar admin.");
  });

});

// Conectando ao banco de dados.
mongoose.connect(DATABASE_URL);
