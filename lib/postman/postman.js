var nodemailer = require('nodemailer');
var htmlToText = require('html-to-text');
var mustache   = require('mustache');
var fs         = require('fs');
var mailgun         = require('nodemailer-mailgun-transport');

module.exports = function(body, template, scope, done){

  var auth = {
    auth: {
        api_key: 'key-b45c3a1abe8c98d60929c8b587dad820',
        domain: 'sandbox87ca31e437d44aa6a090331cba9c9181.mailgun.org'
    }
  };

  var transporter = nodemailer.createTransport(mailgun(auth));

  var template = fs.readFileSync(__dirname + '/templates/'+template+'.html', { encoding: 'utf8' });
  var html = mustache.render(template, scope);
  var text = htmlToText.fromString(html, {});

  var options = {
      from: 'CDL Shopping Natal <suporte@cdlshoppingnatal.com.br>',
      to: body.email,
      subject: body.subject,
      text: text,
      html: html
  };

  transporter.sendMail(options, function(err, info){
    if (err) return done(err);
    done()
  });
};
